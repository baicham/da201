package com.day8.generics;

/*Generics use angle brackets to create a "placeholder" for a future datatype.
 *   When instantiating a generic object you must provide the datatype, from
 *   that point that SPECIFIC object will always be that datatype. But other
 *   instances from the same generic class can be other datatypes (each individual)
 *   instance can NOT change its datatype, however.
 * 
 */
class GenericClass<BunnyWabbit /*extends Comparable, Serializable*/>{
				//the keyword extends is using for both classes AND interfaces
	private BunnyWabbit value;
	
	public GenericClass(BunnyWabbit value) {
		this.value = value;
	}
	
	public BunnyWabbit getValue() {
		return value;
	}
}
/* We can use generics in a few other types of ways:
 * 
 * 	YOu're able to use generics with methods:
 * 		public static <T> void method1(T myVariable) { T.toString(); }
 * 
 *  YOu're able to use generics like:
 *  	public final <T, J> void method1(T myVar, J myOtherVar) { blah blah}
 *  	public final <first, second> void method1(blah blah....
 *  
 *   YOu're able to use generics like:
 *   	private static <T extends Runnable> void method2(T myVar) {} 
 */


public class GenericExample {

	public static void main(String[] args) {
		//GenericClass<String> firstExample = new GenericClass<String>("hello");
		//GenericClass<Integer> firstExample = new GenericClass<Integer>(88);
		
		////later versions of java don't force you to type the datatype in both
		//// angle brackets
		//GenericClass<Character> firstExample = new GenericClass<>('t');
		
		//GenericClass<Object> firstExample = new GenericClass<>("MyString");
		
		//if you don't use the diamond brackets then the generics default to
		// the object class. This is NOT good practice, the angle brackets
		// offer compile time safety.
		GenericClass firstExample = new GenericClass<>(88);
		
		System.out.println(firstExample.getValue() instanceof Object);
		
		/*//Quick instanceof demo
		Object obj = new Thread();
		System.out.println(obj instanceof Thread);
		//				(*object*) instanceof  (*class*)
*/	}
	
	public static <T> void method1(T myVar) {
		method1(new String("mystring"));
	}

	/* Suppose we have a class hierarchy:
	 * 
	 * 					Animal {avgWeight=10;      }
	 * 	Monkey			Wolf			Turtle		...and so on
	 */
	
	/*//we could have many methods that in take each animal...
	void method1(Monkey m) {}
	void method1(Wolf w) {}
	void method1(Turtle t) {}
	//void method1(...an so on...)
	
	//OR we can have one method that takes in an animal parent class
	void method1(Animal an) {}
	
	
	
	//Now suppose....each specific animal has shadowed "avgWeight".
	 					Animal {avgWeight=10;      }
	 * 	Monkey{avgWeight=150}	Wolf{avgWeight=100}	Turtle{avgWeight=200}	...and so on
	 
	void method1(Animal an) {
		//an.avgWeight
		//but I want to print out each INDIVIDUAL avgWeight
	}*/
}
