package com.day3.arrays;

import com.example.HelloWorld;

public class ArrayExample {
	
	public static void main(String rainyday[]) {
		/*
		 * What is an array!?!?!
		 * 
		 * It's a series of data entries (of the same type);
		 * sequential in memory.
		 * 
		 * For example, a series of characters.
		 */
		
		//how do we create an array?
		int[] arryOne = {15, 88, 99};
		//int arryTwo[] = {15, 88, 100}; //two places to put the []
		String[] arryThree = new String[200];
		
		/*
		 * Default value of
		 * 		int is  0, double is 0.0, float is 0.0
		 * 		short is 0, long is 0, boolean is false, 
		 * 		char is "empty character", byte is 0,
		 * 		Object is null
		 */
		//System.out.println(arryThree[78]);
		//we access elements in an array using the square brackets
		
		//every array has an attribute called "length" that holds
		//  the number of elements in each array
		//System.out.println(arryThree.length);
		
		
		////how do we create a multi dimensional array?
		int[][] arryFour = {{9, 88},{100, 32},{23, 65}};
		int[][] arryFive = new int[3][2];
		int[] arrySix = new int[99];
		
		arryFive[1][0] = 17;
		//System.out.println(arryFive[1][0]);
		
		//System.out.println(arryFour[0][1]); //this should be 88
		arryFour[0] = arrySix; //we can change the array address
		//System.out.println(arryFour[0][1]); //this should be 0, not 88
		
		//System.out.println(arryFour); //will print memory address
		//System.out.println(arryFour[0]); // will print memory address
		
		
		//out of bounds indices
		char[] myCharArry = new char[7];
		System.out.println(myCharArry[7]); //throws an exception
		//for the purposes of a for loop:
		//int i=0; i<myCharArry.length
		
		
		
		//demoing addresses for objects, and the exception for strings
		HelloWorld h = new HelloWorld();
		//System.out.println(h);
		
		String s = new String("Andrew");
		//System.out.println(s);
		//System.out.println(s.hashCode());
		
		
	}
}
