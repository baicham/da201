package com.day3.importing;

//import com.day3.classes.Animal;
//import com.day3.classes.Car;
import com.day3.classes.*;

//only imports static members
import static java.lang.Math.*;

public class ImportExample {

	public static void main(String[] args) {
		//there are two ways to reference members of another package
		
		//we could import
		Animal an = new Animal();
		//we could use the full qualifier name
		//com.day3.classes.Car car = new com.day3.classes.Car();
		
		
		Car car= new Car();
	}

}
