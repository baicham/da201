package com.day3.classes;
/*Scopes of java:
 *    static (aka class)- one copy of the variable and/or method
 *    instance (aka object)- each instance has a copy of the variables
 *    					and/or methods
 *    method - the variables only exist within the method
 *    block (aka local)- only exist within blocks/curlybraces
 * 
 */
public class Car {
	boolean parked; //these are instances variables
	double curVin;
	static String color;
	String make;
	String model;
	
	/*Deciding whether to use instance or static scopes...
	 * simply ask yourself "is this information for the collective(class)
	 *    or the individual (instance)?"
	 */
	
	static int counter=0;
	static int vin=0;
	static int howManyBlueCars;
	static int howManyRedCars;
	static Car[] allCars;
	
	static void myStaticMethod() {
		//you may access a static variable inside of a static method
		//BECAUSE neither of them need to reference a specific instance
		System.out.println(counter);
		
		//you may NOT access an instance variable inside of a static
		//method BECAUSE the static method doesn't know which instance
		// to target for your logic
		
		//System.out.println(make);
	}
	void myInstanceMethod() {
		//you may access a instance variable from inside of an instance
		//method BECAUSE we're already inside of an object, so just use
		// the current instance
		System.out.println(make);
		
		//you may access an static variable from inside of an instance
		//method BECAUSE each object knows where its blueprint is
		// so there is no ambiguity
		System.out.println(counter);
	}
	
	//IF there are NO other constructors....the compiler will
	//   create a no args constructor for you
	//   A default constructor is the constructor given to you by
	//    the compiler, NOT any constructor you create
	
	public Car(){
		//no args constructor
		//this is NOT a default constructor
		color="cosmic blue";
		make= "honda";
		model= "civic";
		counter++;
		curVin= vin;
		vin++;
	}
	
	Car(String myColor, String myMake){//this is an args constructor
		color= myColor;
		make= myMake;
		
		if(make=="dodge") {
			model="charger";
		}else {
			model="I don't even......";
		}
		counter++;
		curVin= vin;
		vin++;
	}
	
	//this is an args constructor
	Car(String myColor, String myMake, String myModel){
		color= myColor;
		make= myMake;
		model= myModel;
		counter++;
		curVin= vin;
		vin++;
	}
	
	void MTVsPrintMyCar() {
		System.out.println("Make: "+make+ "  Model: "+model+
				"  Color: "+color);
	}
}
