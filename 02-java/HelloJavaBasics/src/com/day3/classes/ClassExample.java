package com.day3.classes;

/*
 * What is.....a class?
 * 		A blueprint for an object.
 * 
 * What is....an object?
 * 		A group of states(variables) and actions/behaviors(methods)
 * 
 * Naming conventions of java:
 * 		variable names:
 * 			camel case. e.g. myFirstName, myLastName
 * 		class names:
 * 			title case. e.g. Animal, UserStory, ButtonColors
 * 			nouns
 * 		interface names:
 * 			title case. e.g. Runnable, Comparable
 * 			adjectives
 * 		method names:
 * 			camel case. e.g. drawRectangle, run
 * 			verbs
 * 		package names:
 * 			lowercase.    e.g. java, lang, sql, util, etc
 * 		constants:
 * 			uppercase.  e.g. RED, YELLOW, MAX_PRIORITY, etc
 */

public class ClassExample {
	
	public static void main(String[] Trevin) {
		//what is the simplest way to create an animal object.
		Animal an; //NOPE
		Object animal; //NOPE
		Animal an2= new Animal(); //NOPE
		new Animal(); //the simplest way to create an object
		
		//what is the difference between initializing and
		// instantiating?
		int i= 8; //initializing
		Thread t= new Thread(); //instantiating
		
		
		//what is a constructor?
		// a special method that constructs instances of classes
		//Car myCar= new Car("purple", "dodge");
		//myCar.MTVsPrintMyCar(); 
		
		Car myOtherCar= new Car();
		myOtherCar.MTVsPrintMyCar();
		
		Car myThirdCar=new Car("red", "audi", "r8");
		
		
		System.out.println("the other two: ");
		myOtherCar.MTVsPrintMyCar();
		myThirdCar.MTVsPrintMyCar();
		
		Car.color= "yellow";
		myOtherCar.MTVsPrintMyCar();
		myThirdCar.MTVsPrintMyCar();
		
		new Car();
		new Car();
		new Car();
		System.out.println(Car.counter);
	}
}
