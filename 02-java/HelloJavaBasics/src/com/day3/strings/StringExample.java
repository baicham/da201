package com.day3.strings;

public class StringExample {

	/*
	 * What is a String?
	 * 		A class that is implemented using an array of chars;
	 * 	 also, a string is immutable.
	 * 
	 * What is immutable?
	 * 		Can't be changed.
	 */
	
	public static void main(String[] args) {
		Object obj;  //this is the most simple way to create an object reference
				//by default the value will be null (bcuz its an object)
		Object objAgain= new Object();
		Thread thd = new Thread(); //this is how you create objects
		
		//two ways to create strings
		String s= "this right here";
		String s2= new String("Helloooo Worldddd!!! XD");
		
		///System.out.println(s2);
		//obj.    //take a look at the methods on an Object class
		//s2.     //take a look at the methods on a String class
		
		//s2.substring(4); //this method outputs a string address but does NOTHING
			//with it. The original string doesn't change BECAUSE its immutable
		
		//this is one way to utilize the newly created string reference
		//String s3= s2.substring(4);
		//System.out.println(s3);
		
		//this is another way to utilize the newly created string reference
		//s2 = s2.substring(1, 21);
		//s2= s2.concat(" ^_^;"); //concatenation
		//s2= s2.toUpperCase();
		//s2= s2.toLowerCase();
		//System.out.println(s2);
		
		//char c= s2.charAt(21); //similar to the index of an array
		//System.out.println(c);
		
		//System.out.println(s2.length());
		
		//another way to concat a string
		//System.out.println("something like this"+ "something else");
		
		
		//System.out.println(5 + 5 + " my string" + 9+ 8+ (5 + 99)); //type coercion
		
		
		//mutable counterparts to Strings
		
		//stringbuilder, NOT thread safe
		StringBuilder sb= new StringBuilder("Hello =)");
		sb.append(" World");
		System.out.println(sb);
		
		//stringbuffer, thread safe
		StringBuffer sbuff= new StringBuffer("Hello from");
		sbuff.append(" the other siiiiiiide~!");
		System.out.println(sbuff);
		
	}

}
