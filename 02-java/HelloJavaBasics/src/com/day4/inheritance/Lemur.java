package com.day4.inheritance;

public class Lemur extends Monkey{
	boolean tail= false;
	boolean nocturnal= true;
	int height=17; //this is called shadowing
	int weight=19;
	
	
	Lemur(){
		//super();
		super(6, "myString");
		System.out.println("Inside the Lemur constr");
	}
	
	//The override annotation  (any time you see "@" it is an annotation)
	//This annotation forces your spelling of the method to be the same as
	//	the spelling in the parent's.
	@Override
	void speak() {
		System.out.println("\tLemur's speak: ");
		super.speak(); //this is a portal to the parent's speak() logic
		System.out.println("I am king Julian~!");
		
	}
	
	String likeToMoveIt() {
		System.out.println("I like to move it move it!");
		return "";
	}
	
	void printHeight() {
		//without super, it targets THIS SPECIFIC instance's members
		System.out.println("this is my height: "+ height);
		//the keyword super targets the parent's members
		System.out.println("this is my height: "+ super.height);
	}
	
	void printNoc() {
		boolean nocturnal= false;
		System.out.println("this is my noc status: "+nocturnal);
		System.out.println("this is my noc status: "+this.nocturnal);
		
	}
	
	
}
