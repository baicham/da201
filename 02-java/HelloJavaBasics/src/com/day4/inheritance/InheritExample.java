package com.day4.inheritance;
/*
 *    The Animal class
 *          v
 *    The Monkey class
 *         v
 *    The Lemur class
 */
public class InheritExample {

	public static void main(String[] args) {
		//Animal an = new Animal();
		//a family line has been created
		//Monkey mon = new Monkey();
		//Lemur lem= new Lemur();
		//a new family line has been created
		//Human hum = new Human();
		
		//BEFORE inheriting
		//an.  //look at the members of the animal class
		//mon. //look at the members of the monkey class
				//  without inheritance
		//lem. //same as above^
		
		//AFTER inheriting
		//an.   //nothing changes
		//mon.  //now it has access to animal class' members
		//lem.  //now has access to animal AND monkey members
		
		//I could access animal's shadowed variable by casting it
		//System.out.println("w: "+lem.weight+ " h: "+lem.height);
		//System.out.println("w: "+((Animal)lem).weight+ " h: "+lem.height);
		
		//lem.printHeight();
		//lem.printNoc();
		
		//will casting work again?
		//lem.speak();
		//((Animal)lem).speak();
		//mon.speak();
		
		
		//Animal anim = new Lemur(); //this is a upcasting example
		
		//Human huma= new Lemur(); //has to be in the same family line
		//((Human)anim).
		//anim.speak();
		//System.out.println(anim.weight);
		//Animal an2 = new Animal();
		//an2= methodForty();
		//System.out.println((Lemur)an2);
		
		//demo of constructors in inheritance
		//Lemur lem2=new Lemur();
		//System.out.println(lem2.genus);
		
		//demo of overloaded constructors
		Monkey m= new Monkey(7, "theString");
		
		//quick tip
		/*anim;
		System.out.println(anim);
		5;
		System.out.println(5);*/
	}
	
	
	//this is an example of HOW you could use downcasting
	int method1(Object obj) {
		System.out.println((Lemur)obj);
		
		if(obj.getClass().getName()!="Lemur") {
			return 10;
		}else {
			return 9;
		}
	}

}
