package com.day4.inheritance;

public class Monkey extends Animal {
	boolean thumbs= true;
	int numOfFingersPerHand= 4;
	
	Monkey(){
		//this(7, "ssss");
		//the first line of any constructor needs to be a variable of this()
		//  OR   super().  You cannot do both, because it can't be used beyond
		//  the first line of logic.
		super();
		System.out.println("In the monkey constructor");
	}
	
	Monkey(int numOfFingersPerHand, String s){
		this('t');
		//super();
		this.numOfFingersPerHand = numOfFingersPerHand;
		System.out.println("in the one arg constructor");
	}
	
	Monkey(char c){
		//super();
		//this(7, "sss");
		System.out.println("inside char monkey");
	}
	
	void eatBanana() {
		System.out.println("ohm nom nom");
	}
}
