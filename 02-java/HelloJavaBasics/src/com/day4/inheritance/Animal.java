package com.day4.inheritance;

/*
 * If you ever find yourself duplicating variables and methods
 * in multiple classes,
 * you should think about creating a parent class for each of the
 * classes.
 */
public class Animal{
	boolean isAlive= true;
	int height=10;
	int weight=11;
	String color;
	String genus;
	
	Animal(){
		super(); // the "super" constructor call may only be used ONCE, as the
				// first line of any constructor
		genus= "initial coniditions";
		System.out.println("In the animal constructor");
	}
	
	void speak() {
		System.out.println("Make animal noises!");
	}
}
