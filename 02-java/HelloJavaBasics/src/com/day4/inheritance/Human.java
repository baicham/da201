package com.day4.inheritance;

public class Human extends Animal{
	boolean bipedal= true;
	int iq = 100;
	
	Human() {
		System.out.println("Inside the human constr");
	}
	
	void pollute() {
		System.out.println("We ruin what we touch");
	}
}
