package com.day9.covariance;

import java.io.FileNotFoundException;
import java.io.IOException;

public class SuperHuman {

	String name;
	
	 Object useSuperPower() throws FileNotFoundException, IOException {
		System.out.println("Pow! Crash! Bang!");
		return "power used";
	}
}
