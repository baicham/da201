package com.day9.covariance;

import java.io.IOException;
import java.util.InputMismatchException;
/*
 *Covariance in java:
 *		Covariant return types
 *		Covariant access modifiers
 *		Covariant throws declarations
 *
 * Covariance is when you override a method and alter the method signature in
 * 	very specific ways.
 * 
 * Covariant return types- you may alter the return types of a overridden method
 *  by narrowing the range of possible return objects BUT you cannot widen
 *  the range of possible return objects.
 *  
 * Covariant access modifiers- you may alter the access modifiers of an overridden
 *  method by allowing MORE possible classes to use the method BUT you cannot
 *  take access AWAY from classes that already had access to the method.
 *  
 *  Covariant throws declarations- you may alter the throws declarations of an
 *   overridden method by reducing the amount of possible exceptions that can be
 *   thrown BUT you cannot add NEW exceptions that can be thrown. Additionally,
 *   you are able to remove ALL thrown exceptions in the overriden method's logic.
 *   (When I say exception here I am talking about checked exceptions NOT unchecked)
 */
public class SuperVillain extends SuperHuman {
	
	int minionCount;

	@Override
	public String useSuperPower() /*throws IOException, Throwable*/ {
		System.out.println("Aaah! Mahaaa! Muahahahaaaa!");
		return "power used";
	}
}
