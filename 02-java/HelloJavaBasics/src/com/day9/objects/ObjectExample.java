package com.day9.objects;

public class ObjectExample {

	public static void main(String[] args) {
		//This is the Object class. All classes are descendants of the
		// Object class
		Object obj = new Object();

		//obj.toString();  //System.out.print  secretly and automatically
					//calls this method to determine what prints to the console.
		//obj.equals(anotherObject); // compares memory address of two objects
					// UNLESS it is overridden
		//obj.hashCode();  //creates a psuedo unique value determined by the
				//object itself; this value is used inside of Maps
		
		//see how all objects inherit from the Object class
		DummyObject dobj = new DummyObject();
		//dobj.
		
		DummyObject one = new DummyObject();
		DummyObject two = new DummyObject();
		/*System.out.println(one);
		System.out.println(two);
		System.out.println(one.hashCode());
		System.out.println(two.hashCode());*/
		
		// the "==" operator compares the memory address of two objects
		//System.out.println(one==two);
		
		String three= new String("hello");
		String four= new String("hello");
		System.out.println(three==four); //checks the memory address
		System.out.println(three.equals(four)); // checks the contents of the class
								//which was overridden by the String class
		
		String five= "hello";
		String six= "hello";
		System.out.println(five==six);
		System.out.println(five.equals(six));
		
	}

}
