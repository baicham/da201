package com.example.methods;

public class OverloadingExample {
	/*
	 * Overloading is when you have multiple methods with the same name, but different
	 *   parameter lists.
	 *  There are three ways to change the parameter list:
	 *  	-change the data types
	 *  	-change the number of parameters
	 *  	-change the order of parameters
	 */
	
	public static void main(String[] args) {
		methodOne('i','p', 'y', 'l', '6', 't');
		methodOne(false, 7,8,9,3);
		//methodTwo(15);//the 15 is the argument
		
		//without static we'd have to have an instance
		/*OverloadingExample oEx= new OverloadingExample();
		oEx.methodOne();*/
		
		//with static, we don't
		//OverloadingExample.methodOne(); //we don't need the full qualifier because there is no ambiguity
		methodOne();
	}
	
	static void methodOne() {
		System.out.println("there are no arguments");
	}
	
	static void methodOne(int i) {//"int i" is the parameter list
		System.out.println("integer argument: ");
	}
	
	static void methodOne(char i) {
		System.out.println("char argument");
	}
	
	static void methodOne(char i, int k) {
		System.out.println("char, int arguments");
	}
	
	static void methodOne(int k, char i) {
		System.out.println("int, char arguments");
	}
	
	//var args
	 static void methodOne(char... i) {
		System.out.println("multiple char arguments");
	}
	static void methodOne(boolean b, int... k) {
		System.out.println("multiple char arguments again");
		
		for(int myTemp: k) {
			System.out.println("this element is: "+myTemp);
		}
		//if hands you the varargs in the form of an array
		System.out.println("one more element: "+k[0]);
	}
	
	/*
	 * drawRectangle(int x, int y, int height, int length)
	 * drawRectangle(int x, int y, int height, int length, double alpha)
	 */
}
