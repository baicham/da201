package com.example.methods;

public class MethodExample {
	public static void main(String[] args) {
		double num= 2;
		
		/*num= num+2;
		num= num/3;
		System.out.println(num);
		
		num= num+2;
		num= num/3;
		System.out.println(num);
		
		num= num+2;
		num= num/3;
		System.out.println(num);
		
		//and so on for 17 more iterations
		 */
		num = method1(num);
		num = method1(num);
		//18 more times
		}
	
		public static double method1(double num) {
			num= num+2;
			num= num/3;
			System.out.println(num);
			
			return num;
		}
		
		/*The method signature:
		 * 
		 * 		[modifier(s)]	[return type]	[method name]   ( [parameter list] )
		 * 		{
		 * 				//our logic
		 * 		}
		 * 
		 * modifiers can be keywords like: public, protected, private, static, final,
		 * 						default, synchronized, transient, abstract, etc
		 */
}
