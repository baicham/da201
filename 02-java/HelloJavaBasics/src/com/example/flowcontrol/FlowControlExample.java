package com.example.flowcontrol;


public class FlowControlExample {
	/*
	 * What is flow control?
	 * 	if statements, loops, try catch blocks, etc
	 * 
	 */
	
	public static void main(String[] Cellphones) {
		int i=0;
		
		if( false && method1() ) { //a true or false condition (no other falsey values)
			// &&  or || are called short circuit operators
			// the purpose of the short circuit operators is to speed up the logic
			System.out.println("in the if statement");
		}
		/*else {
			System.out.println("in the else statement");
		}*/
		
		System.out.println("done");
	}
	
	public static boolean method1() {
		System.out.println("inside of the method");
		return true;
	}
	
}
