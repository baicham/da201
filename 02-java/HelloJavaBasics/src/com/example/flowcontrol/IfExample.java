package com.example.flowcontrol;

public class IfExample {
	public static void main(String[] argsOrSomething) {
		boolean bool = false;

		// if statements
		if (bool) {
		} else if (!bool) {
		} else {
		}

		// ternary statements
		int x = 0;
		String s = (x <= 9) ? "gravy" : "buttered toast";

		String k = "plank";
		// switch statement
		//     switch only allows conditions of: int, short, byte, char,
		//			(all their wrapper classes), String, and enum
		switch (k) {
		case "edd":
			System.out.println("first case");
			break;
		case "plank":
			System.out.println("second case");
			break;
		default:
			System.out.println("default case");

		}
	}
}
