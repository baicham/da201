package com.example.flowcontrol;

public class LoopsExample {
	public static void main(String[] args) {
		//quickly do loops, but they are the same as Javascript
		boolean bool= false;
		
		//while loop
		while(bool) {
			
		}
		
		//for loop
		for(;bool;) {
			
		}
		
		//do while loop
		do{
		}while(bool);
		
		
		int [] ourArray = {67, 800, 100, 23};
		//an enhanced for loop (for each loop)
		for(int myTemporaryVariable: ourArray) {
			System.out.println(myTemporaryVariable);
		}
		
		
			
			
			
			
	}
}
