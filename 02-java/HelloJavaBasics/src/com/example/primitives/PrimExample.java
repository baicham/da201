package com.example.primitives;

public class PrimExample {
	public static void main(String[] arguments) {
		/*A bit is a 0 or 1 (a "on" or "off" value) 
		 * A byte is 8 bits
		 * A nibble 4 bits
		 * 
		 * meter, kilometer? 1000 meters
		 * byte, kilobyte? 1024      2^10=1024
		 * 
		 * meter= 1
		 * kilometer= 1000
		 * megameter= 1,000,000
		 * gigameter= 1,000,000,000
		 * tera     = 1,000,000,000,000
		 * peta		= 1,000,000,000,000,000
		 * exo
		 * 
		 * 
		 * 
		 * What are the primitive types in java??????
		 * 
		 * int(4 bytes), double(8 bytes), float(4 bytes), boolean(1 bit)
		 * long(8 bytes), char(2 bytes), byte(1 byte), short(2 bytes)
		 */
		
		//how do we declare a variable?
		//like this ->   "datatype" "variable name"
		int x; //this is how we declare a variable
		int x2= 16; //this is how we declare a variable AND assign it a value
		
		boolean bool= true;   //a true or false value
				//there are not falseys or truthys beyond these two
		byte by= 7;  //a smaller space efficient integer representation
		char c= 'T'; //a single character value
		int i= 3489; //a integer numeric value
		double d = 55.289489D; //a decimal numeric value
		float f= 77.290F; //a floating point value. Holds big decimals with less
					//precision
		short s= 15; //2 bytes, space efficient
		long l= 15L; //8 bytes, holds larger ints
		
		///other primitive info
		double dTwo = 32.289;
		double dThree= 75;
		float fTwo= (float)88.98;
		int iTwo= (int)88.77;
		
		//System.out.println(88.65/11);
		//System.out.println(iTwo);
		
		int iThree= 786_3_2_9_8_3_4; //for readability
		//System.out.println(iThree); //doesn't print the underscores
		
		char cTwo = 333;  //number to char
		System.out.println(cTwo);
		int iFour = '�'; //char to number
		System.out.println(iFour);
		
		System.out.println(++iFour);
		
	}
}
