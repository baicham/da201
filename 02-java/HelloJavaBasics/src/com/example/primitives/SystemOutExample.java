package com.example.primitives;

public class SystemOutExample {
	public static void main(String[] args) {
		//the main method is the entry point to the program
		System.out.println("Bad Javier"); //println adds a new line at the end of the string
		System.out.print("Cupcakes "); //print without "ln" doesn't create a new line
		System.out.println("Apple\tsauce"); //escape characters are special
			//combinations of characters that trigger additional functionality
			//  \n- new line,    \t- tab    ,   and so on
		
		System.out.println(88);
		System.out.println("My Index: "+ 88);
	}
}
