package com.day6.exceptions;
/*
 * All exceptions are thrown at runtime.
 * 
 * Checked vs unchecked:
 * 		checked exceptions need to have a try/catch OR they need to be ducked.
 * 		unchecked exceptions could have a try/catch or be duck...but they could
 * 			also not. No syntax error will happen in either case.
 * 
 * 		Who does the "checking"? the compiler
 */
public class ExceptionExample {

	public static void main(String[] args) throws Throwable{
		//This was to demo the stacktrace having multiple lines
	//ExceptionHelper.printMyStackTrace();
		
		//This demo was to demonstrate how to duck using the "throws" keyword
		// when the responsibility is ducked, then the calling method has to "handle"
		// the ducked exception
	//ExceptionHelper.demoHowToDuckResponsibility();
		
		Object obj = new Object();
		//throw obj; // you cannot throw any ol' object
		
		Throwable th = new Throwable();
		
		try { //in a try block, you attempt to execute code that could POSSIBLY
			//throw an exception. In the case below...it will obviously ALWAYS
			// throw an exception
			boolean b= true;
			System.out.println("I'm in the try block...what could go wrong?");
			if(b) {
				throw th;
			}
			System.out.println("hello");
		} catch (Throwable e) { //the catch block is like an if statement that
			//has a true condition when the exception thrown is the same at
			// the exception in the catch's parenthesis
			System.out.println("well, that escalated quickly");
			e.printStackTrace();
			System.out.println("Pineapples");
		}
		
		
		/*//this is an example of a runtime exception not being handled.
		// If the JVM sees the exception it terminates the application
		boolean b= true;
		System.out.println("checkpoint 1");
		if(b) {
			throw new RuntimeException();
		}
		System.out.println("checkpoint 2");*/
	}

}
