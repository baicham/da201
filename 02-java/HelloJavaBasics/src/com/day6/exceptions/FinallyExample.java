package com.day6.exceptions;

import java.io.IOException;

public class FinallyExample {
	public static void main(String[] args) {
		//throw new RuntimeException();
		
		try {
			System.out.println("im in the try block");
			boolean b= false;
			if(b)
				throw new IOException();
			else
				throw new Error();
			//System.exit(0);
		}/*catch(Throwable e){ //order matters
			System.out.println("in the second catch block");
		}*/catch(IOException e){
			System.out.println("in the first catch block");
		}catch(Throwable e){ //order matters
			System.out.println("in the second catch block");
		}finally {
			//regardless of the outcome of the try block, the finally block will
			// run.
			System.out.println("in the finally block");
			//finally will not run if there is a "System.exit();" in the try block
			//finally will not run if there is a fatal error
			//finally will not run if there is a system failure
			
			//we use a finally when you want a code block to run no matter what
			// for example: closing an input stream regardless of whether an
			//   exception has been thrown
		}
		
		//you may have a try with a catch
		//   a try with a finally (no catch block)
		//   or a try catch finally
	}
}
