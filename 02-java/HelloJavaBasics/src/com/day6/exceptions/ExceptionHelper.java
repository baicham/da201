package com.day6.exceptions;

import java.io.FileNotFoundException;
import java.io.IOException;

public class ExceptionHelper {
	
	
	public static void printMyStackTrace() {
		try {
			throw new FileNotFoundException();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public static void demoHowToDuckResponsibility() throws FileNotFoundException, IOException, ArrayIndexOutOfBoundsException {
		
		throw new FileNotFoundException();
	}
	
	public static void demoTryCatchNecessity() {
		//you NEED to handle using a try catch OR a throws declaration
		//throw new FileNotFoundException();
	}
}
