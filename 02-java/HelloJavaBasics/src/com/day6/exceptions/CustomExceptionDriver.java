package com.day6.exceptions;

public class CustomExceptionDriver {
	public static void main(String[] args) {
		MyCustomException myE = new MyCustomException();
		//throw myE;  //our custom exception is checked
		
		MyCustomRuntimeException myRE = new MyCustomRuntimeException();
		//throw myRE;  //our custom runtime exception is unchecked
		
		
		
		///DEMO OF THE CONSTRUCTORS
		MyCustomRuntimeException myRE2 = new MyCustomRuntimeException("muffin button", new MyCustomException("things went wrong", myRE));
		throw myRE2;  //our custom runtime exception is unchecked
		
	}
}
