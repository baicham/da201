package com.day6.defaultvalues;

public class ValueExample {

	static double d;
	static Object obj;
	
	public static void main(String[] args) {
		/*the default values will be printed
		 * arrays, instance scopes, and static scopes receive default values
		*/
		/*System.out.println(d);
		System.out.println(obj);*/

		/*
		 * block scope and method scope do NOT use default values for variables;
		 * you have to initialize them else you'll get an syntax error
		 */
		double d2=0;
		Object obj2=null;
		System.out.println(d2);
		System.out.println(obj2);
		
	}

}
