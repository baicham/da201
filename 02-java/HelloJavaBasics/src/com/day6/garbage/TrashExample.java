package com.day6.garbage;

public class TrashExample {

	public static void main(String[] args) {

		Object obj;
		Object obj2= new Object();
		Object obj3= new Object();
		
		obj= null;
		obj2= null; //eligible
		obj= obj2;
		
		obj= obj3;
		obj3=null;
		
		//how do I call the garbage collector?
		System.gc(); //this does NOT force the garbage collector to clean up
			//it can only SUGGEST to the garbage collector to show up
		
		/*
		 * The garbage collector will call the "finalize()" method of any object
		 *  it is about to delete JUST before its deletion. The finalize() method,
		 *  however, is deprecated. Nowadays, we just use a finally block if we
		 *  want some last minute logic to fire.
		 */
		
	}//the garbage collector will come here because the method scope ends for the
	//reference variables

}
