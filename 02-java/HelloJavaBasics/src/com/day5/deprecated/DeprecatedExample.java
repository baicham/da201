package com.day5.deprecated;

public class DeprecatedExample {

	public static void main(String[] args) {
		
		DeprecatedExample dEx= new DeprecatedExample();
		
		dEx.method1();
	}
	/*
	 * Deprecated means that the support for whatever is 
	 * deprecated has been discontinued.
	 * It will still function for backwards compatibility sake,
	 * HOWEVER it lets the developer know that they should be
	 * switching to a newer system.
	 */
	@Deprecated
	void method1() {
		System.out.println("in method 1");
	}

}
