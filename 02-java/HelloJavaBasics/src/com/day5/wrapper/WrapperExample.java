package com.day5.wrapper;
/*
 * What is a wrapper class?
 *    It wraps "stuff" like presents. AKA it is a class that
 *    wraps primitive types into reference objects
 *    
 * Each primitive data type has a wrapper class counterpart:
 *    int     	Integer
 *    double	Double
 *    float		Float
 *    char		Character
 *    boolean	Boolean
 *    byte		Byte
 *    short		Short
 *    long		Long
 */
public class WrapperExample {

	public static void main(String[] args) {

		Integer i = new Integer(5);
		Character c = new Character('t');
		Boolean bool= new Boolean(false);
		
		//and so on....
		
		System.out.println(i);
		
		
		//normally an object prints the address
		Object obj = new Object();
		System.out.println(obj);
		
		
		Integer myIntWrap= 99;
		//however, the wrapper does not
		System.out.println(myIntWrap);
		
		//this is called....autoboxing
		Integer anIntWrap= 88;
		//this is called...unboxing
		int i2= i;
		
		method1(c);
		
		int i3= Integer.valueOf("3");
		System.out.println(i3);
		
		//boolean firstBool= "true";
		boolean otherBool= Boolean.parseBoolean("true");
		System.out.println(otherBool);
		
		String s1= Double.toString(77.89);
	}
	
	static void method1(char cPrim) {
		
	}

}
