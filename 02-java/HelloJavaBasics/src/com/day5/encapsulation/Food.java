package com.day5.encapsulation;
/*
 * Encapsulation?
 *    A restriction of direct access of a class' members.
 */
public class Food {
	private String name;
	private String color;
	private int calories;
	private String texture= "bumpy";
	private boolean goodTaste;
	private String smell[];
	private boolean umami;
	
	//get texture returns the texture...that's it
	String getTexture() { //accessor
		System.out.println("i ain't got no job");
		System.out.println("nvm, Javier says I got a job");
		return this.texture;
	}
	
	void setTexture(String texture) { //mutator
		this.texture = texture;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getCalories() {
		return calories;
	}

	public void setCalories(int calories) {
		this.calories = calories;
	}

	public boolean isGoodTaste() {
		return goodTaste;
	}

	public void setGoodTaste(boolean goodTaste) {
		this.goodTaste = goodTaste;
	}

	public String[] getSmell() {
		return smell;
	}

	public void setSmell(String[] smell) {
		this.smell = smell;
	}

	public boolean isUmami() {
		return umami;
	}

	public void setUmami(boolean umami) {
		this.umami = umami;
	}
	
}
