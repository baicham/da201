package com.day5.accessmodifiers;
/*
 * Access Modifiers restrict the permissions to an object's
 *   member variables and methods.
 * As opposed to scope, which limits the life span of the variable
 * 	 or method.  
 * 
 * Access modifiers in java:
 *      public		- everyone has access
 *      protected	- accessible in the class itself, the package, the children
 *      		class, grand children classes, etc
 *      (default)	-  accessible in the class itself, and the current package
 *      private		- accessed ONLY in the class it's declared in
 *      
 *      
 *   public		- the class, the current package, children, and anything else 
 *    protected	- the class, the current package, children
 *    (default)	- the class, the current package
 *  private		- the class
 */
public class AccessExample {
	
	public static void main(String[] args) {
		Food foo = new Food();
		
		System.out.println(foo.name);
		foo.cook();
	}
}
