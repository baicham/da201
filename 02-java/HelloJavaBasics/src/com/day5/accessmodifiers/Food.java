package com.day5.accessmodifiers;

public class Food {

	protected String name= "salmon";
	private String color= "pink";
	public int calories= 123;
	boolean cooked= false; //default access modifier
	
	public Food() {
		
	}
	
	private Food(int i){
		setUp();
	}
	
	public void cook() {
		System.out.println("in the process of grillin'");
		cooked= true;
		System.out.println(name);
	}
	
	protected void alive() {
		cooked= false;
	}
	
	private void setUp() {
		//set up all my initial conditions
		//condition 1
		//condition 2
		//condition 3
		// and so on
	}
}
