package com.day7.finalexample;
/*
 * What does the final keyword do?
 * 		It will make a variable immutable (cannot be changed) after it has
 *      been declared. If a variable is declared using final then you must
 *      initialize the variable in the declaration statement.
 * 
 * 		variable-  it's value cannot be altered after declaration
 * 		method-		the method cannot be overridden	
 * 		class-		the class cannot be extended
 * 
 * 		(you cannot have a final constructor BECAUSE....a constructor isn't
 * 			inherited to begin with...so why would you need to make it unable
 * 			to be overridden?)
 */
public final class FinalExample {

	public final int MAX_NUM= 60;
	
	public static void main(String[] args) {
		FinalExample fEx = new FinalExample();
		
		/*System.out.println(fEx.MAX_NUM);
		fEx.MAX_NUM= 15;
		System.out.println(fEx.MAX_NUM);*/
		
		//fEx.method1();
		
		//look at the string class  (ctrl click)
		//String s;
		
		//look at the wrapper classes (ctrl click)
		Integer i;
		Short sh;
		Boolean bool;
		
	}

	final void method1() {
		System.out.println("im in method 1");
	}
	
	
	static final void method2() {
		/*
		 * There is no point in making a final static method BECAUSE
		 *   you cannot override a static method to begin with
		 */
		System.out.println("stuff");
	}
}
