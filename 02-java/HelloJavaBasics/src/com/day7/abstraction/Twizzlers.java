package com.day7.abstraction;

import java.io.Serializable;

public class Twizzlers extends Food /*implements Edible, Serializable, Clonable*/ {

	String flavor;
	int length;
	
	public Twizzlers() {
		super();
		System.out.println("in twizzlers class");
	}
	
	String getFlavor() {
		return flavor;
	}
	
	@Override
	void goBad() {
		
	}

	@Override
	void heatUp(int degrees) {
		
	}
}
