package com.day7.abstraction;

import java.io.Serializable;

/*
 * An interface is a contract; it forces any implementing classes
 * 		to implement its methods.
 * 
 * If youre wondering whether to use an abstract class or an interface,
 *   ask yourself "am I defining that entire base structure for the series of
 *   classes (abstract class)? OR am I simply requiring a series of class to have
 *   some ADDITIONAL functionality (interface)?
 *   
 *  Classes extends other classes AND implements intefaces
 *  HOWEVER
 *  Interface extends other interfaces....they do not extend (or implements) 
 *  classes
 */
public interface Edible extends MyOtherInterface, Serializable{
	//there are no constructors in an interface.
	// Besides, it would confuse the super() call from the child class
	
	//variables in an interface are implicitly "public" "static"
	// and "final"
	int myNum= 0;
	
	//methods in an interface are implicitly "abstract" and "public"
	void digest();
	void giveNutrient();
	
	/*
	 * Java 8 decided....to add the "default" keyword. Default allows you to
	 * 	provide an implementation to a method inside of an interface
	 */
	public default void methodOne() {}
	public default void methodTwo() {
		System.out.println("in method 2");
	}
	
	public static void myStaticMethodImplementation() {
		System.out.println("inside of a static interface method");
	}
}
