package com.day7.abstraction;

public class Chicken extends Food implements Edible, Chewable{

	boolean bone;
	boolean nugget;
	
	boolean getNugget() {
		return nugget;
	}
	
	public Chicken() {
		super();
		System.out.println("in chicken class");
	}

	@Override
	void goBad() {
		
	}

	@Override
	void heatUp(int degrees) {
		
	}

	@Override
	public void digest() {
		
	}

	@Override
	public void giveNutrient() {
		
	}

	@Override
	public void methodTwo() {
		// TODO Auto-generated method stub
		
	}
}
