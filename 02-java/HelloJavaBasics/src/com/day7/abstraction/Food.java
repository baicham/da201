package com.day7.abstraction;

public abstract class Food extends AbstractionExample {
	//you can't make variables abstract, anything inheriting from this class
	//  will already have a copy of the variable
	private String name;
	String[] ingredients;
	int calories;
	
	public Food() {
		super();
		calories= 0;
		name= "";
		System.out.println("in food class");
	}
	
	abstract void goBad();
	abstract void heatUp(int degrees);
	
	/*
	 * abstraction- revealing WHAT something does, not HOW it does that something.
	 * 		aka  hiding the implementation of a method
	 * In the case of abstract methods (or classes) we know conceptually what
	 * 	the method needs to do and the parameters it's allowed to use BUT the
	 *  implementation is hidden from us.
	 */
	/*abstract void drawRectangle();
	abstract void drawRectangle(int x, int y, int height, int width);*/
	
	void method1() {
		//you may have concrete methods inside of an abstract class
		System.out.println("this is a concrete implementation");
	}
}
