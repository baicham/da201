package com.day7.abstraction;
/*
 * Abstract classes and Concrete classes have more similarities than they have
 *  differences. Some of the differences include:
 *  		-Abstract classes cannot be instantiated
 *  		-Abstract classes can have abstract methods
 *  			-An abstract method MUST be overridden
 *   Beyond these differences, the inheritance works the same
 *   
 *   
 *   
 *   Differences between abstract class and interfaces:
 *       interface							abstract classes
 *       ---------							----------------
 *   abstract methods					concrete & abstract method
 *   implements keyword					extends keyword
 *(extends other interfaces though)
 *	methods implicitly:					no implicit modifiers
 *		abstract and public
 *	variables implicitly:
 *		public static final	
 *	NO constructor						YES constructor
 *				BOTH OF THEM MAY HAVE STATIC METHODS
 *
 *----------
 * why would we use one over the other?
 * 	 PRO ac: can provide concrete implementation (including methods and 
 * 			instance variables)
 * 		(java 8 added default...so interfaces now have concrete methods)
 *   PRO ac: constructor
 * 	 PRO i: can implements multiple interfaces without taking up the ONE spot
 *         you have to extend a class
 *
 */
public class AbstractionExample {

	public static void main(String[] args) {
		//Edible ed = new Edible();  //can't instantiate an interface
		//Food food = new Food();  //can't instantiate an abstract class
		Twizzlers twiz = new Twizzlers();
		Chicken chic = new Chicken();
		
		//twiz.
		//chic.
		Edible.myStaticMethodImplementation();
	}

}
