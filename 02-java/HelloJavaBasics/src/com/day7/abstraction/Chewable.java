package com.day7.abstraction;

public interface Chewable {
	void digest();
	//this is fine even if another interface has provided a default implementation
	// you just STILL have to override, but afterwards its fine
	void methodTwo();
}
