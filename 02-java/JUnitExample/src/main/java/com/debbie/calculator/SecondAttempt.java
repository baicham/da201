package com.debbie.calculator;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

//@FixMethodOrder
public class SecondAttempt {
	
	static Calculator tester;
	
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		tester= new Calculator();
		System.out.println("-----------BEFORE CLASS---------------");
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		System.out.println("-----------AFTER CLASS--------------");
	}

	@Before
	public void setUp() throws Exception {
		System.out.println("---------before method------------");
	}

	@After
	public void tearDown() throws Exception {
		System.out.println("--------after method-----------");
	}

	@Test
	public void multiplyTest() {
		//fail("Not yet implemented");
		System.out.println("inside multiply test");
		//				note			expected value		actual value
		assertEquals("10*0 should return 0", 0, tester.multiply(10, 0));
		assertEquals("0*10 should return 0", 0, tester.multiply(0, 10));
		assertEquals("10*2 should return 20", 20, tester.multiply(10, 2));
	}
	
	@Test
	public void addTest() {
		System.out.println("in add test");
		
		assertEquals("blah blah", 17, tester.add(15, 2));
		assertTrue("blah blah blah", 10==tester.add(5, 5));
	}
	
	@Ignore()
	@Test(timeout=3000)
	public void timeTest(){
		System.out.println("inside time test");
		tester.timeMethod();
	}
	
	@Test(expected=IllegalArgumentException.class)
	public void xyzTest() {
		System.out.println("inside xyz method");
		tester.xyzMethod();
	}

}
