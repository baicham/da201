package com.tester;

import java.io.Serializable;
import java.util.List;

public class Person implements Serializable {
	String name;
	int age;
	List<Clothes> wearing;
	
	public Person() {
	}

	public Person(String name, int age, List<Clothes> wearing) {
		super();
		this.name = name;
		this.age = age;
		this.wearing = wearing;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public List<Clothes> getWearing() {
		return wearing;
	}

	public void setWearing(List<Clothes> wearing) {
		this.wearing = wearing;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", wearing=" + wearing + "]";
	}
	
}
