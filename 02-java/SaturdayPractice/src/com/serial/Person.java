package com.serial;

import java.io.Serializable;
import java.util.List;
/*
 * automatically generate the:
 * 			no args constructor
 * 			all args constructor
 * 			getters and setters
 * 			toString method
 */
public class Person implements Serializable {
	String name;
	int age;
	//List<Clothes> wearing;
	List<String> wearing;
	
	public Person() {
		// TODO Auto-generated constructor stub
	}

	public Person(String name, int age, List<String> wearing) {
		super();
		this.name = name;
		this.age = age;
		this.wearing = wearing;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public List<String> getWearing() {
		return wearing;
	}

	public void setWearing(List<String> wearing) {
		this.wearing = wearing;
	}

	@Override
	public String toString() {
		return "Person [name=" + name + ", age=" + age + ", \nwearing=" + wearing + "]";
	}

}
