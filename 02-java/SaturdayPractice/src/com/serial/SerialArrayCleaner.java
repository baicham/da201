package com.serial;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class SerialArrayCleaner {

	//create file paths for each file
	public static String peopleFile=
			"./src/com/serial/allPeople.txt";
	public static String clothingFile=
			"./src/com/serial/allClothing.txt";
	
	
	public static void main(String[] args) {
		
		//created a method to intialize my arraylists THEN write
		//  them to a file
		//initializeMyValues();
		
		//This would be a simple example of how you read a person 
		//  from the file
		//System.out.println(readObjectPeople(peopleFile));
		System.out.println(readObjectPeople(peopleFile).get(0));
		
		/////
		/////NOW we are going to get the price of a clothing item
		//    given only a person object to begin with.
		String clothingString= readObjectPeople(peopleFile)
				.get(0).getWearing().get(2); //Ellen's 3 clothing item's name
		
		
		ArrayList<Clothes> allClothing=
				readObjectClothing(clothingFile); //obtain all clothing to start
		
		//NOW let's cycle through ALL clothing items to check if it has
		// the same name as Ellen's clothing item
		for(int i=0; i< allClothing.size();i++) {
			//gets a name of the current clothing item we are cycling through
			String newString= allClothing.get(i).getName();
			
			//this if statement checks Ellen's clothing name
			//  with the current clothing item we are cycling through
			if(clothingString.equals(newString)) {
				//once we find the clothing item we are printing the price
				System.out.println(allClothing.get(i).getPrice());
			}
		}
	}
	
	public static void initializeMyValues() {
		List<Person> allPeople= new ArrayList<>();
		List<Clothes> allClothing= new ArrayList<>();
		
		///////THIS SECTION POPULATES THE "allClothing" ARRAYLIST
		allClothing.add(new Clothes("fedora", "black", 40));
		allClothing.add(new Clothes("t-shirt", "white", 15));
		allClothing.add(new Clothes("graphic tee", "orange", 25));
		allClothing.add(new Clothes("ripped jeans", "blue", 50));
		allClothing.add(new Clothes("leggings", "black", 7));
		allClothing.add(new Clothes("socks", "white", 800));
		allClothing.add(new Clothes("trench coat", "grey", 35));
		
		////////THIS SECTION POPULATES THE "allPeople" ARRAYLIST
		List<String> outfit1 = new ArrayList<>();
		outfit1.add("graphic tee");
		outfit1.add("ripped jeans");
		outfit1.add("fedora");
		allPeople.add(new Person("Ellen", 75, outfit1));
		
		List<String> outfit2 = new ArrayList<>();
		outfit2.add("leggings");
		outfit2.add("socks");
		allPeople.add(new Person("Stephon", 20, outfit2));
		
		List<String> outfit3 = new ArrayList<>();
		outfit3.add("trench coat");
		allPeople.add(new Person("La'Keyera", 18, outfit3));
		
		writeObject(peopleFile, allPeople);
		writeObject(clothingFile, allClothing);
	}
	
	static ArrayList<Clothes> readObjectClothing(String filename) {
		//THIS was copied from our previous serialization example in training
		//  I only altered 3 lines of code. "return null" at the bottom
		//   "return myList" and the return type of the method itself
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename))) {
			Object obj = ois.readObject();// de-serialization
			//System.out.println(obj);
			ArrayList<Clothes> myList = (ArrayList<Clothes>)obj;
			return myList;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	static ArrayList<Person> readObjectPeople(String filename) {
		//THIS was copied from our previous serialization example in training
		//  I only altered 3 lines of code. "return null" at the bottom
		//   "return myList" and the return type of the method itself
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename))) {
			Object obj = ois.readObject();// de-serialization
			//System.out.println(obj);
			ArrayList<Person> myList = (ArrayList<Person>)obj;
			return myList;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	static void writeObject(String filename, Object obj) {
		//This was taken directly from our serialization example, no changes
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename))) {
			oos.writeObject(obj); // serialization

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
