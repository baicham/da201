package com.serial;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

public class SerialArray {

	public static String peopleFile=
			"./src/com/serial/allPeople.txt";
	public static String clothingFile=
			"./src/com/serial/allClothing.txt";
	
	
	public static void main(String[] args) {
		//THIS IS A MUCH MORE COMPLICATED SOLUTION TO THE
		// PROBLEM
		
		
		//initializeMyValues();
		//System.out.println(readObject(peopleFile));
		System.out.println(readObject(peopleFile).get(0));
		
		String clothingString= 
				((Person)(readObject(peopleFile).get(0)))
				.getWearing().get(2);
		
		ArrayList<Object> allClothing=
				readObject(clothingFile);
		for(int i=0; i< allClothing.size();i++) {
			String newClothingString= 
					((Clothes)(allClothing.get(i)))
					.getName();
			
			if(clothingString.equals(newClothingString)) {
				System.out.println(
					((Clothes)(allClothing.get(i)))
					.getPrice());
			}
		}
	}
	
	public static void initializeMyValues() {
		List<Person> allPeople= new ArrayList<>();
		List<Clothes> allClothing= new ArrayList<>();
		
		allClothing.add(new Clothes("fedora", "black", 40));
		allClothing.add(new Clothes("t-shirt", "white", 15));
		allClothing.add(new Clothes("graphic tee", "orange", 25));
		allClothing.add(new Clothes("ripped jeans", "blue", 50));
		allClothing.add(new Clothes("leggings", "black", 7));
		allClothing.add(new Clothes("socks", "white", 800));
		allClothing.add(new Clothes("trench coat", "grey", 35));
		
		List<String> outfit1 = new ArrayList<>();
		outfit1.add("graphic tee");
		outfit1.add("ripped jeans");
		outfit1.add("fedora");
		allPeople.add(new Person("Ellen", 75, outfit1));
		
		List<String> outfit2 = new ArrayList<>();
		outfit2.add("leggings");
		outfit2.add("socks");
		allPeople.add(new Person("Stephon", 20, outfit2));
		
		List<String> outfit3 = new ArrayList<>();
		outfit3.add("trench coat");
		allPeople.add(new Person("La'Keyera", 18, outfit3));
		
		writeObject(peopleFile, allPeople);
		writeObject(clothingFile, allClothing);
	}
	
	static ArrayList<Object> readObject(String filename) {
		try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream(filename))) {
			Object obj = ois.readObject();// de-serialization
			//System.out.println(obj);
			ArrayList<Object> myList = (ArrayList<Object>)obj;
			return myList;

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}

	static void writeObject(String filename, Object obj) {
		try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(filename))) {
			oos.writeObject(obj); // serialization

		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
