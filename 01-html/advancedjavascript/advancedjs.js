//console.log("the js file is working!");



//this is hoisting
//Hoisting- all declarations will be process before
//  all other lines of code. This allows you to call
//  variables and functions before they are declared
myFunc();
function myFunc(){
    {
        //let myVar= "number";//block scope
    }
    //let myVar= "number";//function scope
}
myFunc();
//let myVar= "number";//global scope


///////arrays!
//array indices   0          1             2
let muffins = ["starmie", "mace windu", "obi-wan"];
//console.log(muffins);
//console.log(muffins[0]);

//multidimensional array (aka a matrix)
let muffinTray= [
                    ["star wars", "mace windu"], //0
                    ["pokemans", "squirtle", "charmander", "infernape"], //1
                    ["food", "ribs", "quesadillas", 5, 10] //2
                ];

//console.log(muffinTray[2][1]); //this will print "ribs"
muffinTray[1][2] = "charizard";
//console.log(muffinTray);

//we could have an array with theoretically infinite dimensions
// example:   myArray[][][][][][][][][][][][][][][]



function myFuncTwo(){
    console.log('yeyuh');
}
//console.log(myFuncTwo); //myFuncTwo by itself is simply the function
//myFuncTwo(); //using "()" behind myFuncTwo invokes the function's logic

/////function returns
//normally
function returnExample(){
    console.log("first return example");
}
//console.log(returnExample()); //undefined at the end of the console
//but we could
function returnExampleTwo(){
    console.log("second return example");
    return "this is my return value";
}
//console.log(returnExampleTwo());


////////what is an anonymous function?
// a function without a name
let funcHolder = function (){
    console.log('this is my anonymous function');
};
//console.log(funcHolder);
//funcHolder;
//funcHolder();

/* let funcHolderTwo = function (myArgument){
    console.log('this is my anonymous function');
    console.log(myArgument);
};
funcHolderTwo("the argument"); */

//////////what is a self-invoking function?
// a function that calls itself
(function theName(myArgument){
    //console.log("in my self invoking function");
    //console.log(myArgument);
}) ("cheese");
//theName(); will not work

////////what is a nested function?
// a function within a function
function birdNest(){
    let ourVar= 7;
    function babyBird(){
        let ourOtherVar= 15;
        console.log('babybird(): '+ ++ourVar);
    }
    //console.log("inside the parent function (nested)");
    babyBird();
    //console.log(ourOtherVar);//will not work
}


//birdNest();
//birdNest();
//birdNest();
//babyBird(); //out of scope


/////////CLOSURE
//question: what is closure?
//answer: 

let foo = (function (){
    let bar = 0;
    return function() {
        return ++bar;
    }
})();

//let thisVar = 9 +11;

/* console.log(foo);
console.log(foo());
console.log(foo());
console.log(bar); */

////////errors in javascript
//throw new Error('This Javier is invalid');

/* function errorExample(ourVar){
    if(ourVar<0)
        throw new Error('number less than zero');
    else{
        //my logic
    }
} */


///////arrow notation (function shorthand notation)
// another way to create a function
// (a, b) => a+b
// OR    (a, b) => {a+b; console.log(a+b);}

let arrowExample = (numba) => {
    console.log("numba "+ numba);
    console.log("Abigail");
}

//arrowExample(5);





