//console.log("the js is showing");

//old way of doing things
function myFunc(vari){
    console.warn("pineapples don't go on pizza: "+vari);
}


let buttonOne = document.getElementById('button');
//in this case, myFuncTwo is a callback function, meaning
//   after the click event returns the callback function will fire
//A higher order function calls a callback function upon completing
buttonOne.addEventListener('click', myFuncTwo);

function myFuncTwo(eve){
    //console.log(eve);
    //console.log(eve.target);
    //console.log(eve.clientY);
    /* console.log("clientx: "+eve.clientX);
    console.log("offsetx: "+eve.offsetX);
    console.log(eve.altKey); */
}
/*
   other events:   onsubmit, onchange, click, dblclick, mouseup,
      mousedown, mouseenter, mouseleave, mouseover, mouseout,
      mousemove

      (mouseover includes children, mouseenter doesn't)
      (same with mouseleave and mouseout respectively)

   there's also:
       keyup, keydown, keypress, cut, paste, change(state change),
       submit
*/


let body=document.querySelector('body');
body.addEventListener('mousemove', myFuncFour);

function myFuncFour(Yani){
  console.log(Yani.type);
  body.style.backgroundColor= "rgb(" + Yani.offsetX + "," +
       Yani.offsetY + ", 40)";
}




