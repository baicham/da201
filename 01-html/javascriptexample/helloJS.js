/* console.log("look at all this javascript!");
// alert("panic!!!");
console.log("done with script");
document.write('<br>helloworld');

for (var i = 0; i < 14; i++) {
    document.write("<br>");
    document.write("look at this dynamic js for loop: " + i);
} */

/*
What is javascript?
    Javascript is a programming/scripting language that allows you
    to implement complex logic on web pages. Everytime a webpage
    does more than just display static imformation you're more than
    likely using JS.

    It was built mainly for DOM manipulation but it has evolved
    heavily due to its popularity.

    Javascript is a dynamically, loosely-type, and object oriented
    language. JS is an interpreted language.

    DOM stands for Document Object Model
        The DOM is a virtual representation of an HTML page
*/

//console.log("We have javascript functioning in a separte file!")
//did you notice a semi colon?

//console.log('quick message');
//did you notice the single quotes?

//an alert summons a small window that pauses the javascript
//  until the user clicks a button.
//alert("look at this alert");
//console.log("after the alert");


//////////////PRIMITIVE DATATYPES in javascript
//          number, string, boolean, null, undefined, symbol
var num= 5;     //number
var numTwo;     //is undefined
//numThree would be undeclared
var name="Aster 'The Raver'";   //string, you can use double quotes
                    // single quotes, or a combination
var isApproved= false;   //boolean, can be true or false
var noMansLand= undefined; //undefined
var noObj= null;   //

//now go to the browser and use "typeof" in the console on the
//   variables.
//we can also attempt to dynamically reassign a var to a diff type
//Also......Not a Number....is a number


// A single = is an assignment operator. It gives a variable on
//      on the left of it, the value on the right of it.

//time for a console == demo
//  a double equals, or comparison, checks if two values are equal
//    (with type coercion) then produces a true or false value

//time for a console === demo
//  a triple equals, or strictly comparison, checks if two values
//  have the same datatype AND value, then produces a true or false

/////backtiks
var tempNum= 25;
var longString= 'Printing longString: ' + tempNum +
    ' and now that we have that...theres more' +
    ' and some more stuff ' + tempNum + 'we are talking about' +
    ' and again with the talking' +
    ' is anyone' + tempNum + 'annoyed yet?';

var longStringTwo= `Printing longString: ${tempNum}
    and now that we have that there's more ${80 + 250}
    and we can keep this going ${"look at me" + 15} all day if
    we want to be honest...`;

//console.log(longString);
//console.log(longStringTwo);

///////////////REFERENCED TYPES
//          object, function, array
var exArray= ['Billy', 'Lankey'];   //array

//function
//a modularized snippet of code
function exFunc() {
    //this is where the snippet of code will be
    console.log("we're in the function example");
}

//this is an object
//an object will be a key-value pair
var exObj={
    name: 'Danny Boy',
    ability: 'electromagnetism',
    'bounty': 250000
};

////objects and their dynamic nature
///////////////////
/* console.log(exObj);
console.log(exObj.name);
console.log(exObj.bounty);
console.log(exObj["name"]); */
/* How is using exObj["name"] useful?
Let's say you have a key that is 'y2.k-18'. It has various symbols
square brackets suddenly become useful.
        var keyName= 'y2.k-18';
        exObj[keyName]; */
/* exObj.role='Mob Boss';
delete exObj.bounty;

exObj.myMethod=exFunc;
console.log(exObj);
//these two (below) are exactly the same
exObj.myMethod();
exFunc(); */

/////dynamic nature of arrays
/* var arrayOfDoggos=['doge', 'pupper', 'goodestboi', 'bowwow'];
console.log(arrayOfDoggos);
console.log(arrayOfDoggos[2]);

console.log(arrayOfDoggos[5]);
arrayOfDoggos[5]="puggerino";
console.log(arrayOfDoggos[5]);
//notice how there is an empty space
arrayOfDoggos[1]=718; */


//pass by value: means that the value has been COPIED
//pass by reference: means that you've been given an
//      REFERENCE to the location. What this does is gives
//      a single variable a second name (aka a second reference)

//////pass by what?
var passby1= 10;
var passby2= passby1;  //this is a copied (pass by value)
passby1 = 25;
//console.log(passby2);       //display the number 10

/////pass by what?
var passby3={value: 10};
var passby4= passby3;  //will this be a copy? or address to passby3?
                //so is this pass by reference or pass by value?
                //this is pass by value.
                //what is being copied is the address itself
                //meaning the variables of objects hold address, not
                //not object itself
passby3.othervalue= 17;

////////FUNCTION
function myFuncTwo(pokemans, strength){//<---this is the parameter list
    console.log("the best pokemon is: "+pokemans+strength);
}
//myFuncTwo("Starmie", 17);//<---this is the argument list
//myFuncTwo("Pikachu", 1800);
//myFuncTwo("Rhyperior", "yes");

////////////SCOPES
/* what is a scope?
  The space where a variable is accessible.
 Global scope: the variable can be accessed by everything
 Function scope: only accessible within the specific function
 Block scope:  only accessible within the block it is defined
 
 There are 3 ways to declare a variable in JS: var, let, and const
 var only enforces the global and function scopes.
 let and const enforce global, function, and block scopes.

 const means the value can't be changed once it has been set 
    a single time.
 */

//var tempVar= 18; //global scope
function myFunction() {
    //tempVar= 18; //if you don't use the var keyword the variable
                    //will be global scope by default. After the
                    // function is called at least once.

    var tempVar= 18; //what scope is this? Function scope.
    console.log("in the function"+tempVar);
}
//myFunction();
//console.log(myFunction);
//console.log(tempVar); //we cannot access tempVar because tempVar is out of scope

function myFuncThree(){
    var itHappens= true;

    if(itHappens){
        //var panic="yes";  //this will be accessible outside of the
                    //block but within the function
        let panic= "yes we are panicking";//this will be accessible ONLY
                    //within the block
        console.log(panic);
    }

    console.log(panic); //this can access the block scope after the
                //if statement has been executed at least once.
}
//myFuncThree();
//console.log(panic); //can't access the function scope


function myFuncFour(){

    //THIS IS CALLED CONTROL FLOW
    //some types of control flow:
    //   if-elseif-else statements, switch case, while loop,
    //   do while loop, for loop, for each loop,
    //      ternary statement
    
    /* if(false){
        //some other logic could be here
        console.log("the if statement has triggered");
        //or even here
    }else{
        //some other logic could be here
        console.log("it didn't happen");
        //or here
    } */

    /* let thisNum= 6;
    while(thisNum<=19){
        console.log("Help me pls: "+thisNum);
        thisNum=thisNum+1;
    } */
}
//myFuncFour();


function myFuncFive(myBool){
    if(myBool){
        console.log("IT WORKED!");
    }else if(!myBool){
        console.log(":( sad face");
    }
}
myFuncFive(NaN);
//falsey values
//      false, "", 0, null, undefined, NaN
//truthy values
//   anything that's not a falsey







