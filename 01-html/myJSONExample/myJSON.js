

//when page loads all HTML elements
window.onload = function () {
    document
        .getElementById("myOnlyButton")
        .addEventListener('click', getJSON); //this adds and event
    //to the button
}

function getJSON() {
    //console.log("muffin button!");

    //getting field value
    let myDiv = document.getElementById('myOnlyDiv');

    //STEP 1 (to create an AJAX request)
    //this object allows up to make requests and get back data
    // in short, this is our data retriever (it calls API)
    let xhttp = new XMLHttpRequest();

    //STEP 2 (to create an AJAX request)
    xhttp.onreadystatechange = function () {

        if (xhttp.readyState == 4 && xhttp.status == 200) {
            //changing the information into something js can
            //  understand
            let myJSON = JSON.parse(xhttp.responseText);

            console.log(myJSON);

            let myDiv = document.getElementById('myOnlyDiv');

            //creating an unordered list of array components
            let myUL = document.createElement('ul');

            for (let i = 0; i < myJSON.colors.length; i++) {
                let myPoint = document.createElement('li');
                let myText = document.createTextNode(myJSON.colors[i]);
                myPoint.appendChild(myText);
                myUL.appendChild(myPoint);
            }

            myDiv.appendChild(myUL);

            //creating an ordered list of object attributes
            let myOL = document.createElement('ol');

            let myPoint2 = document.createElement('li');
            let myText2 = document.createTextNode(myJSON.movies['favorite comedy']);
            myPoint2.appendChild(myText2);
            myOL.appendChild(myPoint2);

            let myPoint3 = document.createElement('li');
            let myText3 = document.createTextNode(myJSON.movies['favorite action']);
            myPoint3.appendChild(myText3);
            myOL.appendChild(myPoint3);

            for (let i=0; i<myJSON.movies['listOfMovies'].length; i++) {
                let myPoint4 = document.createElement('li');
                let myText4 = document.createTextNode(myJSON.movies['listOfMovies'][i]);
                myPoint4.appendChild(myText4);
                myOL.appendChild(myPoint4);
            }

            myDiv.appendChild(myOL);


        }
    }

    //STEP 3 (to create an AJAX request)
    //create a connection
    //open(http method, url)
    xhttp.open("GET", 'https://api.myjson.com/bins/yvno0');

    //STEP 4 (to create an AJAX request)
    //this beings the request process to retrieve information
    xhttp.send();
}

