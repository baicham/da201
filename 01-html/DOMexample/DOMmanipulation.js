//console.log("hello from the js file");

console.log(document);
//console.log(document.URL);
//console.log(document.title);
//console.log(document.all);
//document.all[11]="Wait, html elements have mailmen?";
/* console.log(document.links);
console.log(document.images);
console.log(document.forms); */

/////////GET ELEMENT BY ID
let paraTwo= document.getElementById('para2');
//text content gets all text between the tags (doesn't process tags)
//paraTwo.textContent= "Things are going horribly wrong! PANIC!";

//we can inject html elements with innerhtml
paraTwo.innerHTML='im not <b>gonna let</b> this ruin my day!';


///////////////GET ELEMENT BY CLASS NAME
let muffins= document.getElementsByClassName("headers");
//console.log(muffins);

//muffins[1].textContent="Blueb<i>erry Muff</i>ins";
//muffins[1].innerHTML="Blueb<i>erry Muff</i>ins";

//styling
//you can change styles of elements with:
//   muffins[x].style.'attribute'=......
muffins[1].style.fontWeight="normal"; //normal or bold or value

////////GET ELEMENTS BY TAGNAME!
//getElementsByTagName('');
//   same as elements by class name


/////////////////QUERY SELECTORS!
//css selectors
//"#" prefix for id, '.' prefix for class, no prefix for an element

//let selection= document.querySelector('#para2');
let selection= document.querySelectorAll('.headers');

//console.log(selection);
for(let i=0; i<2; i++){
    selection[i].innerHTML='blah blah blah';
}

//you use the selector in the following ways:
//last-child, first-child, nth-child(n)

let selectionTwo= document.querySelector('.para:nth-child(1)');
selectionTwo.innerText="nth child";
//console.log(selectionTwo);

////NAVIGATING THE DOM
//you can also use stuff like
/*
    .parentNode/.parentElement
    .childNodes/ .childElements
    .firstChild/ .lastChild
    .firstElementChild/.lastElementChild

    also, next and previous siblings

*/

//////////////CREATE ELEMENTS
//creating our new element
let newDiv = document.createElement('div');
//popuulating our new element with attributes
newDiv.id='newestDiv';
newDiv.setAttribute('title', 'div show');
//console.log(newDiv);

//creating a text node, then appending to our new div element
let divText= document.createTextNode('new text node riiiiiiight here');
newDiv.appendChild(divText);
//console.log(newDiv);

//appending our new div element onto an existing elment that is
//  currently inside of the DOM
let newSelection = document.querySelector('#para1');
newSelection.appendChild(newDiv);


/////////DELETE
////////////removeAttribute, removeChild, remove



