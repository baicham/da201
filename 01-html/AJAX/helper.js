//when page loads all HTML elements
window.onload = function() {
    document
        .getElementById("swSubmit")
        .addEventListener('click', getSW); //this adds and event
                    //to the button
}

function getSW(){
    console.log("muffin button!");

    //getting field value
    let swId = document.getElementById('swId').value;

    //STEP 1
    //this object allows up to make requests and get back data
    // in short, this is our data retriever (it calls API)
    let xhttp = new XMLHttpRequest();

    /*
        READY STATE
            State 0: not initialized
            State 1: server connection established
            State 2: request received
            State 3: processing request
            State 4: complete, request finished and
                response is ready
    */
   //STEP 2
    xhttp.onreadystatechange = function() {
        console.log("ready state is changing! weeee~! "+xhttp.readyState);

        if(xhttp.readyState == 4 && xhttp.status==200){
            //changing the information into something js can
            //  understand
            let sw = JSON.parse(xhttp.responseText);

            console.log(sw);
            setValues(sw);
        }
    }

    //STEP 3
    //create a connection
    //open(http method, url)
    //xhttp.open("GET", 'https://swapi.co/api/people/'+ swId);
    xhttp.open("GET", 'https://pokeapi.co/api/v2/pokemon/'+ swId);

    //STEP 4
    //this beings the request process to retrieve information
    xhttp.send();
}



function setValues(sw){
    document.getElementById("swName").innerHTML = sw.name;
}

