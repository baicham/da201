//console.log("in the jspractice.js file");



//falsey values are 
// false, 0, "", NaN, null, undefined
//truthy values are anything that isn't a falsey
//let myBoolCondition = false;
let myNumCondition = 125;

///////IF STATEMENTS
/* if(myNumCondition>4 && myNumCondition<=16){
    console.log('in if statement');
}else if(myNumCondition>16){
    console.log('in first else if');
}
else if(myNumCondition>32){
    console.log('in second else if');
}else{
    console.log('in else statement')
} */
/* let myBoolCondition = false;
if(myBoolCondition){
    console.log("it works");
    console.log("again, its working");} */



///////////TERNARY OPERATOR
//ternary is an inline if statement

//                (my condition) ?   (truthy logic):(falsey logic)
let muffinbutton = (80 > 95) ? 25 : 88;
//console.log(muffinbutton);
//DO NOT MAKE A COMPLEX TERNARY STATEMENT LIKE BELOW
//let button2 = (myNumCondition<=780 && myNumCondition>80 && myNumCondition%2) ? function iffy(){}(): function iffyTwo(function(){}){}
//let muffinbutton = (80>95) ? 25:function (){console.log("this is ridiculous"); return 88}();


////////////SWITCH STATEMENTS
let orange = 88;

/* if(orange===77){
    console.log("seventeh");
}else if(orange===99){
    console.log("nineteh");
}else if(orange==88){
    console.log("eighteh");
}else{
    console.log("defaulteh");
} */
let ourSwitchCaseVariable = 99;

/* switch (ourSwitchCaseVariable) {
    case 77:
        console.log("first case");
        break;
    case 99:
        console.log("second case");
        //with no break, it will perform all logic until it finds
        // a break. Note: it does NOT perform anymore case checks.
    case 88:
        console.log("third case");
        console.log("more logic");
        break;
    default:
        console.log("defaulteh");
        //break;
} */


//////////////LOOPS
//while loop
let ourLoopCondition= 1;

/* while(ourLoopCondition)
{
    console.log("in our while loop" + ourLoopCondition);
    //the following three lines are exactly the same logic
    //ourLoopCondition= ourLoopCondition + 1;
    //ourLoopCondition += 1;
    ourLoopCondition++;  //post incrementer

    if(ourLoopCondition==18){
        ///break;
        //ourLoopCondition=0;
        ourLoopCondition=false;
    }

    //ourLoopCondition--; //post decrementer
    //++ourLoopCondition; //pre incrementer
} */

///////////do while
//like a while loop but it will ALWAYS run at least one time
let doWhileCondition = 0;

/* do{
    console.log("in the do while loop");
}while( doWhileCondition)
 */

/////////for loop
//for(initializer; loop condition; end of loop logic)
/* for(let forLoopCon = 0; forLoopCon < 8; forLoopCon++){
    console.log("in the for loop"+ forLoopCon);
} */


/* for(let i= -15; i>-25; --i){
    
    if(i==-20){
        //break; //this will skip -20 then break out of the loop
        continue;
        //continue will pause the current iteration's logic then
        // go to the next iteration
    }

    console.log("in the for loop"+i);
} */


//"for of" AND "for in" loops
let myArr= [88, "Lankey", true, "pink", 112, 22];
/* for(let i=0; i<myArr.length; i++){
    console.log(myArr[i]);
} */

//"for of" loop
// a for of loop will retreive each element of an array
//      then store it in a temporary variable
/* for(let temporary of myArr){
    console.log(temporary);
} */

//"for in" loop
// a for in loop will retreive each index of an array
//      then store it in a temporary variable
/* for(let temp in myArr){
    console.log(myArr[temp]);
} */




///////////////nested flow control
/* for(let i=0; i<4; i++){
    for(let j= 0; j<2; j++){
        for(let k=0; k<2; k++){
            console.log("i is: "+i+"    j is: "+ j + "    k is: "+k);
        }
    }
} */

/* let davidFay = "yes";
if(davidFay){
    let pokemans = "magikarp";

    if(pokemans){
        console.log("splash, nothing happened");
    }else{
        for(let i=0; i<2; i++){
            console.log("pink pants");
        }
    }
} */




//////////////labels
loopOne:
for(let i=0; i<3; i++){
loopTwo:
    for(let j= 0; j<3; j++){
        if(j==1)
            break loopOne;

        console.log("i is: "+i+"    j is: "+ j);
    }

    //break;
}










console.log("practice script completed");









