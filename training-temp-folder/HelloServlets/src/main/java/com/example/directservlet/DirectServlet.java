 package com.example.directservlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.model.SuperVillain;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DirectServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/*	This method demonstrates how to directly respond to a client from a servlet using text/html.
	 *
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		/* HTTPServletRequest:
		 *
		 * is filled with session info, parameters, etc. Essentially, all the
		 * information associated with the original request
		 * 
		 * HttpServletResponse:
		 * 
		 * references an object that will be sent back to the client; this object can be
		 * used to modify the page. If we don't modify the response then nothing happens
		 * to the page.
		 */

		/* Sets content to be written in PrintWriter as html, this is the default */
		res.setContentType("text/html");
		
		PrintWriter out = res.getWriter();
		out.println("<html><body><h1>The Servlet is directly talking to the client!</h1></body></html>");
		
		System.out.println("We're inside the direct doGet");
	}

	/*	This method demonstrates how to directly respond to a client from a servlet using JSON
	 *
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		/* Sets content to be written in PrintWriter as JSON */
		res.setContentType("application/json");
		
		/*
		 * As you can see, any type of object can be written as a JSON Jackson
		 * specifically requires that objects to be transformed have getters for private
		 * variables
		 */
		
		SuperVillain aster = new SuperVillain("Aster", "FireBreath", 100_000);
		res.getWriter().write(new ObjectMapper().writeValueAsString(aster));

		System.out.println("We're inside the direct doPost");
	}
}
