package com.example.indirectservlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.model.SuperVillain;
import com.fasterxml.jackson.databind.ObjectMapper;

public class IndirectServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/*	This method demonstrates how to redirect from a servlet
	 *		Servlet sends a redirect back to the client and forces the client to visit another resource.
	 *		2 requests and 2 responses are seen by the client.
	 *		So the client is aware of the second resource's existance.
	 *
	 *		Notice how the redirect is handled by the HttpServletResponse
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		System.out.println("	We're inside the INdirect doGet");
		
		//redirect to an html page
		res.sendRedirect("http://localhost:9005/HelloServlets/resources/html/secondpage.html");
		
		//redirect to a specific servlet
		//res.sendRedirect("http://localhost:9005/HelloServlets/dirserv");
		
		//redirect to an external page
		//res.sendRedirect("https://www.google.com/");
	}

	/*	This method demonstrates how to forward from a servlet
	 * 		Servlet sends the request to another servlets to handle the request.
	 * 		1 request and 1 response are seen by the client.
	 * 		So the client is not aware of the second servlet's existence.
	 * 
	 * 		Notice how the forward is handled by the HttpServletRequest
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		System.out.println("	We're inside the INdirect doPost");
		
		//redirect to an html page
		RequestDispatcher rdis = req.getRequestDispatcher("/resources/html/secondpage.html");
		
		//redirect to a specific servlet
		//RequestDispatcher rdis = req.getRequestDispatcher("/dirserv");
		
		//redirect to an external page
		//RequestDispatcher rdis = req.getRequestDispatcher("https://www.google.com/");
		
		rdis.forward(req, res);
	}
}
