package com.example.sessionservlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.example.model.SuperVillain;

public class SessionServlet extends HttpServlet {

private static final long serialVersionUID = 1L;
	
	/*	This method demonstrates how to push an object to the session.
	 *		In short, first create an object in java then call the setAttribute() method to
	 *		push it to the session with a unique identifying name given in quotes.
	 */
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException
	{
		SuperVillain dannyboy = new SuperVillain("Danny Boy", "Technopath", 250_000);
		
		HttpSession session = req.getSession();
		session.setAttribute("villain", dannyboy);
				
		PrintWriter out = res.getWriter();
		out.println("Danny Boy is on the loose...");
	}
	
	/*	This method demonstrates how to gather form data from an HTML page.
	 *		You gather each text field individually, using a name attribute given in the HTML itself.
	 */
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse res)
			throws IOException, ServletException
	{
		String name= req.getParameter("name");
		String superpower= (String)req.getParameter("superpower");
		int bounty= Integer.parseInt(req.getParameter("bounty"));
		
		SuperVillain tempVill = new SuperVillain(name, superpower, bounty);
		
		HttpSession session = req.getSession();
		session.setAttribute("villain", tempVill);
				
		PrintWriter out = res.getWriter();
		out.println("A villain is on the loose...");
	}
}
