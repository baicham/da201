package com.example.controller;

import javax.servlet.http.HttpServletRequest;

//The second step in Front Controller Design Pattern
//		is to make a RequestHelper
public class RequestHelper {

	
	/*	This method demonstrates how to parse a URI and select a controller to forward the request to.
	 *		We use a HttpServletRequest method to get the URI then use a switch statement.
	 */
	public static String process(HttpServletRequest request) {
		
		//we're doing this just for demo purposes. This will print the URI in the console.
		System.out.println(request.getRequestURI());
		
		switch(request.getRequestURI()) {
		case "/HelloFrontController/login.do":
			return LoginController.login(request);
		case "/HelloFrontController/home.do":
			return HomeController.home(request);
		default:
			return "resources/html/unsuccessfullogin.html";
		}
	}
}
