package com.example.controller;

import javax.servlet.http.HttpServletRequest;

public class HomeController {

	/*	This method is an example of a controller. 
	 *
	 *	It returns a view for the client by simple returning the path to an html page as a String.
	 */
	public static String home(HttpServletRequest request) {
		return "resources/html/home.html";
	}
}
