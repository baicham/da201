package com.example.controller;

import javax.servlet.http.HttpServletRequest;

public class LoginController {
	
	/*	This method is an example of a controller.
	 * 
	 *  This method creates mock login functionality.
	 *		Pay attention to how this method ends by returning "/home.do" which is not a path to a html page.
	 *		The home.do will go BACK to the masterservlet then BACK to the requesthelper. The cycle will continue until
	 *		a method returns a resource path.
	 */
	public static String login(HttpServletRequest request) {
		//checks to see if the http method is a post. If it isn't then it will send the user back to the login page.
		if(!request.getMethod().equals("POST")) {
			return "index.html";
		}
		
		String username= request.getParameter("username");
		String password= request.getParameter("password");
		
		//checks to see if the user has the correct mock username and mock password
		if(!(username.equals("mac") && password.equals("cheese"))) {
			return "index.html";
		}
		else {
			//if the user has the correct username and password then both items will be pushed to the session and the user
			//	will be forwarded to the controller that handles "home.do"
			request.getSession().setAttribute("loggedusername", username);
			request.getSession().setAttribute("loggedpassword", password);
			return "/home.do";
		}
	}
}
