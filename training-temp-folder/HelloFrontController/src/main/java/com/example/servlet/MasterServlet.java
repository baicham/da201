package com.example.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.controller.RequestHelper;


//Front Controller Design Pattern
//		Starts with a MasterServlet
public class MasterServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	/*	This method demonstrates how the front controller design pattern's master servlet is constructed.
	 * 		In short, we forward the request to a helper class. It's as simple as that for the basic FC design pattern.
	 *
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		System.out.println(request.getRequestURI());
		request.getRequestDispatcher(RequestHelper.process(request)).forward(request,response);
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher(RequestHelper.process(request)).forward(request,response);
	}
}
