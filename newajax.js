//button function
window.onload = function() {
    document.getElementById('swSubmit')
        .addEventListener('click', getSw);
}

//end button function
//callback to getSw
//create getSw

function getSw() {
    console.log("here is your pokemon");
    //ensure callback works
    let swId = document.getElementById('swId').value;
    let xhttp = new XMLHttpRequest();

    //create variables for next function

    xhttp.onreadystatechange = function() {
            console.log("It works this far");

            //ensure this function is called

            if ((xhttp.readyState == 4) && (xhttp.status == 200)) {
                let sw = JSON.parse(xhttp.responseText);
                setValues(sw);

            }
        }
        //end of function
        //callback function called for setValues
        //open and send xhttp requests

    xhttp.open('GET', 'https://pokeapi.co/api/v2/pokemon/' + swId);
    xhttp.send();

}

//print results to page

function setValues(sw) {
    document.getElementById('swName').innerHTML = ("Name: ") + sw.name;
    document.getElementById('swWeight').innerHTML = ("Weight: ") + sw.weight;
    document.getElementById('swHeight').innerHTML = ("Height: ") + sw.height;
    document.getElementById('swSprite').src = sw.sprites.front_default;
    document.getElementById('BaseExp').innerHTML = ("Base Experience: ") + sw.base_experience;
    document.getElementById('swType').innerHTML = ("Type: ") + sw.types[0].type.name;
};