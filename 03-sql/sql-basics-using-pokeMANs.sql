--select * from employee;

--TO CREATE A NEW USER
----AND GRANT THAT USER PERMISSIONS
CREATE USER pokemondb IDENTIFIED BY p4ssw0rd;

GRANT connect, RESOURCE TO pokemondb;
GRANT DBA TO pokemondb WITH ADMIN OPTION;


-----------------CREATE A TABLE
--DATATYPES IN SQL: NUMBER, VARCHAR, VARCHAR2, DATE, TIMESTAMP, BLOB, CLOB
-------
--CONSTRAINTS:  primary key, foreign key, not null, unique, and check
--NOT NULL ensures the record has a value in this column
--UNIQUE ensures that any two records will not have the same value
--PRIMARY KEY it represents the official unique identifier for this table
--      PK is a combination of unique and not null
--FOREIGN KEY is a reference pointer to another table's primary key
--      FK is used as a locator (locates another record)
-- example: FOREIGN KEY (refKey from another table)
--                  REFERENCES other_table (other_PK)
--CHECK creates an additional custom condition for the column
-- example:   CONSTRAINT constraint_name CHECK (column_name BETWEEN 100 and 999)
CREATE TABLE pokemon( -- DDL
    pokemonid NUMBER(15), -- PRIMARY KEY,
    pokemon_name VARCHAR2(100) UNIQUE,
    pokemon_type VARCHAR2 (25),
    PRIMARY KEY(pokemonid)
);

--describes the structure of the table
DESC pokemon;

--drop     DDL
DROP TABLE pokemon;
--alter     DDL
ALTER TABLE pokemon ADD pokemon_secondtype VARCHAR2(25);
--truncate      DDL
----not going to do an example BUT truncate deletes all entries into the table
----while keeping the table structure itself. DROP is like throwing a cup of
----water in the trash, while TRUNCATE is like pouring out the water and keeping
----the cup.

--insert        DML
INSERT INTO pokemon VALUES(1, 'bulbasaur', 'grass', 'ice');
INSERT INTO pokemon VALUES(7, 'squirtle', 'water', null);
INSERT INTO pokemon VALUES(121, 'starmie', 'water', 'psychic');
INSERT INTO pokemon VALUES(151, 'mew', 'psychic', null);
INSERT INTO pokemon(pokemonid, pokemon_type, pokemon_name)
        VALUES(25, 'electric', 'pikachu');

--seeing ALL entries into the table
select * from pokemon;

UPDATE pokemon SET POKEMON_SECONDTYPE='poison' WHERE pokemonid=1;
DELETE FROM pokemon WHERE pokemonID=25;
DELETE FROM pokemon;
commit;
