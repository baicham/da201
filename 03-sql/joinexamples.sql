
SELECT * FROM album;
SELECT * FROM artist;

--INNER
SELECT * FROM artist a INNER JOIN album b ON a.artistid = b.artistid;
--old way to use a inner join
SELECT * FROM artist a, album b WHERE a.artistid=b.artistid;

--LEFT
SELECT * FROM artist a LEFT OUTER JOIN album b ON a.artistid = b.artistid;
--we don't need the outer keyword, it's strictly for readability
SELECT * FROM artist a LEFT JOIN album b ON a.artistid = b.artistid;
--old way to use a left join
SELECT * FROM artist a, album b WHERE a.artistid=b.artistid(+);

--RIGHT
SELECT * FROM artist a RIGHT OUTER JOIN album b ON a.artistid = b.artistid;
--old way to use a right join
SELECT * FROM artist a, album b WHERE a.artistid(+)=b.artistid;

--FULL
SELECT * FROM artist a FULL OUTER JOIN album b ON a.artistid = b.artistid;


---LEFT without middle of vendiagram, (excluded aka distinct aka strictly left outer join)
SELECT * FROM artist a LEFT OUTER JOIN album b ON a.artistid=b.artistid
    WHERE a.artistid NOT IN(
        SELECT a.artistid FROM artist a INNER JOIN album b ON a.artistid = b.artistid);
--DISTINCT LEFT JOIN
SELECT count(*) FROM artist a LEFT OUTER JOIN album b ON a.artistid = b.artistid
    WHERE b.artistid IS NULL;
--DISTINCT FULL JOIN  (this will contain the values from distinct left AND distinct right)
SELECT count(*) FROM artist a FULL OUTER JOIN album b ON a.artistid = b.artistid
    WHERE b.artistid IS NULL OR a.artistid IS NULL;
    --DISTICT RIGHT JOIN
SELECT count(*) FROM artist a RIGHT OUTER JOIN album b ON a.artistid = b.artistid
    WHERE a.artistid IS NULL;


--SELF JOIN
-----SELECT * FROM table01 A INNER JOIN table01 B ON A.id=B.foreignid;

--CROSS JOIN   aka   CARTESIAN JOIN
-----SELECT * FROM table01 CROSS JOIN table02;


------alias using the AS keyword
SELECT Albumid AS alb, title AS tle FROM album;
--another type of aliasing
SELECT * FROM album alb WHERE alb.albumid BETWEEN 5 and 9;

-----what is an entity?
-- tables=entity=data object
    
COMMIT;      







