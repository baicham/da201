--QUERY BASICS--

--select [column1, column2, etc] FROM [the table name];
SELECT lastname, firstname FROM employee;
--select *  <---the '*' means all columns
SELECT * FROM employee;
--ALIAS (using the "AS" keyword) changes the title of the attribute in a result set
SELECT lastname AS muffin, firstname AS button FROM employee;


----DIFFERENT SELECTS
SELECT * FROM employee;
SELECT * FROM employee WHERE title='Sales Manager';
SELECT * FROM employee WHERE title='Sales Support Agent' AND firstname='Jane';
SELECT * FROM eMPLOyeE WHERE firstname='Andrew' OR firstname='nancy' OR firstname='Margaret';
----ALSO, you can use >, <, <=, >= the where clause as well
--different ways to say not equals
SELECT * FROM employee WHERE firstname != 'Jane';
SELECT * FROM employee WHERE firstname <> 'Jane';
SELECT * FROM employee WHERE firstname ^= 'Jane';
--NULL references
SELECT * FROM employee WHERE reportsto IS NULL;
SELECT * FROM employee WHERE reportsto IS NOT NULL;

DESC album;
SELECT * FROM album;
--LIKE keyword
-- '%' means you can insert MULTIPLE characters (zero characters is also acceptable)
-- '_' means you must insert a SINGLE character
SELECT * FROM album WHERE title LIKE 'B%';
SELECT * FROM album WHERE title LIKE '%Big Ones';
SELECT * FROM album WHERE title LIKE '_ig Ones';
SELECT * FROM album WHERE title LIKE '%B_g_O%';

DESC employee;
SELECT * FROM employee;
----ORDER BY examples--- aka sorting
--DEFAULTS to ascending order
SELECT * FROM employee ORDER BY lastname;
SELECT * FROM employee ORDER BY lastname ASC;
SELECT * FROM employee ORDER BY lastname DESC;
SELECT * FROM employee ORDER BY city, firstname;


SELECT * FROM employee;
--BETWEEN keyword--
--inclusive range
SELECT * FROM employee WHERE employeeid BETWEEN 5 AND 7;

--IN keyword
SELECT * FROM eMPLOyeE WHERE firstname='Andrew' 
                OR firstname='Nancy' OR firstname='Margaret';
--looks cleaner to us the IN operator  -- it's also faster                
SELECT * FROM employee WHERE firstname IN ('Andrew', 'Nancy', 'Margaret');

--AGGREGATE functions
--what is an AGGREGATE function?
-------a calculation operating on a group of records (data entries)
--different aggregate functions
------COUNT, MAX, MIN, AVG, SUM, DISTINCT    
--          there are more but I want to highlight these
SELECT * FROM album;
SELECT COUNT(*) FROM album;
SELECT * FROM employee;
SELECT COUNT(*) FROM employee;
SELECT COUNT(*) FROM employee WHERE title='Sales Support Agent';
SELECT COUNT(DISTINCT ( title)) FROM employee;

SELECT * FROM invoice;
--OTHER AGGREGATE examples
SELECT invoiceid, customerid, total FROM invoice;
SELECT MAX(total) FROM invoice;
SELECT MIN(total) FROM invoice;
SELECT AVG(total) FROM invoice;
SELECT SUM(total) FROM invoice;
--GROUP BY, forces aggregate functions to work on subsets of grouped data
--      as opposed to the entire result set (aka temporary table)
SELECT billingcountry, COUNT(*) FROM invoice GROUP BY billingcountry;

--HAVING example
-----when group by is not used HAVING behaves like WHERE
-----WHERE filters before aggregate functions, HAVING may filter after
SELECT billingcountry, SUM(total) FROM invoice 
    WHERE billingcountry!= 'Ireland' GROUP BY billingcountry;
--SELECT billingcountry, SUM(total) FROM invoice 
--    WHERE sum(total)>50 GROUP BY billingcountry;
SELECT billingcountry, SUM(total) FROM invoice 
    GROUP BY billingcountry HAVING billingcountry!='Ireland';
--HAVING is capable of creating a condition based on AGGREGATE values, WHERE cannot
SELECT billingcountry, SUM(total) FROM invoice 
    GROUP BY billingcountry HAVING SUM(total)>50;
--WHERE goes before GROUP BY goes before HAVING
    
    
--SCALAR functions-------
--THE FOLLOWING IS MY OWN TERMS, OTHERS MAY NOT KNOW WHAT YOU'RE TALKING ABOUT
---Categories of scalar functions:
----    numeric, character, date, conversion

--first, what is dual?
SELECT 'hello world' FROM dual;
SELECT * FROM dual;
--NUMERIC
--      abs(x), ceil(x), floor(x), trunc(x,y), round(x,y)
SELECT abs(-99) FROM dual;
SELECT floor(88.77) FROM dual;
SELECT ceil(88.77) FROM dual;
SELECT trunc(889.991199, 4) from DUAL;
--CHARACTER         there are lots of these. Including BUT not limited to:
---     upper(x), lower(x), length(x)
SELECT 'HElLO wOrLd' FROM dual;
SELECT upper('HElLO wOrLd') FROM dual;
SELECT lower('HElLO wOrLd') FROM dual;
SELECT length('HElLO wOrLd') FROM dual;
--DATE          there are lots of these
--          next_day(x, 'week_day'), last_day(x), sysdate
SELECT sysdate FROM dual;
SELECT last_day(sysdate) FROM dual;
--CONVERSIONS       convert data types
---         to_char(), to_date()     and more
------we'll ignore the example for now

SELECT * FROM invoice;
SELECT billingcountry, billingcity FROM invoice;
SELECT upper(billingcountry), billingcity FROM invoice;
--SO...what does a scalar function actually do?
-- it's an operation that works on a SINGLE record, as opposed to an aggregate function
--  that works on a set of data.



-----SUBQUERY EXAMPLE--------AKA nested queries--------
DESC customer;
SELECT * FROM customer;
SELECT * FROM invoice;

SELECT * FROM customer WHERE customerid IN(
    SELECT customerid FROM invoice WHERE total>16.0);
--------the INNER query is defining a set of customerids that the OUTER query will
---             choose from. For this reason, the INNER query must complete first.

--more subqueries
SELECT customerid, total, invoiceid FROM (SELECT * FROM invoice WHERE total>16.0)
        WHERE invoiceid>200;
