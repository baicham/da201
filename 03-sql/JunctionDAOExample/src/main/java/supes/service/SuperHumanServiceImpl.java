package supes.service;

import java.util.List;

import supes.dao.SuperHumanDao;
import supes.dao.SuperHumanDaoImpl;
import supes.model.SuperHuman;

public class SuperHumanServiceImpl implements SuperHumanService {

	private SuperHumanDao supeDao = new SuperHumanDaoImpl();
	
	@Override
	public List<SuperHuman> getAllSuperHumans() {
		//checking it twice logic
		return supeDao.selectAllSuperHumans();
	}

	@Override
	public SuperHuman getSuperHuman(String name) {
		return supeDao.selectSuperHumanByName(name);
	}

	@Override
	public void jointPrinter(String name) {
		supeDao.jointPrinter(name);

	}

	@Override
	public boolean checkUsernameAndPassword(String u, String p) {
		// TODO Auto-generated method stub
		return false;
	}

}
