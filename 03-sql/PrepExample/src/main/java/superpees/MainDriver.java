package superpees;

import java.util.List;

import superpeeps.model.SuperHuman;
import superpeeps.service.SuperHumanService;
import superpeeps.service.SuperHumanServiceImpl;

public class MainDriver {

	public static void main(String[] args) {
		SuperHumanService shService = new SuperHumanServiceImpl();
		
		List<SuperHuman> supes = shService.getAllSuperHuman();
		
		System.out.println("PRINTING ALL SUPERHUMANS");
		for(SuperHuman s: supes) {
			System.out.println(s);
		}
		
		System.out.println("PRINTING A SINGLE SUPERHUMAN");
		System.out.println(shService.getSuperHuman("Asura"));
		
		System.out.println("PRINTING A SINGLE SUPERHUMAN WITH POWERS");
		shService.jointPrinter("Danny Boy");
		
		System.out.println("done");

	}

}
