package superpeeps.dao;

import java.util.List;

import superpeeps.model.SuperHuman;

public interface SuperHumanDao {
	//CRUD METHODS ONLY
	
	//CREATE
	public int insertSuperHuman(SuperHuman sh);
	//READ
	public List<SuperHuman> selectAllSuperHuman();
	public SuperHuman selectPokemonById(int id);
	public SuperHuman selectPokemonByName(String name);
	public List<SuperHuman> selectByType(String type);
	//UPDATE
	public int updatePokemon(SuperHuman p);
	//DELETE
	public int deletePokemon(SuperHuman p);
	
	//read that prints
	public void jointPrinter(String name);
}
