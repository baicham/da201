package superpeeps.service;

import java.util.List;

import superpeeps.model.SuperHuman;

public interface SuperHumanService {
	public List<SuperHuman> getAllSuperHuman();
	public SuperHuman getSuperHuman(String name);
	
	public void jointPrinter(String name);
}
