package superpeeps.service;

import java.util.List;

import superpeeps.dao.SuperHumanDao;
import superpeeps.dao.SuperHumanDaoImpl;
import superpeeps.model.SuperHuman;

public class SuperHumanServiceImpl implements SuperHumanService{

	private SuperHumanDao supeDao= new SuperHumanDaoImpl();
	
	public List<SuperHuman> getAllSuperHuman() {
		return supeDao.selectAllSuperHuman();
	}

	@Override
	public SuperHuman getSuperHuman(String name) {
		// TODO Auto-generated method stub
		return supeDao.selectPokemonByName(name);
	}

	@Override
	public void jointPrinter(String name) {
		supeDao.jointPrinter(name);
		
	}

}
