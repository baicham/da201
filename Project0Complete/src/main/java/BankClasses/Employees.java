package BankClasses;

import java.util.ArrayList;


public class Employees extends LoginPage {
	
	static Customer Indi;
	protected static String filename="./UserFiles/customers.txt";
	
	
	
	public Employees() {
		super("Employee");
	}

	public static void empOptions(UserParent emp) {
		if (!emp.getAccess().equals("Employee")&&!emp.getAccess().equals("Admin")) {
			System.out.println(emp.getAccess()+" " + emp.getUsername());
			System.out.println("Invalid account access");
			MainMenu(emp);
		}
		System.out.println("(1) Approve/Deny Accounts");
		System.out.println("(2) Get Active Accounts");
		System.out.println("(3) Find Customer Users");
		System.out.println("(4) Show All Customer Users");
		System.out.println("(5) Personal Account Options");
		System.out.println("Press Any Option To Go Back To Main Menu");
		int choice=scan.nextInt();
		switch (choice) {
		case 1:
			findUnapproved(emp);
			Admin.adminAccess(emp);
			break;
		case 2:
			findApproved(emp);
			Admin.adminAccess(emp);
			break;
		case 3:
			FindCust("Customer", emp, LoginPage.getCusts(emp));
			Admin.adminAccess(emp);
			break;
		case 4:
			showAllCusts(LoginPage.getCusts(emp), "Customer");
			Admin.adminAccess(emp);
			break;
		case 5:
			priorNext(emp);
			break;
		default:
			LoginPage.MainMenu(Indi);
			break;
		}
	}
	
	protected static void showAllCusts(ArrayList<UserParent> list, String access) {
		for(UserParent temp:list) {
			if(temp.getAccess().equals(access)) {
			System.out.println(temp);
			}
		}
	}
	
		static protected void FindCust(String access, UserParent admin, ArrayList<UserParent> custList) {

		System.out.println("(1) Get Account By Username");
		System.out.println("(2) Go Back");
		System.out.println("(3) Go To Home Page");
		int choice = scan.nextInt();
		switch(choice) {
		case 1:
			System.out.println("Enter Username: ");
			String name=scan.next();
			for (int i=0; i<custList.size();i++) {
				if(name.equalsIgnoreCase(custList.get(i).getUsername()) && custList.get(i).getAccess().equalsIgnoreCase(access)) {
					System.out.println(custList.get(i));
					getAccInfo(custList.get(i));
				}
			}
			FindCust(access,admin, custList);
			break;
		case 2:
			Admin.adminAccess(admin);
			break;
		case 3:
			LoginPage.MainMenu(Indi);
			break;
		
	}
}

}
