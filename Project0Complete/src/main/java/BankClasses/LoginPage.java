package BankClasses;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.InputMismatchException;

public class LoginPage extends UserParent{
	
	
	
	public static void main(String[] args) {
		try {
		UserParent User=new UserParent();
		MainMenu(User);
	}catch(Throwable e) {
		System.out.println("You Can't Hack Me!!!");
		LoginPage.main(args);
	}
	}
	
	
	static Employees empl;
	static Customer Indi;
	static UserParent User;
	static int i = 0;
	
	int n;
	int x;
	

	
	public static void MainMenu(UserParent Indi) {
		System.out.println("Please enter your option choice.");
		System.out.println("(1) Register for an Login"); 				
		System.out.println("(2) Customer Login");
		System.out.println("(3) Employee Login");
		System.out.println("(4) Admin Login");
		System.out.println("(5) Exit Application");
		
		int mainOption = scan.nextInt();
		switch (mainOption) {
		case 1:
			whichAccess();
			break;
		case 2:
			priorNext(login(Indi));
			break;
		case 3:
			Employees.empOptions(login(empl));
			break;
		case 4:
			Admin.adminAccess(login(Indi));
			break;
		case 5:
			System.out.println("GoodBye");
			
			break;
		default:
			System.out.println("Please Make A Valid Choice");
			MainMenu(Indi);
			break;
		}
	}
	
	public static void priorNext(UserParent User) {
		System.out.println("(0) Create Account");
		System.out.println("(1) Select Account");
		System.out.println("Press Any Other Key To Go Back");
		int choice = scan.nextInt();
		switch(choice) {
		case 0:
			System.out.println("Make (0)Checking or (1)Savings Account?");
			int select=scan.nextInt();
			switch(select) {
			case 0:
				 commit(User, 2, "Checking");
				MainMenu(User);
				break;
			case 1:
				 commit(User, 2, "Saving");
				MainMenu(User);
				break;
			default:
				break;
			}
		case 1:
			 getAcc(User, getAccInfo(User));
			 nextOption(User);
			break;
		default:
			MainMenu(User);
			break;
		}
	}

	public static void nextOption(UserParent Indi) {
		System.out.println("(1) Make Deposit");
		System.out.println("(2) Make Withdrawal");
		System.out.println("(3) Get Account Information");
		System.out.println("(4) Make Joint Account");
		System.out.println("(5) Create Additional Accounts");
		System.out.println("(6) Transfer Between Accounts");
		System.out.println("(7) Change Password");
		System.out.println("(8) Go To Home Page");
		
		int nextOption = scan.nextInt();
		switch (nextOption) {
		case 1:
			Indi.makeDeposit(Indi);
			 updateAcc(Indi);
			nextOption(Indi);
			break;
		case 2:
			Indi.withdraw(Indi);
			 updateAcc(Indi);
			nextOption(Indi);
			break;
		case 3:
			Indi.speak();
			nextOption(Indi);
			break;
		case 4:
			 makejoint(Indi);
			nextOption(Indi);
			break;
		case 5:
			 commit(Indi, 6, null);
			break;
		case 6:
			transfer(Indi,login(Indi));
			nextOption(Indi);
			break;
		case 7:
			 changePass(Indi);
			nextOption(Indi);
			break;
		case 8:
			MainMenu(Indi);
			break;
		default:
			nextOption(Indi);
			break;
		}
	}
	static void whichAccess() {
		System.out.println("(1) New Customer Account");
		System.out.println("(2) New Employee Account");
		int choice=scan.nextInt();
		switch(choice) {
		case 1:
			UserParent Indi=new UserParent("Customer");
			System.out.println("Account Was Created");
			System.out.println(Indi.getAccFirstName()+" "+Indi.getAccLastName()+" "+Indi.getUsername()+" "+Indi.getPassword()+ " "+Indi.getAccess());
			 commit(Indi, 1, null);
			MainMenu(Indi);
			break;
		case 2:
			empl=new Employees();
			System.out.println("Account Was Created");
			 commit(empl, 1, null);
			MainMenu(empl);
			break;
		}
	}
	
	LoginPage(){}
	
	LoginPage(String name){
		super(name);
	}
	
	
	private static String url=
			"jdbc:oracle:thin:@dbdiddy.czxlfsyznyrj.us-east-2.rds.amazonaws.com:1521:orcl";
	private static String username="Project0";
	private static String password="p4ssw0rd";
	
	
	private static void makeUser(UserParent User) {
		String sql="{ call make_user(?,?,?,?,?)}";
		try (Connection conn= DriverManager.getConnection(url, username, password)){
			CallableStatement cs=conn.prepareCall(sql);
			cs.setString(1, User.getUsername());
			cs.setString(2, User.getAccFirstName());
			cs.setString(3, User.getAccLastName());
			cs.setString(4, User.getPassword());
			cs.setString(5, User.getAccess());
			int status = cs.executeUpdate();			
		}catch(SQLException s) {
		System.out.println("UserName Already Exists");
		}catch(InputMismatchException s) {
			System.out.println("System Error!!!###");
		}
	}
	
	
	private static void makeAcc(UserParent User, String type) {
		double accBal=User.getAccBalance();
		String sql="{ call make_acc(?,?,?,?)}";
		try (Connection conn= DriverManager.getConnection(url, username, password)){
			CallableStatement cs=conn.prepareCall(sql);
			cs.setDouble(1, accBal);
			cs.setString(2, type);
			cs.setString(3, User.isApproved());
			cs.setString(4, User.getUsername());
			int status = cs.executeUpdate();		
		}catch(SQLException s) {
			System.out.println("Account Could Not Be Made");
		}	
	}
	
	static void updateAcc(UserParent User) {
		int acc=User.getAccNumber();
		double bal=User.getAccBalance();
		String sql="{ call update_bal(?,?) }";
		try (Connection conn= DriverManager.getConnection(url, username, password)){
			CallableStatement cs=conn.prepareCall(sql);
			cs.setInt(1, acc);
			cs.setDouble(2, bal);
			int status = cs.executeUpdate();			
		}catch(SQLException s) {
		System.out.println("Could Not Update Balance");
		}	
	}
	
	public static void makejoint(UserParent User) {
		if(User.getAccess().equalsIgnoreCase("Customer")) {
			Customer cust=new Customer();
			cust.setAccNumber(User.getAccNumber());
			makeUser(cust);
			String sql="{ call make_joints(?,?) }";
			try (Connection conn= DriverManager.getConnection(url, username, password)){
				CallableStatement cs=conn.prepareCall(sql);
				cs.setInt(1, cust.getAccNumber());;
				cs.setString(2, cust.getUsername());
				int status = cs.executeUpdate();	
			}catch(SQLException s) {
			System.out.println("Could Not Find Account");
			}
		}else if(User.getAccess().equalsIgnoreCase("Employee")) {
			Employees cust=new Employees();
				int acc=(User).getAccNumber();
				makeUser(cust);
				String sql="{ call make_joints(?,?) }";
				try (Connection conn= DriverManager.getConnection(url, username, password)){
					CallableStatement cs=conn.prepareCall(sql);
					cs.setInt(1, acc);
					cs.setString(2, cust.getUsername());
					int status = cs.executeUpdate();	
				}catch(SQLException s) {
					System.out.println("Could Not Find Account");
				}	
			}else {
				System.out.println("Could Not Find Account");;
					LoginPage.nextOption(User);
				}
			}
	public static void deleteAcc(int accnum) {
	try (Connection conn= DriverManager.getConnection(url, username, password)){
		String sql="{ Call delete_Accs(?)";
		CallableStatement cs=conn.prepareCall(sql);
		cs.setInt(1, accnum);
	}catch(SQLException e) {
		System.out.println("Cannot Delete Account");
	}
	}
	
	public static int getAccInfo(UserParent User) {
		int i=1;
		ArrayList<UserParent> list=new ArrayList<>();
		try (Connection conn= DriverManager.getConnection(url, username, password)){
			String sql="Select * from user_acc A inner join bank_accounts B on A.acc_num=B.acc_num where username=?";
			PreparedStatement ps=conn.prepareStatement(sql);
			ps.setString(1, User.getUsername());
			ResultSet rs= ps.executeQuery();
			while(rs.next()) {
					list.add(new UserParent(rs.getInt(2),rs.getString(5),rs.getDouble(4), rs.getString(7)));
			}
			for(UserParent temp:list) {
				int x=list.indexOf(temp);
				if(temp.isApproved().equals("true")) {
				System.out.println("("+x+") "+temp.getAccNumber()+" "+temp.getAccType()+" Account Balance: "+temp.getAccBalance());
				}
			}
			System.out.println("Which account would you like to edit?");
			int acc=scan.nextInt();
			return (list.get(acc).getAccNumber());
		}catch (SQLException e) {
			System.out.println("Could Not Find Account");
		}return 0;
	}
	

	
	public static void getAcc(UserParent User, int acc) {
		try (Connection conn= DriverManager.getConnection(url, username, password)){
			String sql="Select * from bank_accounts where acc_num=?";
			PreparedStatement ps=conn.prepareStatement(sql);
			ps.setInt(1, acc);
			ResultSet rs= ps.executeQuery();
			while(rs.next()) {
				if(rs.getDate(4).equals(null)) {
					getAccInfo(User);
				}else {
			User.setAccBalance(rs.getDouble(2));
			User.setAccType(rs.getString(3));
			User.setDate(rs.getDate(4));
			User.setAccNumber(acc);
			}			
			}
		}catch (SQLException e) {
			System.out.println("Could Not Find Account");;
		}
	}
	
	public static UserParent login(UserParent User) {
		UserParent User2=new UserParent();
		System.out.println("Enter Username");
		String name=scan.next();
		System.out.println("Enter Password");
		String pass=scan.next();
		try (Connection conn= DriverManager.getConnection(url, username, password)){
			String sql="Select * from bank where username=?";
			PreparedStatement ps=conn.prepareStatement(sql);
			ps.setString(1,name);
			ResultSet rs= ps.executeQuery();
			while(rs.next()) {
				if(pass.equals(rs.getString(4))){
					User2.setAccFirstName(rs.getString(2));
					User2.setAccLastName(rs.getString(3));
					User2.setUsername(rs.getString(1));
					User2.setPassword(rs.getString(4));
					User2.setAccess(rs.getString(5));
					return User2;
				}else {
					System.out.println("Incorrect Username or password 1");
					LoginPage.MainMenu(User);
					return null;
				}
			}
			LoginPage.MainMenu(User);
		}catch (SQLException e) {
			System.out.println("Incorrect Username or Password 2");
			LoginPage.MainMenu(User);
			return null;
		}catch (NullPointerException e) {
			System.out.println("Could Not Find Account");
			return User;
		}
		return User;
	}
	
	protected static void findUnapproved(UserParent emp) {
		try (Connection conn= DriverManager.getConnection(url, username, password)){
			String sql="Select * from bank_accounts where visible='false'";
			PreparedStatement ps=conn.prepareStatement(sql);
			ResultSet rs= ps.executeQuery();
			while(rs.next()) {
				System.out.println(rs.getInt(1)+" "+rs.getString(3)+" "+rs.getDate(4));
			}
			System.out.println("(0) Set All To Visible");
			System.out.println("(1) Select Account to Make Visible");
			System.out.println("(2) Deny Accounts");
			int choose=scan.nextInt();
			switch(choose) {
			case 0:
				set_all(emp);
				break;
			case 1:
				System.out.println("Enter Account Number");
				int choice=scan.nextInt();
				appAccs(choice, emp);
				break;
			case 2:
				System.out.println("Which Account Would You Like To Deny");
				int num=scan.nextInt();
				deleteAcc(num);
				break;
			}
		}catch (SQLException e) {
			System.out.println("No accounts found");
			MainMenu(emp);
		}
	}
	
	public static void set_all(UserParent User) {
		try (Connection conn= DriverManager.getConnection(url, username, password)){
			String sql="{ call set_all}";
			CallableStatement cs= conn.prepareCall(sql);
			int status = cs.executeUpdate();
	}catch(SQLException e) {
		System.out.println("Could Not Find Account");
		}
	}
	
	
	protected static void appAccs(int num, UserParent emp) {
		try (Connection conn= DriverManager.getConnection(url, username, password)){
			String sql="{ call set_some(?)}";
			CallableStatement cs= conn.prepareCall(sql);
			cs.setInt(1, num);
			int status = cs.executeUpdate();
			}catch (SQLException e) {
			System.out.println("No Accounts Found");
		}
	}
	
	
	protected static void findApproved(UserParent emp) {
		try (Connection conn= DriverManager.getConnection(url, username, password)){
			String sql="select * from bank A inner join user_acc B on A.username=B.username";
			PreparedStatement ps=conn.prepareStatement(sql);
			ResultSet rs= ps.executeQuery();
			while(rs.next()) {
			System.out.println(rs.getString(1)+" "+rs.getInt(7));
			}
		}catch (SQLException e) {
			System.out.println("No Accounts Found or Found Serious Error again");
			e.printStackTrace();
		}
	}
	
	public static void changePass(UserParent User) {
		System.out.println("Enter New Password");
		String pass=scan.next();
		try (Connection conn= DriverManager.getConnection(url, username, password)){
			String sql="update bank set password=? where username=?";
			PreparedStatement ps=conn.prepareStatement(sql);
			ps.setString(1, pass);
			ps.setString(2, User.getUsername());
			ResultSet rs= ps.executeQuery();
			while(rs.next()) {
			System.out.println("Password Change Successful");
			nextOption(User);
			}
			System.out.println("Set 'em!!!");
		}catch (SQLException e) {
			System.out.println("No Accounts Found or Found Serious Error again");
			nextOption(User);
		}
	}
	
	public static ArrayList<UserParent> getCusts(UserParent User) {
		ArrayList<UserParent> list=new ArrayList<>();
		try (Connection conn= DriverManager.getConnection(url, username, password)){
			String sql="select * from bank";
			PreparedStatement ps=conn.prepareStatement(sql);
			ResultSet rs= ps.executeQuery();
			while(rs.next()) {
				list.add(new UserParent(rs.getString(1),rs.getString(2), rs.getString(3), rs.getString(5)));
			}return list;
		}catch(SQLException e) {
			System.out.println("Could Not Find Account");;
		}
		return list;
	}
	

	public static void commit(UserParent User, int num, String type) {
		switch(num) {
		case 1:
			makeUser(User);
			break;
		case 2:
			makeAcc(User, type);
			break;
		case 3:
			updateAcc(User);
			break;
		case 4:
			makejoint(User);
			break;
		case 5:
			getAccInfo(User);
			break;
		case 6:
			LoginPage.priorNext(User);
			break;
		default:
			System.out.println("System Error");
			LoginPage.MainMenu(User);
			break;
			
		}
	}

	
}
