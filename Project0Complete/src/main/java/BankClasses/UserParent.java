package BankClasses;

import java.util.Scanner;

public class UserParent {
	
	
	protected String approved = "false";
	private int accNumber;
	protected double accBalance=0.0; 
	private String accFirstName;
	private String accLastName;
	private String username;
	private String password;
	private String access;
	private String accType;
	static Scanner scan=new Scanner(System.in);
	private java.sql.Date date;
	
	protected java.sql.Date getDate() {
		return this.date;
	}
	
	protected void setDate(java.sql.Date date) {
		this.date=date;
	}
	
	protected String isApproved() {
		return approved;
	}

	protected void setApproved(String approved) {
		this.approved = approved;
	}

	protected int getAccNumber() {
		return accNumber;
	}

	protected void setAccNumber(int accNumber) {
		this.accNumber = accNumber;
	}

	protected double getAccBalance() {
		return accBalance;
	}

	protected void setAccBalance(double accBalance) {
		this.accBalance = accBalance;
	}

	
	protected String getAccFirstName() {
		return accFirstName;
	}

	protected void setAccFirstName(String name) {
		this.accFirstName=name;
	}
	
	protected void setAccFirstName() {
		System.out.println("Please Enter First Name");
		String first=scan.next();
		this.accFirstName = first;
	}

	protected String getAccLastName() {
		return accLastName;
	}

	protected void setAccLastName(String name) {
		this.accLastName=name;
	}
	
	protected void setAccLastName() {
		System.out.println("Please Enter Last Name");
		String last=scan.next();
		this.accLastName = last;
	}

	protected String getUsername() {
		return username;
	}

	protected void setUsername(String name) {
		this.username=name;
	}
	
	protected void setUsername() {
		System.out.println("Please Enter Username");
		String user=scan.next();
		this.username = user;
	}

	protected String getPassword() {
		return password;
	}
	
	protected void setPassword(String name) {
		this.password=name;
	}

	protected void setPassword() {
		System.out.println("Please Enter Password");
		String pass=scan.next();
		this.password = pass;
	}

	protected String getAccess() {
		return access;
	}

	protected void setAccess(String access) {
		this.access = access;
	}

	protected String getAccType() {
		return accType;
	}

	protected void setAccType(String accType) {
		this.accType = accType;
	}

	protected UserParent() {
	}

	protected UserParent(String access) {
		setAccFirstName();
		setAccLastName();
		setUsername();
		setPassword();
		setAccess(access);
	}
	protected void createAcc(UserParent User, String accType) {
		User.setAccBalance(0.0);
		User.setAccType(accType);
	}
	
	
	
	protected double deposit(double x) {
		if (x <= 0) {
			System.out.println("Input Incorrect");
			return 0.0;
		} else {
			this.setAccBalance(this.getAccBalance() +x);
			return this.getAccBalance();
		}
	}

	protected double takeOut(double x) {
		if (x <= 0) {
			System.out.println("Input Is Incorrect");
			return 0.0;
		} else if (x > this.getAccBalance()) {
			System.out.println("Withdrawal Amount Is Above Available Balance");
			return 0.0;
		} else {
			this.setAccBalance(this.getAccBalance() - x);
			return this.getAccBalance();
		}
	}

	public void speak() {
		System.out.println("Your name is: " + getUsername());
		System.out.println(
				"Your account was made: " + getDate());
		System.out.println("Your account number is: " + getAccNumber());
		System.out.println("Your account balance is: " + getAccBalance());
	}

	

	public static void transfer(UserParent User, UserParent User2) {
		System.out.println("Which Account Would You Like To Transfer From?");
		LoginPage.getAcc(User, LoginPage.getAccInfo(User));
		System.out.println("Which Account Would You Like To Transfer To?");
		LoginPage.getAcc(User2, LoginPage.getAccInfo(User2));
		System.out.println("How much would would you like to transfer?: ");
		double y = scan.nextDouble();
		System.out.println("Transfer amount: "+y);
		User.takeOut(y);
		User2.deposit(y);
		System.out.println("You are transferring: " + y);
		System.out.println("You're new account balance is: " + User.getAccNumber()+" "+User.getAccType()+" Account Balance: "+User.getAccBalance());
		System.out.println("You're new account balance is: " + User2.getAccNumber()+" "+User2.getAccType()+" Account Balance: "+User2.getAccBalance());
		LoginPage.updateAcc(User2);
		LoginPage.updateAcc(User);
	}
	
	public void makeDeposit(UserParent Indi) {
		System.out.println("How much would you like to deposit?: ");
		double x = scan.nextDouble();
		System.out.println("Current account balance is: " + Indi.getAccBalance());
		deposit(x);
		System.out.println("You are making a deposit of " + "$" + x);
		System.out.println("You're new account balance is: " + "$" + Indi.getAccBalance());
		// syncAcc(Indi);
	}

	public void withdraw(UserParent Indi) {
		System.out.println("How much would would you like to withdraw?: ");
		// System.out.println("Withdrawal amount: ");
		// double x=getAccBalance()-y;
		double y = scan.nextDouble();
		takeOut(y);
		System.out.println("You are withdrawing: " + y);
		System.out.println("You're new account balance is: " + Indi.getAccBalance());
		// syncAcc(Indi);
	}
	

	@Override
	public String toString() {
			return "\n[" + getAccess() + ": Account Name= " + getUsername() + ", First Name: "+getAccFirstName()+", Last Name: "+getAccLastName();
		

	}

	UserParent (String User, String first, String last, String access){
		this.setUsername(User);
		this.setAccFirstName(first);
		this.setAccLastName(last);
		this.setAccess(access);
	}
	UserParent(int num, String type, double bal, String visible){
		this.setAccNumber(num);
		this.setAccBalance(bal);
		this.setAccType(type);
		this.setApproved(visible);
	}

}
