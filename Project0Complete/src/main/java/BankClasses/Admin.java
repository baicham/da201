package BankClasses;


public class Admin extends Employees{
	
	
	static Customer Indi;
	static Admin admin;
	
	
	static public void adminAccess(UserParent admin) {
		if (admin.getAccess().equals("Admin")) {
			System.out.println("Admin Menu");
			System.out.println("(1) Approve/Deny Accounts");
			System.out.println("(2) Get Active Accounts");
			System.out.println("(3) Delete Active Accounts");
			System.out.println("(4) Find Customer Users");
			System.out.println("(5) Find Employee Users");
			System.out.println("(6) Show All Customer Users");
			System.out.println("(7) Show All Employee Users");
			System.out.println("(8) Personal Account Options");
			System.out.println("Press Any Option To Go Back To Main Menu");
			int choice=scan.nextInt();
			switch (choice) {
			case 1:
				findUnapproved(admin);
				adminAccess(admin);
				break;
			case 2:
				findApproved(admin);
				adminAccess(admin);
				break;
			case 3:
				findApproved(admin);
				System.out.println("Enter Account Number You Would Like To Delete");
				int num=scan.nextInt();
				deleteAcc(num);
				adminAccess(admin);
				break;
			case 4:
				FindCust("Customer", admin, LoginPage.getCusts(admin));
				adminAccess(admin);
				break;
			case 5:
				FindCust("Employee", admin, LoginPage.getCusts(admin));
				adminAccess(admin);
			case 6:
				Employees.showAllCusts(LoginPage.getCusts(admin), "Customer");
				adminAccess(admin);
				break;
			case 7:
				Employees.showAllCusts(LoginPage.getCusts(admin), "Employee");
				adminAccess(admin);
				break;
			case 8:
				priorNext(admin);
				break;
			default:
				LoginPage.MainMenu(admin);
				break;
			}
		}else if(admin.getAccess().equals("Employee")){
			empOptions(admin);
		}else {
			System.out.println("Could Not Find Account");
			MainMenu(admin);
		}
	}	
}