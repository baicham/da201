console.log("js script is loaded")
window.onload = function() {
    document.getElementById("button")
        .addEventListener("click", qandA);


    document.getElementById("questionbutt").addEventListener("click", makeQuestion);
    console.log("question button is functional");
}
let i = 0;
let j = 0;
let answer = document.createElement("input");
let ansField = document.getElementById("answerHere");
let enter = document.getElementById("startover");

function startOver() {
    enter.innerHTML = "This is the last question. Click here to start over.";
    enter.setAttribute("type", "submit");
    console.log("The function to start over has been called.");
}
enter.onclick = function() {
    i = 0;
    j = 0;
    enter.innerHTML = "";
    enter.setAttribute("type", "hidden");
    console.log("reset attribute")
}

function makeQuestion() {
    console.log("makeQuestion has been called");
    let getobj = new XMLHttpRequest;

    getobj.onreadystatechange = function() {

        console.log("first ready state function is working");

        if (getobj.readyState == 4 && getobj.status == 200) {

            let info = JSON.parse(getobj.responseText);


            let ques = info.Terms.javascript[i].definition;
            document.getElementById("question").innerHTML = ques
                //creation and insertion of question

            //creation of answer field

            ansField.appendChild(answer);
            answer.setAttribute("type", "text");
            answer.setAttribute("id", "textans");
            answer.setAttribute("placeholder", "ANSWER HERE");
            document.getElementById("button").setAttribute("type", "submit");
            document.getElementById("questionbutt").setAttribute("type", "hidden");






        }
    }
    getobj.open("GET", "https://api.myjson.com/bins/1ebq6o");
    getobj.send();
}



function qandA() {
    let getobj = new XMLHttpRequest;


    console.log("qandA has been called");

    getobj.onreadystatechange = function() {

        console.log("Ready state is changing");

        if (getobj.readyState == 4 && getobj.status == 200) {
            console.log("ready state is 4 and status is 200");
            let info = JSON.parse(getobj.responseText);


            if (i == info.Terms.length - 1) {
                startOver();
            }


            let textans = document.getElementById("textans").value; //text field
            let rOrW = document.getElementById("rOrW") //where we tell them right or wrong
            console.log("about to enter insertrOrW");
            insertrOrW();


            function insertrOrW(a) { //function to insert right or wrong
                rOrW.innerHTML = a;

                let ans = info.Terms.javascript[j].Term; //variable now equals actual answer
                document.getElementById("answerfield").innerHTML = ans //insert actual answer
                console.log("we are at  the answer check");
                if (textans.toLowerCase() == ans.toLowerCase()) { //compares typed to actual answer
                    rOrW.innerHTML = ("CorrectAnswer!") //if else statement to display right or wrong
                } else {
                    rOrW.innerHTML = ("Wrong Answer.")
                }
                console.log("if statement works");
                nextquestion();


                function nextquestion() {
                    let buttonelem = document.createElement("input");
                    let divbutt = document.getElementById("nextquestion")
                    divbutt.appendChild(buttonelem);
                    buttonelem.setAttribute("id", "nextbutt");
                    buttonelem.setAttribute("value", "Next Question");
                    buttonelem.setAttribute("type", "submit");
                    document.getElementById("nextbutt").onclick = function() {
                        i++;
                        j++;
                        console.log("i is " + i + ", j is " + j)
                        document.getElementById("question").innerHTML = "";
                        divbutt.removeChild(buttonelem);
                        document.getElementById("answerfield").innerHTML = "";
                        document.getElementById("rOrW").innerHTML = "";
                        ansField.removeChild(answer);
                        document.getElementById("button").setAttribute("type", "hidden");
                        document.getElementById("questionbutt").setAttribute("type", "submit");
                    }

                }
            }
        }
    }
    getobj.open("GET", "https://api.myjson.com/bins/1ebq6o");
    getobj.send();


}