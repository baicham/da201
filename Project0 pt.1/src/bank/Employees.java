package bank;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import java.util.ArrayList;
import java.util.Collections;


public class Employees extends UserParent {
	
	static Customer Indi;
	private static ArrayList<UserParent> custs=new ArrayList<>();
	protected static String filename="./UserFiles/customers.txt";
	
	
	
	public Employees() {
		super("Employee");
	}


	static public void emplAccess() {
		Employees emp = (Employees) login("Employee");
		if(emp==(null)) {
			LoginPage.MainMenu(Indi);
		}else {
			empOptions(emp);
		
}
	}
	
	public static void empOptions(Employees emp) {
		System.out.println("(1) Approve Accounts");
		System.out.println("(2) Get Active Accounts");
		System.out.println("(3) Find Customer Accounts");
		System.out.println("(4) Personal Account Options");
		System.out.println("Press Any Option To Go Back To Main Menu");
		int choice = scan.nextInt();
		switch (choice) {
		case 1:
			approveAccs("Customer");
			empOptions(emp);
			break;
		case 2:
			readAccs("Customer");
			empOptions(emp);
			break;
		case 3:
			FindCust("Customer");
			empOptions(emp);
			break;
		case 4:
			LoginPage.nextOption(emp);
		default:
			LoginPage.MainMenu(Indi);
			break;
		}
	}
	
	protected static void readAccs( String access) {
		ArrayList<UserParent> custList=readFromFile();
		for(int i=0; i<custList.size();i++) {
		if(custList.get(i).getAccess().equalsIgnoreCase(access)) {
			System.out.println(custList.get(i));
		}
		}
		
	}
	
	static protected void FindCust(String access) {

		System.out.println("(1) Get Account By Name");
		System.out.println("(2) Go Back To Admin Page");
		System.out.println("(3) Go To Home Page");
		int choice = scan.nextInt();
		switch(choice) {
		case 1:
			System.out.println("Enter Username: ");
			String name=scan.next();
			ArrayList<UserParent> custList=readFromFile();
			for (int i=0; i<custList.size();i++) {
				if(name.equalsIgnoreCase(custList.get(i).getUsername()) && custList.get(i).getAccess().equalsIgnoreCase(access)) {
					System.out.println(custList.get(i));
					custList.get(i).speak();
				}
			}
			FindCust(access);
			break;
		case 2:
			Admin.adminAccess();
			break;
		case 3:
			LoginPage.MainMenu(Indi);
			break;
		
	}
}
	static protected void approveAccs(String access) {
		for(int i=0; i<UserList.size(); i++) {
			if(UserList.get(i).getAccess().equalsIgnoreCase(access)){
				UserList.get(i).setApproved(true);
				custs.add(UserList.get(i));
				UserList.remove(i);
			}
		}System.out.println("Accounts written successfully.");
		WriteToFile(custs);
	}
	
	protected static ArrayList<UserParent> readFromFile() {
		try(ObjectInputStream read=new ObjectInputStream(new FileInputStream(filename))){
			Object User=(Object) read.readObject();
			ArrayList<UserParent> list=(ArrayList<UserParent>)User;
			Collections.sort(list, (person0, person1)
					->{return person0.getAccNumber()-(person1.getAccNumber());});
			return list;
		
		} catch (FileNotFoundException e) {
		
			e.printStackTrace();
		} catch (IOException e) {
		
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
		
			e.printStackTrace();
		}
		return null;
	}
	
	
	private static void WriteToFile(ArrayList User) {
		try(ObjectOutputStream write=new ObjectOutputStream(new FileOutputStream(filename))){
			
			write.writeObject(User);
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			
		}
	}
	
}
