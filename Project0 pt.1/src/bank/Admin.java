package bank;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Scanner;




public class Admin extends Employees{
	static Customer Indi;
	private static ArrayList<UserParent> custs=new ArrayList<>();
	
	
	static public void adminAccess() {
		System.out.println("(1) Approve Accounts");
		System.out.println("(2) Get Active Accounts");
		System.out.println("(3) Search Customer Accounts");
		System.out.println("(4) Search Employee Accounts");
		System.out.println("Press Any Option To Go Back To Main Menu");
		int choice=scan.nextInt();
		switch(choice) {
		case 1:
			approveOptions();
			break;
		case 2:
			getOptions();			
			break;
		case 3:
			Employees.FindCust("Customer");
			break;
		case 4:
			Employees.FindCust("Employee");
			break;
		default:
			LoginPage.MainMenu(Indi);
			break;
		}
	}
	
	
	
	
	
	
	static public void approveOptions() {
		System.out.println("(1) Approve Customer Accounts");
		System.out.println("(2) Approve Employee Accounts");
		System.out.println("(3) Go Back To Admin Page");
		int adminChoice=scan.nextInt();
		switch(adminChoice) {
		case 1:
			approveAccs("Customer");
			approveOptions();
			break;
		case 2:
			approveAccs("Employee");
			approveOptions();
			break;
		
		case 3: 
			adminAccess(); 
			break;
		default:
			System.out.println("Please Enter A Valid Option");
			approveOptions();								
			}
		
	}
	
	static public void getOptions() {
		System.out.println("(1) Get Customer Accounts");
		System.out.println("(2) Get Employee Accounts");
		System.out.println("(3) Go Back Admin Page");
		int secChoice=scan.nextInt();
		switch(secChoice) {
		case 1:
			readAccs("Customer");
			getOptions();
			break;
		case 2:
			readAccs("Employee");
			getOptions();
			break;
		case 3:
			adminAccess();
			break;
		default:
			System.out.println("Please Enter A Valid Option");
			getOptions();
			break;
		}

	}

	
	
	
	
}
