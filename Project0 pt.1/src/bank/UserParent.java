package bank;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.Scanner;

public class UserParent implements Serializable {
	private boolean approved = false;
	private int accNumber; // variable for account number
	private double accBalance; // variable for account balance
	private double accFees; // variable for account fees
	private int memberMonth; // variable for month account was made
	private int memberYear; // variable for year account was made
	private int memberDay; // variable for day of month account was made
	private String accName; // variable for name on account
	private String username; // variable for username
	private String password; // variable for password
	private String access;
	public static long serialVersionUID1 = 1L;
	boolean joint = false;
	String accType;
	private String joiName;
	private int mulAcc;
	private boolean sevAcc;
	private double secAccBal;
	private Date date = new Date();
	private static long serialVersionUID = 1L;
	static protected ArrayList<UserParent> UserList = new ArrayList<>();
	transient protected static Scanner scan = new Scanner(System.in); // variable for scanner class; to be inherited
	
	protected UserParent() {
	}

	protected UserParent(String access) {
		setAccName();
		setMemberDay(date.getDate());
		setMemberMonth(date.getMonth() + 1);
		setMemberYear(date.getYear() + 1900);
		makeAccNum();
		setAccBalance(0.00);
		setAccFees(0.00);
		setUsername();
		setPassword();
		setAccess(access);
		setAccType("Checking");
		setMulAcc(0);
		setSevAcc(false);
		setJoiName("null");
		
	}
	
	protected double getSecAccBal() {
		return secAccBal;
	}

	protected void setSecAccBal(double secAccBal) {
		this.secAccBal = secAccBal;
	}

	protected boolean isSevAcc() {
		return sevAcc;
	}

	protected void setSevAcc(boolean sevAcc) {
		this.sevAcc = sevAcc;
	}

	protected String getJoiName() {
		return joiName;
	}

	protected void setJoiName(String joiName) {
		this.joiName = joiName;
	}

	protected int getMulAcc() {
		return mulAcc;
	}

	protected void setMulAcc(int mulAcc) {
		this.mulAcc = mulAcc;
	}
	
	protected String getAccess() {
		return access;
	}

	private void setAccess(String access) {
		this.access = access;
	}

	protected String getUsername() {
		return username;
	}

	private void setUsername() {
		System.out.println("Set Username: ");
		String username = scan.next();
		this.username = username;
	}

	protected String getPassword() {
		return password;
	}

	private void setPassword() {
		System.out.println("Set Password: ");
		String password = scan.next();
		this.password = password;
	}

	protected int getAccNumber() { // getter for account number
		return accNumber; // should be able to get this when logging in
	}

	private void setAccNumber(int accNumber) { // setter for making account number
		this.accNumber = accNumber; // should be made once and be immutable thereafter
	}

	protected double getAccBalance() { // getter for account balance
		return accBalance; // should be able to get balance whenever need be
	}

	private void setAccBalance(double accBalance) { // setter for acc balance
		this.accBalance = accBalance; // should have a method for resetting once account is
										// made and whenever a transaction occurs
	}

	protected double getAccFees() { // should be able to see account fees applicable
		return accFees; // for the personal account
	}

	private void setAccFees(double accFees) { // setter for account fees
		this.accFees = accFees; // this should not be changed by the user, only auth personnel
	}

	protected int getMemberMonth() { // getter for month account was made
		return memberMonth; // for displaying information about the account
	}

	private void setMemberMonth(int memberMonth) { // setter for month account was made
		this.memberMonth = memberMonth; // only needs to be made once
	}

	protected int getMemberYear() { // getter for year account was made
		return memberYear; // for displaying information about the account
	}

	private void setMemberYear(int memberYear) { // setter for year account was made
		this.memberYear = memberYear; // only needs to be made once
	}

	protected int getMemberDay() { // getter for day of month account was made
		return memberDay; // for displaying information about the account
	}

	private void setMemberDay(int memberDay) { // setter for day of month account was made
		this.memberDay = memberDay; // only needs to be made once
	}

	protected String getAccName() { // getter for name on account
		return accName; // for displaying information about the account
	}

	protected void setAccName() { // setter for account name
		System.out.println("Enter Name: ");
		String name = scan.next(); // using scanner class for user input
		this.accName = name; // for setting the account name
	}

	protected boolean isApproved() {
		return approved;
	}

	protected void setApproved(boolean approved) {
		this.approved = approved;
	}

	protected void makeAccNum() { // setter for making account number
		setAccNumber(getAccName().hashCode()); // takes the name on account and uses hashcode
		if (getAccNumber() < 0) {
			setAccNumber(getAccNumber() * (-1));
		}

	}
	


	protected String getAccType() {
		return accType;
	}

	protected void setAccType(String accType) {
		this.accType = accType;
	}

	protected boolean isJoint() {
		return joint;
	}

	protected void setJoint(boolean joint) {
		this.joint = joint;
	}

	protected double deposit(double x) {
		if (x <= 0) {
			System.out.println("Input Incorrect");
			return 0.0;
		} else {
			this.setAccBalance(this.getAccBalance() + x);
			return this.getAccBalance();
		}
	}

	protected double takeOut(double x) {
		if (x <= 0) {
			System.out.println("Input Is Incorrect");
			return 0.0;
		} else if (x > this.getAccBalance()) {
			System.out.println("Withdrawal Amount Is Above Available Balance");
			return 0.0;
		} else {
			this.setAccBalance(this.getAccBalance() - x);
			return this.getAccBalance();
		}
	}

	public void speak() {
		System.out.println("Your name is: " + getAccName());
		System.out.println(
				"Your account was made on: " + getMemberMonth() + "/" + getMemberDay() + "/" + getMemberYear());
		System.out.println("Your account number is: " + getAccNumber());
		System.out.println("Your account balance is: " + getAccBalance());
	}

	protected static UserParent login(String access) {
		ArrayList<UserParent> custList = Admin.readFromFile();
		System.out.println("Enter Username: ");
		String inputUsername = UserParent.scan.next();
		System.out.println("Enter Password: ");
		String inputPass = UserParent.scan.next();

		for (int i = 0; i < custList.size(); i++) {
			if (custList.get(i).getAccess().equalsIgnoreCase(access)
					&& inputUsername.equals(custList.get(i).getUsername())
					&& inputPass.equals(custList.get(i).getPassword())) {
				custList.get(i).speak();

				return custList.get(i);
			}
		}
		System.out.println("Incorrect Username or Password");
		return null;
	}

	public void transfer(UserParent Indi) {
		System.out.println("How much would would you like to transfer?: ");
		// System.out.println("Withdrawal amount: ");
		// double x=getAccBalance()-y;
		double y = scan.nextDouble();
		takeOut(y);
		Indi.setSecAccBal(y+Indi.getSecAccBal());
		System.out.println("You are transferring: " + y);
		System.out.println("You're new account balance is: " + getAccBalance());
		System.out.println("Your savings account balance is: "+Indi.getSecAccBal());
		// syncAcc(Indi);
	}
	public void makeDeposit(UserParent Indi) {
		System.out.println("How much would you like to deposit?: ");
		double x = scan.nextDouble();
		deposit(x);
		System.out.println("Current account balance is: " + getAccBalance());
		System.out.println("You are making a deposit of " + "$" + x);
		System.out.println("You're new account balance is: " + "$" + getAccBalance());
		// syncAcc(Indi);
	}

	public void withdraw(UserParent Indi) {
		System.out.println("How much would would you like to withdraw?: ");
		// System.out.println("Withdrawal amount: ");
		// double x=getAccBalance()-y;
		double y = scan.nextDouble();
		takeOut(y);
		System.out.println("You are withdrawing: " + y);
		System.out.println("You're new account balance is: " + getAccBalance());
		// syncAcc(Indi);
	}

	@Override
	public String toString() {

		if (joint) {
			return "\n[" + getAccess() + ": Account Name= " + getAccName() + ", Joint Account"
					+ ", Other Names on Account= " + getJoiName() + ",  Account type= " + getAccType() + ", Username= "
					+ getUsername() + ", Password= " + getPassword() + ",\n Customer Account Number= " + getAccNumber()
					+ ", Account Balance= " + getAccBalance() + ", Member Since= " + getMemberMonth() + "/"
					+ getMemberDay() + "/" + getMemberYear() + "]\n";
		} else if (joint && sevAcc) {
			return "\n[" + getAccess() + ": Account Name= " + getAccName() + ", Joint Account"
					+ ", Other Names on Account= " + getJoiName() + ", Account type= " + getAccType() + ", Username= "
					+ getUsername() + ", Password= " + getPassword() + ",\nCustomer Account Number= " + getAccNumber()
					+ ", Account Balance: "+getAccBalance()+",Savings Account Number= " + getMulAcc() + ", Savings Account Balance= " + getSecAccBal()
					+ ", Member Since= " + getMemberMonth() + "/" + getMemberDay() + "/" + getMemberYear() + "]\n";

		} else if (sevAcc) {
			return "\n[" + getAccess() + ": Account Name= " + getAccName() + ", Account type= " + getAccType()
					+ ", Username= " + getUsername() + ", Password= " + getPassword() + ",\nCustomer Account Number= "
					+ getAccNumber() +", Account Balance: "+getAccBalance()+ ",Savings Account Number= " + getMulAcc() + ", Savings Account Balance= "
					+ getSecAccBal() + ", Member Since= " + getMemberMonth() + "/" + getMemberDay() + "/"
					+ getMemberYear() + "]\n";
		} else {
			return "\n[" + getAccess() + ": Account Name= " + getAccName() + ", Account type= " + getAccType()
					+ ", Username= " + getUsername() + ", Password= " + getPassword() + ", \nCustomer Account Number= "
					+ getAccNumber() + ", Account Balance= " + getAccBalance() + ", Member Since= " + getMemberMonth()
					+ "/" + getMemberDay() + "/" + getMemberYear() + "]\n";
		}

	}

	/*
	 * protected static void syncAcc(UserParent user) { if (user.isJoint()) { for
	 * (int i = 0; i < Employees.readFromFile().size(); i++) { if
	 * (user.getAccNumber() + 1 == Employees.readFromFile().get(i).getAccNumber() &&
	 * !user.getAccName().equalsIgnoreCase(Employees.readFromFile().get(i).
	 * getAccName())) {
	 * Employees.readFromFile().get(i).setAccBalance(user.getAccBalance());
	 * UserList.add(Employees.readFromFile().get(i)); } } } }
	 */

}
