package bank;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import java.util.Scanner;

public class LoginPage extends UserParent{
	
	public static void main(String[] args) {
		save();
		MainMenu(Indi);
	}
	
	
	
	
	static Employees empl;
	static Customer Indi;
	
	static Scanner scan = new Scanner(System.in);
	static int i = 0;
	int n;
	int x;
	

	
	public static void MainMenu(UserParent Indi) {
		//Employees.saveFile();
		System.out.println("Please enter your option choice.");
		System.out.println("(1) Register for an Account"); 				// need to make a method for making joint account and
																		// transferring between multiples, making multiples with different acc
																
		
			
		System.out.println("(2) Customer Login"); 						// need method for checking account existence and displaying multiple accs

		System.out.println("(3) Employee Login"); 						// need method for logging in as employees or admin, with tools and
																		// tying them to writing to files for pending accs

		System.out.println("(4) Admin Login");
		System.out.println("(5) Exit Application");
		
		int mainOption = scan.nextInt();
		switch (mainOption) {
		case 1:
			whichAccess();
			break;
		case 2:
			Customer.custAccess(Indi);
			break;
		case 3:
			Employees.emplAccess();
			break;
		case 4:
			Admin.adminAccess();
			break;
		case 5:
			System.out.println("GoodBye");
			break;
		default:
			System.out.println("Please Make A Valid Choice");
			MainMenu(Indi);
			break;
		}
	}

	public static void nextOption(UserParent Indi) {
		System.out.println("(1) Make Deposit");
		System.out.println("(2) Make Withdrawal");
		System.out.println("(3) Get Account Information");
		System.out.println("(4) Make Joint Account");
		System.out.println("(5) Create Additional Accounts");
		System.out.println("(6) Transfer Between Accounts");
		System.out.println("(7) Go To Home Page");
		if(Indi.getAccess().equalsIgnoreCase("Employee")&&Indi.isApproved()==true) {
			System.out.println("(8) Employee Options");
		}
		
		

		int nextOption = scan.nextInt();
		switch (nextOption) {
		case 1:
			Indi.makeDeposit(Indi);
			nextOption(Indi);
			break;
		case 2:
			Indi.withdraw(Indi);
			nextOption(Indi);
			break;
		case 3:
			Indi.speak();
			nextOption(Indi);
			break;
		case 4:
			if(Indi.getAccess().equalsIgnoreCase("Customer")) {
				System.out.println("Please Enter Other Users");
				String name=scan.next();
				Indi.setJoiName(name);
				Indi.setJoint(true);
				nextOption(Indi);
			}else if(Indi.getAccess().equalsIgnoreCase("Employee")) {
				System.out.println("Please Enter Other Users");
				String name=scan.next();
				Indi.setJoiName(name);
				Indi.setJoint(true);
				nextOption(Indi);
			}else {
				
				System.out.println("You Must First Login");
				nextOption(Indi);
			}
		case 5:
			if(Indi.getAccess().equalsIgnoreCase("Customer")){
				Indi.setMulAcc(Indi.getAccNumber()+1);
				Indi.setSevAcc(true);
				nextOption(Indi);
			}else if(Indi.getAccess().equalsIgnoreCase("Employee")) {
				Indi.setMulAcc(Indi.getAccNumber()+1);
				Indi.setSevAcc(true);
				nextOption(Indi);
			}else {
				System.out.println("You Must First Login");
				nextOption(Indi);
			}
		case 6:
			Indi.transfer(Indi);
			nextOption(Indi);
		case 7:
			UserList.add(Indi);
			MainMenu(Indi);
			break;
		case 8:
			if(Indi.getAccess().equalsIgnoreCase("Employee")&&Indi.isApproved()==true) {
				Employees.empOptions((Employees)Indi);
			}else {
				System.out.println("Please Make Valid Decision");
				nextOption(Indi);
			}
		default:
			UserList.add(Indi);
			MainMenu(Indi);
			break;
		}
	}
	static void whichAccess() {
		System.out.println("(1) New Customer Account");
		System.out.println("(2) New Employee Account");
		int choice=scan.nextInt();
		switch(choice) {
		case 1:
			Indi = new Customer();
			Indi.speak();
			nextOption(Indi);
			break;
		case 2:
			empl=new Employees();
			empl.speak();
			nextOption(empl);
			break;
			
		}
	}
	
	protected static void save() {
		String filename="./UserFiles/customers.txt";
		try(ObjectInputStream read=new ObjectInputStream(new FileInputStream(filename))){
			Object User=(Object) read.readObject();
			UserList=(ArrayList<UserParent>)User;
		
		} catch (FileNotFoundException e) {
		
			e.printStackTrace();
		} catch (IOException e) {
		
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
		
			e.printStackTrace();
		}
		
	}
}
