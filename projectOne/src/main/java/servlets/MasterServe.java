package servlets;

import java.io.IOException;
import java.net.HttpRetryException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controllers.RequestHelp;

public class MasterServe extends HttpServlet{
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse  res) throws ServletException, IOException {
		req.getRequestDispatcher(RequestHelp.process(req)).forward(req,res);

	}

	
}
