package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import controllers.JsonReq;

public class MasterJSON extends HttpServlet{
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse  res) throws ServletException, IOException {
		System.out.println("in the json master");
		JsonReq.process(req, res);

	}



}
