package servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import controllers.LoginController;
import controllers.NewUserController;

public class RequestHelp {
	
	public static String process(HttpServletRequest req) throws IOException, ServletException {
		System.out.println(req.getRequestURI()+"IN HELPER");
		switch(req.getRequestURI()) {
		case "/projectOne/html/login.go":
			return LoginController.login(req);
		case "/projectOne/html/makenew.go":
			return NewUserController.makeUser(req);
		}
		return "homepage.html";
	}

}
