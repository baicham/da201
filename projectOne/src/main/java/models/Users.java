package models;

import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

public class Users implements HttpSessionBindingListener{
	private int user_id;
	private String username;
	private String password;
	private String first_name;
	private String last_name;
	private String email;
	private String role;
	private int role_id;
	@Override
	public void valueBound(HttpSessionBindingEvent event) {
		// TODO Auto-generated method stub
		HttpSessionBindingListener.super.valueBound(event);
	}
	public Users() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Users(int user_id, String username, String password, String first_name, String last_name, String email,
			String role, int role_id) {
		super();
		this.user_id = user_id;
		this.username = username;
		this.password = password;
		this.first_name = first_name;
		this.last_name = last_name;
		this.email = email;
		this.role = role;
		this.role_id=role_id;
	}

	public Users(String username2, String password2, String firstname, String lastname, String email2,
			String role_id2) {
		this.username=username2;
		this.password=password2;
		this.first_name=firstname;
		this.last_name=lastname;
		this.email=email2;
		this.role=role_id2;
	}
	
	public Users(String username, String password, String first_name, String last_name, String email, int role_id) {
		super();
		this.username = username;
		this.password = password;
		this.first_name = first_name;
		this.last_name = last_name;
		this.email = email;
		this.role_id=role_id;
	}

	public int getUser_id() {
		return user_id;
	}

	public void setUser_id(int user_id) {
		this.user_id = user_id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirst_name() {
		return first_name;
	}

	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}

	public String getLast_name() {
		return last_name;
	}

	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getRole() {
		return role;
	}

	public void setRole_id(String role) {
		this.role = role;
	}

	public void setRolenum(int role_num) {
		this.role_id=role_num;
	}
	
	public int getRolenum() {
		return role_id;
	}
	
	@Override
	public String toString() {
		return "[user_id=" + user_id + ", username=" + username + ", password=" + password + ", first_name="
				+ first_name + ", last_name=" + last_name + ", email=" + email + ", role=" + role + "]";
	}
	
	
}
