package models;

import java.sql.Blob;
import java.sql.Timestamp;;

public class Reimbursements {
	private int reimb_id;
	private double reimb_amount;
	private String reimb_submitted;
	private String reimb_resolved;
	private String reimb_description;
	private Blob reimb_reciept;
	private int reimb_author;
	private int reimb_resolver;
	private int reimb_status;
	private int reimb_type;
	private String status;
	private String type;
	private String author;
	private String resolver;
	
	

	public Reimbursements(int reimb_id, double reimb_amount, String reimb_submitted, String reimb_resolved,
			String reimb_description, int reimb_author, String status, String type, String author, String resolver) {
		super();
		this.reimb_id = reimb_id;
		this.reimb_amount = reimb_amount;
		this.reimb_submitted = reimb_submitted;
		this.reimb_resolved = reimb_resolved;
		this.reimb_description = reimb_description;
		this.reimb_author = reimb_author;
		this.status = status;
		this.type = type;
		this.author = author;
		this.resolver = resolver;
	}

	public Reimbursements(Users user, double amount, String desc, int type) {
		this.reimb_amount = amount;
		this.reimb_description = desc;
		this.reimb_type = type;
		this.reimb_author = user.getUser_id();
		this.reimb_status = 0;
	}

	public void setAuthor(String auth) {
		this.author = auth;
	}

	public String getAuthor() {
		return this.author;
	}

	public void setResolver(String res) {
		this.resolver = res;
	}

	public String getResolver() {
		return this.resolver;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public Reimbursements() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Reimbursements(int reimb_id, double reimb_amount, String reimb_submitted, String reimb_resolved,
			String reimb_description, Blob reimb_reciept, int reimb_author, int reimb_resolver, int reimb_status,
			int reimb_type) {
		super();
		this.reimb_id = reimb_id;
		this.reimb_amount = reimb_amount;
		this.reimb_submitted = reimb_submitted;
		this.reimb_resolved = reimb_resolved;
		this.reimb_description = reimb_description;
		this.reimb_reciept = reimb_reciept;
		this.reimb_author = reimb_author;
		this.reimb_resolver = reimb_resolver;
		this.reimb_status = reimb_status;
		this.reimb_type = reimb_type;
	}

	public Reimbursements(int reimb_id, double reimb_amount, String reimb_submitted, String reimb_resolved,
			String reimb_description, Blob reimb_reciept, int reimb_author, int reimb_resolver, String reimb_status,
			String reimb_type) {
		super();
		this.reimb_id = reimb_id;
		this.reimb_amount = reimb_amount;
		this.reimb_submitted = reimb_submitted;
		this.reimb_resolved = reimb_resolved;
		this.reimb_description = reimb_description;
		this.reimb_reciept = reimb_reciept;
		this.reimb_author = reimb_author;
		this.reimb_resolver = reimb_resolver;
		this.status = reimb_status;
		this.type = reimb_type;
	}

	public int getReimb_id() {
		return reimb_id;
	}

	public void setReimb_id(int reimb_id) {
		this.reimb_id = reimb_id;
	}

	public double getReimb_amount() {
		return reimb_amount;
	}

	public void setReimb_amount(double reimb_amount) {
		this.reimb_amount = reimb_amount;
	}

	public String getReimb_submitted() {
		return reimb_submitted;
	}

	public void setReimb_submitted(String reimb_submitted) {
		this.reimb_submitted = reimb_submitted;
	}

	public String getReimb_resolved() {
		return reimb_resolved;
	}

	public void setReimb_resolved(String reimb_resolved) {
		this.reimb_resolved = reimb_resolved;
	}

	public String getReimb_description() {
		return reimb_description;
	}

	public void setReimb_description(String reimb_description) {
		this.reimb_description = reimb_description;
	}

	public Blob getReimb_reciept() {
		return reimb_reciept;
	}

	public void setReimb_reciept(Blob reimb_reciept) {
		this.reimb_reciept = reimb_reciept;
	}

	public int getReimb_author() {
		return reimb_author;
	}

	public void setReimb_author(int reimb_author) {
		this.reimb_author = reimb_author;
	}

	public int getReimb_resolver() {
		return reimb_resolver;
	}

	public void setReimb_resolver(int reimb_resolver) {
		this.reimb_resolver = reimb_resolver;
	}

	public int getReimb_status_id() {
		return reimb_status;
	}

	public void setReimb_status_id(int reimb_status_id) {
		this.reimb_status = reimb_status_id;
	}

	public int getReimb_type_id() {
		return reimb_type;
	}

	public void setReimb_type_id(int reimb_type_id) {
		this.reimb_type = reimb_type_id;
	}

	@Override
	public String toString() {
		return "Reimbursements [reimb_id=" + reimb_id + ", reimb_amount=" + reimb_amount + ", reimb_submitted="
				+ reimb_submitted + ", reimb_resolved=" + reimb_resolved + ", reimb_description=" + reimb_description
				+ ", reimb_reciept=" + reimb_reciept + ", reimb_author=" + reimb_author + ", reimb_resolver="
				+ reimb_resolver + ", reimb_status=" + reimb_status + ", reimb_type=" + reimb_type + ", status="
				+ status + ", type=" + type + ", author=" + author + ", resolver=" + resolver + ", getAuthor()="
				+ getAuthor() + ", getResolver()=" + getResolver() + ", getStatus()=" + getStatus() + ", getType()="
				+ getType() + ", getReimb_id()=" + getReimb_id() + ", getReimb_amount()=" + getReimb_amount()
				+ ", getReimb_submitted()=" + getReimb_submitted() + ", getReimb_resolved()=" + getReimb_resolved()
				+ ", getReimb_description()=" + getReimb_description() + ", getReimb_reciept()=" + getReimb_reciept()
				+ ", getReimb_author()=" + getReimb_author() + ", getReimb_resolver()=" + getReimb_resolver()
				+ ", getReimb_status_id()=" + getReimb_status_id() + ", getReimb_type_id()=" + getReimb_type_id()
				+ ", getClass()=" + getClass() + ", hashCode()=" + hashCode() + ", toString()=" + super.toString()
				+ "]";
	}

}
