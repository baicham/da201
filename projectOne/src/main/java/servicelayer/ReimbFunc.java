package servicelayer;

import java.util.HashMap;

import org.apache.log4j.Logger;

import ersdao.ReimbDao;
import models.Reimbursements;

public class ReimbFunc {
	ReimbDao rd=new ReimbDao();
	static ReimbDao reimb=new ReimbDao();
	final static Logger logger=Logger.getLogger(UserFunc.class);
	
	@SuppressWarnings("unchecked")
	static HashMap<Integer, Reimbursements> reimbMap=reimb.getAll();
	
	public static Reimbursements getReimb(int id) {
		if(reimbMap.containsKey(id)) {
			logger.info("Reimbursement number: "+id+" has been pulled");
			return reimbMap.get(id);
		}return null;
	}
	
	public static void listAll() {
		
	}
	public static void insertReim(Reimbursements reim) {
		ReimbDao.makeReim(reim);
	}

	
	
}
