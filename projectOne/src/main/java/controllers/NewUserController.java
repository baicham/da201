package controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ersdao.UserDao;
import models.Users;

public class NewUserController {
	
	public static String makeUser(HttpServletRequest req) {
		String username= req.getParameter("username");
		String password= req.getParameter("password");
		String firstname= req.getParameter("firstname");
		String lastname= req.getParameter("lastname");
		String email= req.getParameter("email");
		Users user=new Users(username, password, firstname, lastname, email, 1);
		UserDao.addOne(user);
		System.out.println("added user");
		return "goBack.go";
	}
	

	public static String updateUser(HttpServletRequest req) {
		Users part=SessionServe.user;
		String username= req.getParameter("username");
		String password= req.getParameter("password");
		String firstname= req.getParameter("firstname");
		String lastname= req.getParameter("lastname");
		if(username.equals("")) {
		}else {
			part.setUsername(username);
			}
		if(password==null) {
		}else {
			part.setPassword(password);
		}
		if(firstname==null) {
		}else {
			part.setFirst_name(firstname);
		}
		if(lastname==null) {
		}else {
			part.setLast_name(lastname);
		}
		System.out.println(part);
		UserDao.updateUser(part);
		System.out.println("updated user");
		return "goBack.go";
		}
	
	
	
}
