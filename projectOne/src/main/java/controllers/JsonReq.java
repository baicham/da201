package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.Users;

public class JsonReq {

	public static void process(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
		System.out.println(req.getRequestURI());
		System.out.println(SessionServe.user);
		Users man=SessionServe.user;
		System.out.println(man);
		switch (req.getRequestURI()) {
		case "/projectOne/resources/user.JSON":
			SessionJson.getCustomJson(req, res);
			break;
		case "/projectOne/resources/get.JSON":
			if (SessionServe.user.getRole().equalsIgnoreCase("employee")) {
				DirectJson.sendSome(req, res);
			} else {
				DirectJson.sendAll(req, res);
			}
			break;
		case "/projectOne/resources/getOne.JSON":
			DirectJson.sendSome(req, res);
			break;
		case "/projectOne/resources/getall.JSON":
			System.out.println("working");
			DirectJson.sendAll(req, res);
			break;
			
		}
	}

}
