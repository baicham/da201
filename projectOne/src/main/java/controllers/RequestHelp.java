package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.catalina.Session;

public class RequestHelp {
	static SessionServe ss = new SessionServe();
	public static String process(HttpServletRequest req) throws IOException, ServletException {
		System.out.println(req.getRequestURI() + "IN HELPER");
		switch (req.getRequestURI()) {
		case "/projectOne/html/login.go":
			return SessionServe.makeSess(req);
		case "/projectOne/html/makeNew.go":
			if(SessionServe.user==null) {
				return "myIndex.html";
			}
			return "newPage.html";
		case "/projectOne/html/reimb.go":
			if(SessionServe.user==null) {
				return "myIndex.html";
			}
			return "index.html";
		case "/projectOne/html/logout.go":
			System.out.println(SessionServe.session.getAttributeNames());
			SessionServe.session.removeAttribute("session");
			SessionServe.session.invalidate();
			SessionServe.user=null;
			return "myIndex.html";
		case "/projectOne/html/Profile.go":
			if(SessionServe.user==null) {
				return "myIndex.html";
			}
			return "hamburger.html";
		case "/projectOne/html/goBack.go":
			if(SessionServe.user==null) {
				return "myIndex.html";
			}
			else if(SessionServe.user.getRole().equalsIgnoreCase("employee")) {
				return "afterLogin.html";
			}else {
				return "managerAfterLogin.html";
			}
		case "/projectOne/html/reimb1.go":
			if(SessionServe.user==null) {
				return "myIndex.html";
			}
			return "index1.html";
		case "/projectOne/html/updatePage.go":
			if(SessionServe.user==null) {
				return "myIndex.html";
			}
			return NewUserController.updateUser(req);
		case "/projectOne/html/updateProfile.go":
			if(SessionServe.user==null) {
				return "myIndex.html";
			}
			return "profileUpdate.html";
		case "projectOne/html/updateReim.go":
			if(SessionServe.user==null) {
				return "myIndex.html";
			}
			return "createReim.html";
		case "/projectOne/html/newReim.go":
			if(SessionServe.user==null) {
				return "myIndex.html";
			}
			return MakeReimb.makeReq(req);
		case "/projectOne/html/makeReim.go":
			if(SessionServe.user==null) {
				return "myIndex.html";
			}
			return "createReim.html";
		case "/projectOne/html/appReimb.go":
			if(SessionServe.user==null) {
				return "myIndex.html";
			}
			return MakeReimb.updateReim(req);
		case "/projectOne/html/newUser.go":
			return NewUserController.makeUser(req);
		}
		return "homepage.html";
	}

	
	
	
}
