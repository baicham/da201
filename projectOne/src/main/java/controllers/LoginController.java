package controllers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

import models.Users;
import servicelayer.UserFunc;


public class LoginController {
	
	public static Users login(HttpServletRequest req) throws IOException, ServletException{
	
		
		String username= req.getParameter("username");
		String password= req.getParameter("password");
		System.out.println(username);
		System.out.println(password);
		return(UserFunc.login(username, password));
		
	}
	
	
}
