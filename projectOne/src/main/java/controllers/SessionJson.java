package controllers;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class SessionJson extends HttpServlet{
	
	public static void getCustomJson(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		res.setContentType("application/json");
		res.getWriter().write(new ObjectMapper().writeValueAsString(SessionServe.user));
		System.out.println(SessionServe.user);
	}

}
