package controllers;

import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import ersdao.ReimbDao;
import models.Reimbursements;
import servicelayer.ReimbFunc;

public class MakeReimb {
	
	public static String makeReq(HttpServletRequest req) {
		int amount= new Integer(req.getParameter("amount"));
		String desc=req.getParameter("description");
		System.out.println(req.getParameter("type"));
		Integer type=new Integer(req.getParameter("type"));
		Reimbursements reim=new Reimbursements(SessionServe.user, amount, desc, type);
		ReimbDao.makeReim(reim);
		return "goBack.go";
		}
		
		public static String updateReim(HttpServletRequest req) {
			int reimId=new Integer(req.getParameter("alterID"));
			String choice=req.getParameter("choice");
			System.out.println(choice);
			int userid=SessionServe.user.getUser_id();
			if(choice.equalsIgnoreCase("Approve")) {
				ReimbDao.updateRecord(userid, 1, reimId);
			}else if(choice.equalsIgnoreCase("Deny")) {
				ReimbDao.updateRecord(userid, 2, reimId);
			}return "index.html";
		}
	
	

}
