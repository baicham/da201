package controllers;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import ersdao.ReimbDao;

public class DirectJson extends HttpServlet{
	static ReimbDao rd=new ReimbDao();
	public static void sendAll(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException{
		System.out.println("in the direct servlet");
		res.setContentType("application/json");
		res.getWriter().write(new ObjectMapper().writeValueAsString(rd.getAll()));
	}
	
	public static void sendSome(HttpServletRequest req, HttpServletResponse res) throws JsonProcessingException, IOException {
		
		res.setContentType("application/json");
		res.getWriter().write(new ObjectMapper().writeValueAsString(
				ReimbDao.getReimById(SessionServe.user.getUser_id())));
	}
}
