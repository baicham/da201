package ersdao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.sql.Types;

import models.Users;

public class UserDao implements ERSDoaInt {
	static HashMap<String, Users> allUsers = new HashMap<String, Users>();

	static{
	       try {
	           Class.forName("oracle.jdbc.driver.OracleDriver");
	       } catch (ClassNotFoundException e) {
	           e.printStackTrace();
	       }
	   }
	@Override
	public HashMap getAll() {
		try (Connection conn = DriverManager.getConnection(url, username, password)) {
			String sql = "select * from ERS_USERS A inner join ers_users_roles B on A.USER_ROLE_ID= B.ers_user_role_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				allUsers.put(rs.getString(2), new Users(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4),
						rs.getString(5), rs.getString(6), rs.getString(9),rs.getInt(8)));
			}
			return allUsers;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	
	public static void addOne(Users user) {
		System.out.println("in the add");
		try (Connection conn = DriverManager.getConnection(url, username, password)) {
			String sql = "{call insert_user(?,?,?,?,?,?)}";
			CallableStatement cs=conn.prepareCall(sql);
			cs.setString(1, user.getUsername());
			cs.setString(2, user.getPassword());
			cs.setString(3, user.getFirst_name());
			cs.setString(4, user.getLast_name());
			cs.setString(5, user.getEmail());
			cs.setInt(6, user.getRolenum());
			cs.executeUpdate();
			System.out.println("added");
		}catch(SQLException e) {
			e.printStackTrace();
		}
			
	}


	public void updateRecord() {
		// TODO Auto-generated method stub
		
	}


	

	
	public static  String hashing(String user, String pass) {
		String sql="select get_customer_hash(?,?) from dual";
		try(Connection conn=DriverManager.getConnection(url, username, password)){
			PreparedStatement ps=conn.prepareCall(sql);
			ps.setString(1, user);
			ps.setString(2, pass);
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				return rs.getString(1);
			}return null;
		}catch(SQLException e) {
			e.printStackTrace();
			System.out.println("could not hash password");
			return null;
		}
	}
	
	public static void updateUser(Users user) {
		String sql="{call updateUser(?,?,?,?,?,?)}";
		try(Connection conn=DriverManager.getConnection(url, username, password)){
			CallableStatement cs=conn.prepareCall(sql);
			cs.setInt(1, user.getUser_id());
			cs.setString(2, user.getUsername());
			cs.setString(3, user.getPassword());
			cs.setString(4, user.getFirst_name());
			cs.setString(5, user.getLast_name());
			cs.setString(6, user.getEmail());
			cs.execute();
	}catch(SQLException e) {
		e.printStackTrace();
	}
	}
	
}