package ersdao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import models.Reimbursements;
import models.Users;

public class ReimbDao implements ERSDoaInt {

	static ReimbDao rd=new ReimbDao();
	static{
	       try {
	           Class.forName("oracle.jdbc.driver.OracleDriver");
	       } catch (ClassNotFoundException e) {
	           e.printStackTrace();
	       }
	   }
	public static HashMap<Integer, Reimbursements> reimList= rd.getAll();
	@Override
	public  HashMap<Integer,Reimbursements> getAll() {
		HashMap<Integer, Reimbursements> reimbMap = new HashMap<Integer, Reimbursements>();
		try (Connection conn = DriverManager.getConnection(url, username, password)) {
			String sql = "SELECT * FROM ERS_REIMBURSEMENT_STATUS A INNER JOIN ERS_REIMBURSEMENT B ON A.REIMB_STATUS_ID=B.REIMB_STATUS_ID" + 
					"    INNER JOIN ERS_REIMBURSEMENT_TYPE C ON B.REIMB_TYPE_ID=C.REIMB_TYPE_ID inner join ers_users D on B.reimb_author = d.ers_users_id" + 
					"    left outer join ers_users E on B.REIMB_RESOLVER= E.ers_users_id";
			PreparedStatement ps = conn.prepareStatement(sql);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				reimbMap.put(rs.getInt(3), new Reimbursements(rs.getInt(3), rs.getDouble(4), rs.getString(5), rs.getString(6), 
						rs.getString(7), rs.getInt(9), rs.getString(2), rs.getString(14), rs.getString(16), rs.getString(23)));
				}
			return reimbMap;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		//return null;
		return null;
	}

	public static void updateRecord(int id, int status, int reimid) {
		String sql="update ers_reimbursement set reimb_resolved=current_timestamp, reimb_resolver=?, reimb_status_id=? where reimb_id=?";
		try(Connection conn=DriverManager.getConnection(url, username, password)){
			PreparedStatement ps=conn.prepareStatement(sql);
			ps.setInt(1, id);
			ps.setInt(2, status);
			ps.setInt(3, reimid);
			ps.execute();
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}

	
	public static void makeReim(Reimbursements reim) {
		String sql="{call make_request(?,?,?,?,?)}";
		try(Connection conn=DriverManager.getConnection(url, username, password)){
			PreparedStatement ps=conn.prepareStatement(sql);
			ps.setDouble(1, reim.getReimb_amount());
			ps.setString(2, reim.getReimb_description());
			ps.setInt(3, reim.getReimb_author());
			ps.setInt(4, reim.getReimb_status_id());
			ps.setInt(5, reim.getReimb_type_id());
			ps.execute();
			
		}catch(SQLException e) {
			e.printStackTrace();
			System.out.println("could not make reimbursement");
		}
	}
	
	public static HashMap<Integer, Reimbursements> getReimById(int id){
		String sql = "SELECT * FROM ERS_REIMBURSEMENT_STATUS A INNER JOIN ERS_REIMBURSEMENT B ON A.REIMB_STATUS_ID=B.REIMB_STATUS_ID" + 
				"    INNER JOIN ERS_REIMBURSEMENT_TYPE C ON B.REIMB_TYPE_ID=C.REIMB_TYPE_ID inner join ers_users D on B.reimb_author = d.ers_users_id" + 
				"    left outer join ers_users E on B.REIMB_RESOLVER= E.ers_users_id where reimb_author=?";
		HashMap<Integer, Reimbursements> hm=new HashMap<Integer, Reimbursements>();
		try(Connection conn=DriverManager.getConnection(url, username, password)){
			PreparedStatement ps=conn.prepareStatement(sql);
			ps.setInt(1, id);
			ResultSet rs=ps.executeQuery();
			while(rs.next()) {
				hm.put(rs.getInt(3),  new Reimbursements(rs.getInt(3), rs.getDouble(4), rs.getString(5), rs.getString(6), 
						rs.getString(7), rs.getInt(9), rs.getString(2), rs.getString(14), rs.getString(16), rs.getString(23)));
				
					}
			return hm;
	}catch(SQLException e) {
		e.printStackTrace();
		System.out.println("lazy debugging");
		return hm;
	}
}
}