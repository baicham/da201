window.onload = function() {
    getUser();
}

function getUser() {
    console.log("function has been called")

    let xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        console.log("ready state is changing");
        console.log(xhttp.readyState)
        console.log(xhttp.status)
        if ((xhttp.readyState == 4) && (xhttp.status == 200)) {
            console.log("ready state and status is good")
            let user = JSON.parse(xhttp.responseText);
            setValues(user);
        }
    }
    xhttp.open("POST", "http://localhost:9005/projectOne/resources/user.JSON");
    xhttp.send();
}

function setValues(user) {
	document.getElementById("Id").innerHTML=user.user_id;
    document.getElementById("username").innerHTML = user.username;
    document.getElementById("First").innerHTML = user.first_name;
    document.getElementById("Last").innerHTML = user.last_name;
    document.getElementById("Email").innerHTML = user.email;
}