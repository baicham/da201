package junit;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import ersdao.UserDao;
import servicelayer.UserFunc;

public class TestingUserFunc {
	static UserFunc functions;
	static UserDao ud = new UserDao();

	@BeforeClass
	public static void beforeClass() {
		System.out.println("--------Before Class-----------");

	}

	@Before
	public void beforeMethod() {
		System.out.println("-----------before test---------");

	}

	@AfterClass
	public static void afterClass() {
		System.out.println("-----------after class----------");

	}

	@After
	public void afterMethod() {
		System.out.println("-----------after test--------------");

	}

	@Ignore
	@Test
	public void testLogin() {
		assertNull(UserFunc.login("fbsg", "afdb"));
		System.out.println(UserFunc.login("USERNAMER", "PASSWORD"));
		assertThat(UserFunc.login("USERNAMER", "PASSWORD"), notNullValue());
		assertNull(UserFunc.login("USERNAMER", "afdb"));
		assertNull(UserFunc.login("fbsg", "PASSWORD"));

	}

	@Test
	public void testLoginCheck() {
		System.out.println(ud.hashing("USERNAMER", "PASSWORD"));
	}

}
