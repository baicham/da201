package junit;

import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertThat;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import ersdao.ReimbDao;
import models.Reimbursements;
import models.Users;
import servicelayer.ReimbFunc;
import servicelayer.UserFunc;

public class TestingReimbFunctions {
	static ReimbFunc rf=new ReimbFunc();
	static ReimbDao rd=new ReimbDao();

	@BeforeClass
	public static void beforeClass() {
		System.out.println("--------Before Class-----------");

	}

	@Before
	public  void beforeMethod() {
		System.out.println("-----------before test---------");

	}

	@AfterClass
	public static void afterClass() {
		System.out.println("-----------after class----------");

	}

	@After
	public  void afterMethod() {
		System.out.println("-----------after test--------------");

	}

	@Ignore
	@Test
	public void testGetReimb() {
		System.out.println(ReimbFunc.getReimb(0));
		assertThat(ReimbFunc.getReimb(0), notNullValue());
		assertThat(ReimbFunc.getReimb(1), nullValue());
		System.out.println(rd.getAll());	
	}
	
	@Ignore
	@Test
	public void testMakeReim() {
		Users user=UserFunc.login("USERNAMER","PASSWORD");
		Reimbursements reimb=new Reimbursements(user,450, "speeling bee", 2);
		ReimbDao.makeReim(reimb);
	}
	
	@Test
	public void testPullReim() {
		Users user=UserFunc.login("USERNAME","PASSWORD");
		System.out.println(user);
		System.out.println(ReimbDao.getReimById(user.getUser_id()));
	}
}
