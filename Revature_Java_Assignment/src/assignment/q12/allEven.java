/*
 * Question 12 needs us to print all even numbers in a specified range to the console. We use a for 
 * loop to add the integers into an ArrayList. We use an enhanced for loop to groom through the list
 * and take out the even numbers using modulus 2 and add them to another ArrayList. We then print the
 * second ArrayList to the console, displaying the evens in the range. For modularity, the main
 * method calls the method using an integer for the argument. This lets us print the evens within
 * any specified range given.
 */



package assignment.q12;

import java.util.ArrayList;

public class allEven {
	public static void main(String[] args) {
		evens(100);
		
	}
	static void evens(int n) {
		ArrayList<Integer> newlist=new ArrayList<>();
		ArrayList<Integer> newlist2=new ArrayList<>();
		for (int i=1;i<=n;i++) {
			newlist.add(i);
		}
		for (int myTemp:newlist) {
			if(myTemp%2==0) {
				newlist2.add(myTemp);
			}
		}
		System.out.println(newlist2);
	}

}
