/*
 * Question 4 asks us to make a method for calculating the factorial of a given integer.
 * We can accomplish this with a ternary statement. We find if the integer is less than 2.
 * Since 0 and 1 factorial are both 1, we simply return 1. Otherwise, we create a recursive 
 * function for multiplying the current number with the method result of the next number 
 * down the line. We then return the result. The preferred data type for this would be
 * either long or BigInteger, since int would not hold enough data to correctly display 
 * factorials of numbers in the lower double digits.
 */


package assignment.q04;

public class nFactorial {
public static void main(String[] args) {

	System.out.println(Nfact(16));
	
}

public static long Nfact(long n) {
	
	long x= (n<2)? 1: Nfact(n-1)* n;
	
	return x;
}
}
