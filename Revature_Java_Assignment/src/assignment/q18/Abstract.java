/*
 * 		This is the creation of the interface for question 18. It holds abstract methods for 
 * use in the concrete parent class.
 */



package assignment.q18;

public interface Abstract {
	public boolean isUppercase();
	public String toUp();
	public void convertString();
}
