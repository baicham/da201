/*
 * This is the concrete child class for question 19. It inherits the variables and
 * methods from its parent class. We instantiate the methods and variables using the
 * dot operator. The result is the printed to the console.
 */




package assignment.q18;

public class ConcreteChild {
	public static void main(String[] args) {
		
	
	ConcreteSuper sup=new ConcreteSuper();
	sup.s="Hello";
	System.out.println(sup.isUppercase());
	System.out.println(sup.toUp());
	sup.convertString();
	
	}
}
