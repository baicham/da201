/*
 * This is the parent class for question 18. This is the class that implements 
 * the interface and adds the body for these methods. The child class will inherit
 * these methods and variables at class creation by instantiating this class.
 */




package assignment.q18;

public class ConcreteSuper implements Abstract {
	String s;
	int i;

	public boolean isUppercase() {
		boolean up = !s.equals(s.toLowerCase());
		return up;

	}

	public String toUp() {
		String s2 = s.toUpperCase();
		return s2;
	}

	public void convertString() {
		try {
			i = Integer.valueOf(s);
			System.out.println(i + 10);
		} catch (NumberFormatException e) {
			System.out.println(e.getMessage());
		}
	}
}
