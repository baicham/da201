/*
 * 		Question 6 asks us to find whether a given number is even or odd with out using 
 * the modulus operator. In binary representation of numbers, the right-most number 
 * represents the number 1 or 0, so if a number is odd, the right digit will be 1,
 * otherwise it will be 0. We can use bitwise operation to check the binary representation
 * of the number and check this last number with the number 1. The number 1 will always have 
 * 0's in all spaces but the right first one. this will result in only checking the 
 * right most digit for 1. If it returns true, the String, "even" or "odd", will be printed 
 * to the console.
 */


package assignment.q06;

public class NoMod {
	public static void main(String[] args) {
		evenorOdd(3);

	}

	static void evenorOdd(int n) {
		if ((n & 1) == 0) {					//Bitwise operation compares the binary representation of the numbers.
			System.out.println("Even");		//So, 5=101 in binary, 1=001
		} else {							//Therefore,	101
			System.out.println("Odd");		//and			001
		}									//Then			001  and is even.
	}
}
