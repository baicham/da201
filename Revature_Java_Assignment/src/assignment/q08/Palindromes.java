/*
 * Question 8 asks us to find the palindromes in an ArrayList. We begin by adding the specified strings
 * into a list. Then we use the string reverse method found in the answer to question 3 and ussing
 * the String.equals() method to compare the list element to the reversed string. If this results in
 * a boolean true, the list item is printed to the console, if not, then it goes on to the next
 * item via a for loop.
 */



package assignment.q08;

import java.util.ArrayList;

import assignment.q03.StringReverse;

public class Palindromes {
	public static void main(String[] args) {
		
			ArrayList<String> sA = new ArrayList<>();
			sA.add(0, "karan");
			sA.add(1, "madam");
			sA.add(2,"tom");
			sA.add(3,"civic");
			sA.add(4,"radar");
			sA.add(5,"sexes");
			sA.add(6,"jimmy");
			sA.add(7,"kayak");
			sA.add(8, "john");
			sA.add(9,"refer");
			sA.add(10,"billy");
			sA.add(11,"did");
			
			Palins(sA);
			
	}
	
	static void Palins(ArrayList sA) {
			for (int i = 0; i < sA.size(); i++) {
				if( sA.get(i).toString().equals(StringReverse.myBackString(sA.get(i).toString())) ) {
					System.out.println(sA.get(i));
					
					
				}
				
				
				
			}
		

	}

	
}
