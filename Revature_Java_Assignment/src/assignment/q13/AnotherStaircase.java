/*
 * For question 13, we needed to make a downward staircase, using a pattern of 0's and 1's. The patter 
 * is in an "on off" state. We use a nested for loop to create the layering needed and to provide
 * a correspondence for the 0 and 1 the program needs by taking the inner for loop variable mod 2.
 */


package assignment.q13;

public class AnotherStaircase {
	public static void main(String[] args) {
		staircase(4);
	}

	static void staircase(int n) {
		int j = 0;
		String str1 = "0";
		String str2 = "1";
		for (int k = n; k > 0; k--) {
			for (int i = k - 1; i < n; i++) {
				
				if (j % 2 == 0) {
					System.out.print(str1);
					j++;
				} else {
					System.out.print(str2);
					j++;
				}

			}

			System.out.println("");
		}
	}
}
