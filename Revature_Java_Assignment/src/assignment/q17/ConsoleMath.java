/*
 * Question 16 asks to create a method for calculating the simple interest rate
 * with information  gathered through the console. We use the scanner class to
 * gather the information for each variable. First, the principle, then the interest 
 * rate, the loan length in terms of years. The principle is a double, since we 
 * can make purchases in dollars and cents. We multiply the entered interest rate by
 * .01 to calculate it into percentage. Then, we take an integer representation of
 * the years. The method then calculates and prints the result to the console.
 */




package assignment.q17;

import java.util.Scanner;

public class ConsoleMath {
	public static void main(String[] args) {
		Scanner scan=new Scanner(System.in);
		System.out.println("Enter principle");
		double principle=scan.nextDouble();
		System.out.println("Enter Interest Rate");
		double rate=scan.nextDouble();
		System.out.println("Enter Amount of Time for Loan");
		int time=scan.nextInt();
		System.out.println("Interest Amount owed is: "+calculate(principle,rate,time));
	}

	static Double calculate(double principle, double rate, int time) {
		principle*=.01;
		double x=principle*rate*time;
		return x;
	}
	
	
}
