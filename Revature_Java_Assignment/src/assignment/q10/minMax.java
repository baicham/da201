/*
 * Question 10 asks us to make a ternary statement to compare two values to give the minimum
 * of the two. The ternary statement is given in a method apart from the main method
 * for modularity. The main method calls the statement with arguments for compartison.
 * The ternary checks for the boolean output from the statement "x<=y"(is x less than 
 * or equal to y). We then give the output for the result on either side of the colon.
 * We incorporate the equals sign for clarity, if the two numbers are equal, then either 
 * number could be printed and still be correct. This provides a small amount more of 
 * disambiguity.
 */


package assignment.q10;

public class minMax {
	public static void main(String[] args) {
		compare(10, 10);

	}

	static void compare(int x, int y) {
		String s = (x <= y) ? "Min is " + x : "Min is " + y;
		System.out.println(s);
	}

}
