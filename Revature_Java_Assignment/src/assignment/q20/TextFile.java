/*
 * For question 20, we needed to create a .txt file with information according
 * to the assignment specs. We then needed to read from that file and display 
 * it on the console. I stored the .txt file next to the .java file and used the 
 * relative file path according the src file for the .java file to gain access
 * to it. I used the java.io.File package to hold a variable "file" for the 
 * address of the file. Then, the scanner variable is issued to the file. Use a 
 * delimiter to cut the file at the specified token, namely ":". 
 * 		A while loop will run the code until there is no next line using the 
 * scanner.hasNextLine() method. Thereafter, we print to the console the specified 
 * text with the tokens found by the scanner.next() methods. We use System.out.print 
 * for the lines we don't want to break, and System.out.println for the ones we do.
 * 		We ensure the scanner closes with the scanner.close() method and catch any 
 * exceptions in the catch block of the try/catch statement surrounding the method.
 * We use the FileNotFound Exception because of the possibility of the file path
 * being mistyped or otherwise corrupted.
 * 		The console text displays the correct format according to the assignment
 * specs.
 */








package assignment.q20;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.InputMismatchException;
import java.util.Scanner;
import java.util.regex.Pattern;

public class TextFile {
	public static void main(String[] args) {
		File file = new File(".\\src\\assignment\\q20\\Data.txt.txt");
		try {
			Scanner scan = new Scanner(file);
			scan.useDelimiter(":");
			while (scan.hasNextLine()) {

				System.out.print("Name:" + scan.next() + " ");
				System.out.println(scan.next());
				System.out.println("Age:" + scan.next());
				System.out.println("State" + scan.nextLine());
			}

			scan.close();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}
	}
}
