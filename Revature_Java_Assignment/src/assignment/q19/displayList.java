/*Question 19 asks us to make an ArrayList and place the numbers 1 through 10
 * into the list. We do this by adding a for loop to insert a specified amount of 
 * numbers into the list. For modularity, this is a method in the class, with the 
 * main method calling it and inserting an integer for use. This way, this can 
 * easily be used with larger or smaller integers as needed. We display the list
 * to ensure the numbers have been added successfully.
 * 		We create two variables to hold integers, 'a' and 'b'. These variables 
 * will hold the sum of the even and odd numbers. We use an enhanced for loop to 
 * go through the list and check for modularity with 2. If the number mod 2 is 0, 
 * then it is even and added to 'a', if not then it is added to 'b'. We then print
 * the integers values to the console for the sum of evens, 'a', and odd, 'b'.
 * 		We check for primes with the same method used in question 9. This method
 * checks for primes, adds them to another ArrayList, and prints it to the console.
 * 
 */





package assignment.q19;

import java.util.ArrayList;

public class displayList {
	public static void main(String[] args) {
		listMaker(10);

	}

	static void listMaker(int n) {
		ArrayList<Integer> list = new ArrayList<>();
		ArrayList<Integer> list2 = new ArrayList<>();
		int a = 0;
		int b = 0;
		for (int i = 0; i <= n; i++) {
			list.add(i);
		}
		System.out.println(list);
		for (int j : list) {
			if (j % 2 == 0) {
				a += j;
			} else {
				b += j;
			}
		}
		System.out.println(a);
		System.out.println(b);
		boolean prime;
		for (int j = 2; j < n; j++) {
			prime = true;
			for (int k = 2; k < j; k++) {
				if (j % k == 0) {
				prime = false;
			}
			}
			if (prime == true) {
				list2.add(j);
			}
		}
		System.out.println(list2);

	}

}
