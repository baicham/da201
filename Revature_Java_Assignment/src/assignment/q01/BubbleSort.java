/*
 * Question 1 asks us to create a method for bubble sorting through an array of
 * numbers. We use a nested for loop to iterate through the array and compare the
 * first number to the rest, find a larger one, and switch them via a  variable.
 * The newly sorted array is then printed to the console. 
 */





package assignment.q01;

public class BubbleSort {
public static void main(String[] args) {
	int[] arr= {1,0,5,6,3,2,3,7,9,8,4};
	arraySort(arr);
}

static void arraySort (int x[]) {
	int y=0;
	for (int k=0; k<x.length; k++) {
		for(int i=k;i<x.length;i++) {
			if(x[k]>x[i]) {
				y=x[k];
				x[k]=x[i];
				x[i]=y;
			}
		}
	}
	for (int tempVar:x) {
		System.out.println(tempVar);
	}
}
}
