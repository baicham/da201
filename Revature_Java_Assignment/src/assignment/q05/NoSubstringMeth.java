/*
 * Question 5 asks us to gather the characters of a string and display the characters
 * to the console relating to the indices up to the integer given. We do this by making
 * the string into a char array and iterating through it the specified number of times and 
 * adding the characters to a new array. We then print the array to the console.
 */




package assignment.q05;

public class NoSubstringMeth {
	public static void main(String[] args) {
		noSub("string",3);
		
	}
static void noSub(String str, int idx) {
	char[] newArr= new char[str.length()];
	for (int i=0;i<str.length(); i++) {
		newArr[i]=str.charAt(i);
	}
	String s="";
	for (int a=0; a<idx; a++) {
		s+=newArr[a];
	}
	System.out.println(s);
}
}
