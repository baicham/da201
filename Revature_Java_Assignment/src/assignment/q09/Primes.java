/*
 * Question 9 asks us to print the prime numbers in a specified range. We use an ArrayList and
 * a for loop to dynamically add the integers into it. We then utilize a nested for loop to check
 * whether the number is divisible by the inside integer. We start the inside variable at 2 and go 
 * all the way to the outside integer and check the modulus comparison.
 */



package assignment.q09;


import java.util.ArrayList;

public class Primes {
	public static void main(String[] args) {
		list(100);

	}

	public static void list(int n) {
		ArrayList<Integer> list = new ArrayList<>();
		boolean prime;
		for (int i = 2; i < n; i++) {
			prime = true;
			for (int j = 2; j < i; j++) {
				if (i % j == 0) {
					prime = false;
				}
			}
			if (prime == true) {
				list.add(i);
			}
		}
		System.out.println(list);

	}
}