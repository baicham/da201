/*
 * Question 11 asks us to create classes in other packages and use the variables in them
 * for addition in this classes method. We begin by simply making the variables public and 
 * giving the variables values in this class. We then take those variables and add them in
 * a System.out.println.
 */


package assignment.q11_solution;

import assignment.q11_01.floatProb1;
import assignment.q11_02.floatProb2;

public class floatSol {
	public static void main(String[] args) {
		float var11= floatProb1.var1;
		float var22=floatProb2.var2;
		System.out.println(var11+var22);
	}

}
