/*
 * For question 16, we needed to create a class that would read from the command line
 * and display the strings and the character count for each string. We do this by 
 * using the args portion of the main method and inserting it into the method.
 * The args portion of the main method will gather information from the command line.
 * We use a for loop and iterate through the String array gathered from the command
 * line. iterating through each word, we print the word into the console and use the 
 * args.length() method to cound the characters of each word. We display a concatenated
 * string to the console.
 */


package assignment.q16;

public class CommandLineString {
	public static void main(String[] args) {
		for (int i = 0; i < args.length; i++) {
			System.out.println(args[i] + " has " + args[i].length() + " characters");
		}
	}
}
