/*
 * For question 14, we needed a switch statement for displaying the integer representation of 
 * the quare root of a number, the current date, and to take a string and store it in a string array.
 * 		The statement takes the argument in the method call in the main method and takes the result of
 *  moduluating it by 3. This lets us use one of the three paths and print it to the console. We take
 *  the square root using the Math.sqrt() function and casting it as an integer. We import the 
 *  java.util.Date package to incorporate the day, month and year and use the variables in the statement.
 *  For the last statement, we create the string, split it by " " (spaces in the string) and input
 *  the results into an array. We then print the array to the console.
 * 
 */




package assignment.q14;

import java.util.Date;


public class Switch {
	public static void main(String[] args) {
		

		swItch(3);
		
		
	}

	static void swItch(int n) {
		Date awesome=new Date();
		
		switch (n % 3) {
		case 1:
			int x = (int) Math.sqrt(n);
			System.out.println(x);
			break;
		case 2:
			System.out.println((1+awesome.getMonth()) + "/" + awesome.getDate() + "/" + (1900+awesome.getYear()));
			break;
		case 0:
			String str = "I am learning Core Java";
			String arr[] = str.split(" ");
			for (int i = 0; i < arr.length; i++) {
				System.out.print(arr[i]);
			}
		}

	}
}
