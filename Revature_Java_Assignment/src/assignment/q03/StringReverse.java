/*
 * For question 3, we needed to simply reverse a string. By using the String.substring() method, we
 * can take the substring of the string, use a for loop to iterate through it, and put the first
 * character into the last spot, and so forth. We then return the String to be displayed in the
 * console.
 */



package assignment.q03;

public class StringReverse {
	public static void main(String[] args) {
myBackString("String");
	}
	
	public static String myBackString(String s) {		//Since a string in java is already an array of chars,
		for (int i=0; i<s.length(); i++) {		//we can manipulate it to reverse the order. 
			s=s.substring(1,s.length()-i)+		//
					s.substring(0,1)+			//
					s.substring(s.length()-i,s.length());
			
		}
		
		return(s);
		
		
	}
}
