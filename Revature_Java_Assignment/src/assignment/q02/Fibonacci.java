/*
 * Question 2 asks us to display the fibonacci numbers. we create an integer array 
 * and iterate through it using a for loop with the specified amount of iterations
 * given by the argument in the method calling. We take the sum of the first two numbers,
 * 0 and 1 respectfully, and make the next number in line. We make the numbers to be
 * added 1 and 2 less than the for loops variable to create the number for the next number
 * in line. we then print the array to the console. For modularity, we create a method and 
 * call it in the main method with an argument for how far into the sequence we want to go.
 */



package assignment.q02;

public class Fibonacci {
	public static void main(String[] args) {
		numCrunch(25);
	}

	
	
	static void numCrunch(int n) {
		int[] arr= new int[n];
		arr[0]=0;
		arr[1]=1;
		for (int i=2; i<n; i++) {
			arr[i]=(arr[i-2]+arr[i-1]);
		}
		for (int newTemp:arr) {
			System.out.print(newTemp+", ");
		}
	}

}
