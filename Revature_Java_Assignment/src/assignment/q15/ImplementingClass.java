/*
 * See Test class for full comments.
 */


package assignment.q15;

public class ImplementingClass implements MathInterface {
	private int i;
	private int j;
	
	
	public int getI() {
		return i;
	}
	public void setI(int i) {
		this.i = i;
	}
	public int getJ() {
		return j;
	}
	public void setJ(int j) {
		this.j = j;
	}
	public void addition() {
		System.out.println(getI()+getJ());
	}
	public void subtraction() {
		System.out.println(getI()-getJ());
	}
	public void multiplication() {
		System.out.println(getI()*getJ());
	}
	public void division() {
		System.out.println((double)getI()/getJ());
	}
}
