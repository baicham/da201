/*
 * We create a Math Interface that holds abstract methods for simple mathematical
 * operations. The implementing class then adds a body for the methods and carries
 * out the logic for the operations. The child class, TestClass, instantiates the 
 * parent class. We hard code the variables for the parent class, using getters 
 * and setters. We set the variables to private for security. The instance of the 
 * parent class is used for calling the methods. The result is displayed on the console.
 */


package assignment.q15;

public class TestClass {
public static void main(String[] args) {
	ImplementingClass imp=new ImplementingClass();
	imp.setI(4);
	imp.setJ(3);
	imp.multiplication();
	//imp.subtraction();
	//imp.addition();
	//imp.division();
}
}
