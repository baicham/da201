/*
 * Question 7 asks us to use the Comparator interface and compare employees based on 
 * name, age and department. The employee class implements the Comparator interface.
 * The Comaparator interface is a functional interface since it only has one method
 * to be overridden for use. Give the Employee class constructors to be used in the Compare 
 * class as well ass getters and setters for each variable. At the bottom of the class,
 * we initialize the interface's method for the use of sorting for age.
 */


package assignment.q07;

import java.util.Comparator;

public class Employee implements Comparator<Employee> {

	private String name;
	private String department;
	private int age;

	Employee(){
	
	}
	
	
	Employee(String name, String department, int age) {
		this.name = name;
		this.department = department;
		this.age = age;
	}

	@Override
	public String toString() {
		return "Employee [name="+name+", department="+department+", age="+age+"]";
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	@Override
	public int compare(Employee o1, Employee o2) {
		// TODO Auto-generated method stub
		return ((int) (o1.getAge())-o2.getAge());
	}

	
	
	
}
