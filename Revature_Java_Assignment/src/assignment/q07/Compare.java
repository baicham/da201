/*
 * In this class, we initialize an object ArrayList using the constructor for initializing
 * the variables. We use an enhanced for loop to print the current list to the console. We
 * instantiate the compare() method to sort the list by age, then print it to the console.
 * Afterwards, we override the compare() method with lambda notation to sort the lists by
 * name and department and print them to the console.
 */



package assignment.q07;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class Compare {
	public static void main(String[] args) {
		ArrayList<Employee> emp=new ArrayList<Employee>();
		
		emp.addAll(Arrays.asList(new Employee [] {
				new Employee("Jimmy","HR",29),
				new Employee("Harry","Stocking",31),
				new Employee("Sarah","Inventory",20)
		}));
		
		System.out.println("The Original List");
		for(Employee e:emp) {
			System.out.println(e);
		}
		
		System.out.println("DONE");
		Collections.sort(emp, new Employee());
		System.out.println("Sorted by Age");
		for(Employee e:emp) {
			System.out.println(e);
		}
		System.out.println("Done");
		System.out.println("Sorted by Name");
		Collections.sort(emp, (arg0,arg1) 
				-> {return arg0.getName().compareTo(arg1.getName());});
		for(Employee e:emp) {
			System.out.println(e);
		}
		
		System.out.println("DONE");
		Collections.sort(emp, (arg0,arg1)
				-> {return arg0.getDepartment().compareTo(arg1.getDepartment());});
	}

}
