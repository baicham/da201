--PLSQL example

--TO CREATE A NEW USER
create user marvel identified by p4ssw0rd;
grant connect, resource to marvel;
grant dba to marvel with admin option;

--creating food table
create table bank(
    username varchar2(50),
    firstname varchar2(50) not null,
    lastname varchar2(50) not null,
    password varchar2 (50) not null,
    access_type varchar2(50) not null,
    acc_id number,
    primary key(username)
);
create table bank_Accounts(
    acc_num number,
    acc_bal number,
    acc_type varchar2 (50),
    acc_date timestamp,
    primary key(acc_num)
);
create table user_acc (
    username varchar2(50));
insert into food values(1,'banana', '...its bananas');
insert into food values(4,'jackfruit', '...just pres backspaces');
insert into food values(7,'orange', '...we fear complexity?');
insert into food values(8,'jambalaya', '...its...spelled with all a"s?');

select * from food;

--stored procedures
----they are blocks of code. essentially, they are functions
-- to delete a procedure-----drop procedure hello_world_procedure;
create or replace procedure hello_world_procedure
is

begin   dbms_output.put_line('HELLO WORLD');
end;
/
--how to execute a stored procedure
begin
    hello_world_procedure();
end;
/


--structure of a procedure in sql
/*
create [or replace] procedure prod_name [list of parameters]
is
*declaration section*
begin
*execution section*
exception
*exception section*
end;
/   <----note that there is a slash at the end of the procedure/function
*/

-------insert stored procedure
create or replace procedure insert_food (f_id in number, f_name in varchar2,
                                            f_recipe in varchar2)
is
begin
    insert into food values(f_id, f_name, f_recipe);
    commit;
end;
/
--exxecuting
begin
    insert_food(10,'bacon', 'love');
end;
/

select * from food;

--out example

create or replace procedure get_food(f_id in number, f_name out food.food_name%type)
is
begin
    select food_name into f_name from food where food_id=f_id;
end;
/
--execute block
declare
    temp_variable varchar2(4000);
begin 
    get_food(7, temp_variable);
    dbms_output.put_line('solution: '||temp_variable);
end;
/
--instead of declaring the dattatype in the parameter declaration, type table.table_column%type to get a datatype from the table



--------------function---------------

create or replace function get_max_id
return number
is
    max_id number;
begin
    select max(food_id) into max_id from food;
    return max_id;
end;
/
---------INTO == (=);

--CALL GET_MAX_ID
DECLARE 
    max_id number;
begin
    max_id:=get_max_id();
    dbms_output.put_line('le max: '||max_id);
end;
/
-------------:= == (=);
------------------select * from food;


create or replace function get_max(num1 in number, num2 in number)
return number
is 
begin
    if num1>num2 then
        return num1;
    else
        return num2;
    end if;
end;
/

--execution
declare 
    greater number;
begin
    greater:=get_max(55,160);
    dbms_output.put_line('max num is: '||greater);
end;
/
--we want to execute again
select get_max(33,79) from dual;

/*					
	procedure works with dml
	functions work with dql.
	scopes in plsql?
*/

select * from food;
-----sequences------
create sequence food_seq
    start with 50
    increment by 1;
------------------------
create or replace procedure insert_food_null_id(f_name in varchar2, f_recipe in varchar2)
is
begin
    insert into food values(food_seq.nextval, f_name, f_recipe);
    commit;
end;
/
--exec
begin 
    insert_food_null_id('mashed potatoes', 'tatsy');
end;
/

select * from food;
delete from food where food_id=51;



-----triggers------
--a trigger is essentially an even listener

create or replace trigger food_insertb
before insert on food
for each row
begin
    if: new.food_id is null then
        select food_seq.nextval into :new.food_id from dual;
    end if;
end;
/
--to delete a trigger---drop trigger food_insertb;
insert into food values(null, 'food', 'recipe');
select * from food;
/*
create or replace trigger trigger_name
before | after           insert | update | delete
on table_name 
for each row    <-----------required if we want to see/manipulate rows of data
declare
begin
exception
end;
/   <-----------slashs
*/


/*

stored procedures                                   vs                               functions

executed instruction                                                           must return value or set
DML,    DQL        
SELECT ONLY
IN/OUT PARAMETERS (VALUES AND REFERENCES)                                          ONLLY IN
PROCEDURES CAN CALL FUNCTIONS                                                   CANNOT CALL PROCEDURES
EXECUSTION BLOCK                                                                  CAN BE USED IN QUERIES OR EXEC BLOCKS
(YES, YOU CAN USE A PROCEDURE INSIDE OF A PROCEDURE)

cursor-
- a cursor is a temporary work area created in the system memory when a sql statement is execusted. a cursor contains
information on a select statement an the ros of data accessed by it.
    (basically points to a result set)

view-
in sql, a view is a virtual table based on the result set of a sql statement. a view contains rows and columns, just like a real 
table. the fields in a view are fields from one or more rela tables in the database. you can ad sql functions:
        WHERE, and JOIN statements to a view and present that data as if the data were coming from one single table.
--CURSORS
-------A temporary work area created in the system memory when a sql statement
--is executed. Cursor contains information on a select statement and the rows of
--data accessed by it.
CREATE OR REPLACE FUNCTION AFTER_1968 RETURN SYS_REFCURSOR
is
    EMPLOYEE_C SYS_REFCURSOR;
BEGIN
    OPEN EMPLOYEE_C FOR SELECT * FROM EMPLOYEE WHERE 
    BIRTHDATE >= TO_DATE('01-01-1968', 'DD-MM-YYYY');
    return EMPLOYEE_C;
END;
/
select after_1968 from dual;
select * from employee;







*/