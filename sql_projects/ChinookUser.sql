.--2.1
Select * FROM Employee;
Select * FROM employee WHERE lastname='King';
SELECT * FROM employee WHERE firstname='Andrew' AND REPORTSTO is NULL;
--2.2
SELECT * FROM album ORDER BY TITLE DESC;
SELECT firstname FROM customer ORDER BY city;
--2.3
SELECT * FROM GENRE;
INSERT INTO genre VALUES(26,'Powerful Music');
INSERT INTO genre VALUES (27, 'Popular Music');

SELECT * FROM employee;
INSERT INTO employee (EmployeeId, LastName, FirstName)
        VALUES (9,'Stevens','Rachael');
INSERT INTO employee (EmployeeId, LastName, FirstName)
        VALUES (10,'Jones', 'Charles');
        
SELECT * FROM customer;
INSERT INTO customer (customerid, lastname, firstname, email)
        VALUES (60, 'Johnson', 'Larry','larry.johnson@email.com');
INSERT INTO customer (customerid, lastname, firstname,email)
        VALUES (61, 'Adams', 'John', 'john.adams@email.com');        
        
--2.4
UPDATE customer SET firstname='Robert', lastname='Walter' WHERE firstname='Aaron' and lastname='Mitchell';
-----------------custid=32
UPDATE artist SET Name='CCR' WHERE Name='Creedence Clearwater Revival';
SELECT * FROM artist;
--artistid=76
SELECT * FROM invoice;
--2.5
SELECT * FROM invoice WHERE billingaddress like 'T%';

--2.6
SELECT * FROM invoice WHERE total BETWEEN 15 AND 20;
SELECT * FROM employee WHERE hiredate BETWEEN '01-JUN-03' AND '01-MAR-04';
SELECT * FROM invoiceline;

--2.7
DELETE FROM INVOICELINE WHERE INVOICEID in (50,61,245,268,290,342,116);
delete from invoice WHERE customerid=32;
DELETE FROM customer WHERE firstname='Robert' and lastname='Walter';

--3.1
SELECT CURRENT_TIMESTAMP FROM DUAL;
SELECT * FROM mediatype;
--SELECT NAME FROM MEDIATYPE WHERE MEDIATYPEID=4;
select length(name) from mediatype;

--3.2
select avg(total) from invoice;
select max(total) from invoice;
select * from invoiceline;
--3.3
create or replace function time_stamp
return timestamp
is
begin
    return current_timestamp;
end;
/
select time_stamp from dual;
select * from mediatype;

create or replace function media_length(mediaid in number)
return number
is
num_of_string number(30);
begin
    select length(name) into num_of_string from mediatype where mediatypeid=mediaid;
    return num_of_string;
end;
/
select media_length(4) from dual;

--3.2
create or replace function avg_total
return number
is
x number(20);
begin
    select avg(total) into x from invoice;
    return x;
end;
/
select avg_total from dual;

create function expensive_track
return number
is
x number(20);
begin
    select max(unitprice) into x from invoiceline;
    return x;
end;
/
select expensive_track from dual;

--3.3

create or replace function my_func_for_avg
return number
is
x number(30);
begin
    select avg(unitprice) into x from invoiceline;
    return x;
end;
/
select my_func_for_avg from dual;

3.4
create or replace procedure get_table_new(table_name in varchar2,column_name in varchar2)
is
begin
    select column_name from table_name;
end;
/

--CURSORS
CREATE OR REPLACE FUNCTION AFTER_1968 RETURN SYS_REFCURSOR
is
    EMPLOYEE_C SYS_REFCURSOR;
BEGIN
    OPEN EMPLOYEE_C FOR SELECT * FROM EMPLOYEE WHERE 
    BIRTHDATE >= TO_DATE('01-01-1968', 'DD-MM-YYYY');
    return EMPLOYEE_C;
END;
/
select after_1968 from dual;
select * from employee;

--4.1
create or replace procedure get_names_of_emp (emp_names out sys_refcursor) is
begin
     open emp_names for select firstname,lastname from employee;
end;
/

--4.2
create or replace procedure emp_info_up (emid in number, first  in varchar2, last in varchar2) is
begin
    update employee set firstname = first, lastname = last where employeeid = emid;
end;
/


/*
create or replace procedure emp_info_up(emp_id in number, new_add in String, new_city in varchar2, new_state in varchar2, new_country in varchar2,
                    new_zip in string, new_phone in string, new_fax in string, new_email in string)
is
begin
    update employee set address=new_add where employeeid=emp_id;
    update employee set city=new_city where employeeid=emp_id;
    update employee set state=new_state where employeeid=emp_id;
    update employee set country=new_country where employeeid=emp_id;
    update employee set phone=new_phone where employeeid=emp_id;  
    update employee set fax=new_fax where employeeid=emp_id;
    update employee set email=new_email where employeeid=emp_id;
end;
/
*/

create or replace procedure get_man(empid in number, reported out varchar2)
is 
begin
    select reportsto into reported from employee where employeeid=empid;
end;
/

--4.3

create or replace procedure cust_info(cus_id in number, first_name out customer.firstname%type, last_name out customer.lastname%type,
                        comp_out out customer.company%type)
is
begin
    select firstname into first_name from customer where customerid=cus_id;
    select lastname into last_name from customer where customerid=cus_id;
    select company into comp_out from customer where customerid=cus_id;
end;
/

--5.0

create or replace procedure delete_invoice(invoice_id in number)
is
begin
  delete from invoice where invoiceid = invoice_id;
end;
/

create or replace procedure add_customer (cid in number, first in varchar2, last in varchar2,
    comp in varchar2, ad in varchar2, city in varchar2, state in varchar2, con in varchar2, zip in varchar2, 
    phone in varchar2, fax in varchar2, email in varchar2, srid in number) 
is
begin 
    insert into customer values (cid, first, last, comp, ad, city, state, con, zip, phone, fax, email, srid);
end;
/

--6.1

create or replace trigger here_i_am
after insert on employee
begin
    dbms_output.put_line('Rock Me Like A Hurricane');
end;
/

create or replace trigger whhz_UUUp
after update on album
begin
    dbms_output.put_line('whaaaazzzzzzzuuuuuuppppp!>!!>!>!>!>>!!>>!>!>!>!!');
end;
/

create or replace trigger makindaze
after delete on customer
begin
    dbms_output.put_line('-\_(*_*)_/-');
end;
/

--7.1

select firstname, lastname, invoiceid from customer A inner join invoice B on A.customerid=B.customerid;

--7.2

select A.customerid, firstname, lastname, invoiceid, total from customer A left outer join invoice B on A.customerid=B.customerid;

--7.3

select name, title from album A right outer join artist B on A.artistid=B.artistid;

--7.4

select * from artist cross join album order by name;

--7.5

select * from employee A inner join employee B on A.employeeid=B.reportsTo;

--9.0
--file stored as chinook.bak