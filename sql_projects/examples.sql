select * from album;
select * from artist;

--inner
select * from artist a INNER JOIN album b ON a.artistid=b.artistid;

--left
select * from artist a LEFT OUTER JOIN album b ON a.artistid=b.artistid;

--right
select * from artist a RIGHT OUTER JOIN album b ON a.artistid=b.artistid;

--full
select * from artist a FULL OUTER JOIN album b ON a.artistid=b.artistid;
--outer is optional

--left without middle of venn diagram (strictly left outer join)
select * from artist a left join album b on a.artistid=b.artistid
    where a.artistid not in (
    select a.artistid from artist a inner join album b on a.artistid=b.artistid);

--cleaner
select * from artist a full join album b on a.artistid=b.artistid
    where b.artistid is null;

--strictly full
select * from artist a full join album b on a.artistid=b.artistid
    where b.artistid is null or a.artistid is null;

--strictly right
select * from artist a full join album b on a.artistid=b.artistid
    where a.artistid is null;



--self join
---select * from table1 A INNER JOIN table1 B ON a.id=b.foreignid;

--cross join    aka cartesian join
---select * from table01 cross join tabl2;

-------alias using the as keyword
select albumid as alb, title as tle from album;
--another type of aliasing
select * from album alb where alb.albumid between 5 and 9;

------what is an entity?
--tables = entity = data object





