select * from ers_users;
SELECT * FROM ERS_REIMBURSEMENT;

commit;
SELECT * FROM ERS_REIMBURSEMENT_STATUS A INNER JOIN ERS_REIMBURSEMENT B ON A.REIMB_STATUS_ID=B.REIMB_STATUS_ID INNER JOIN ERS_REIMBURSEMENT_TYPE C ON B.REIMB_TYPE_ID=C.REIMB_TYPE_ID where reimb_author=0;
select * from ERS_REIMBURSEMENT_STATUS;
select * from ERS_REIMBURSEMENT_TYPE;
select * from ERS_USERS_ROLES;
select * from  ERS_REIMBURSEMENT where Reimb_author=0;
insert into ERS_REIMBURSEMENT_STATUS values(0, 'PENDING');
INSERT INTO ERS_REIMBURSEMENT_STATUS VALUES (1, 'APPROVED');
INSERT INTO ERS_REIMBURSEMENT_STATUS VALUES (2, 'DENIED');
INSERT INTO ERS_REIMBURSEMENT_STATUS;
UPDATE ERS_USERS_ROLES SET USER_ROLE='EMPLOYEE' WHERE ERS_USER_ROLE_ID=1;
UPDATE ERS_USERS_ROLES SET USER_ROLE='MANAGER' WHERE ERS_USER_ROLE_ID=2;
INSERT INTO ERS_REIMBURSEMENT_TYPE VALUES (0, 'LODGING');
INSERT INTO ERS_REIMBURSEMENT_TYPE VALUES (1, 'TRAVEL');
INSERT INTO ERS_REIMBURSEMENT_TYPE VALUES (2, 'FOOD');
INSERT INTO ERS_REIMBURSEMENT_TYPE VALUES (3, 'OTHER');
COMMIT;
INSERT INTO ERS_USERS VALUES (0.2, 'toad', GET_CUSTOMER_HASH('toad','toad'), 'Toad', 'Toad', 'toad@marioparty.com', 2);
SELECT * FROM ERS_USERS;
DELETE ERS_USERS WHERE ERS_USERS_ID=0.2;
ALTER TABLE ERS_USERS add constraint EEmail Unique (user_email);
update ers_reimbursement_type set reimb_type='Boo' where reimb_type_id=2;
delete from ers_users where ers_username='USERNAME';
delete from ERS_REIMBURSEMENT where reimb_id=0;
select * from ERS_USERS A inner join ers_users_roles B on A.USER_ROLE_ID= B.ers_user_role_id;

INSERT INTO ERS_REIMBURSEMENT VALUES (0,1000, CURRENT_TIMESTAMP, NULL, 'MADE IT TO DESTINATION', 
                    NULL, 0, NULL, 0, 0);
                    
COMMIT;
SELECT * FROM ERS_REIMBURSEMENT_STATUS A INNER JOIN ERS_REIMBURSEMENT B ON A.REIMB_STATUS_ID=B.REIMB_STATUS_ID INNER JOIN 
                                            ERS_REIMBURSEMENT_TYPE C ON B.REIMB_TYPE_ID=C.REIMB_TYPE_ID;

SELECT * FROM ERS_REIMBURSEMENT;
insert into ers_reimbursement values(1,100,current_timestamp,null,'into the wonderland', null, 0.1,null, 1,1);
update ers_users set ers_password=?;

CREATE OR REPLACE FUNCTION GET_CUSTOMER_HASH(USERNAME VARCHAR2, PASSWORD VARCHAR2) RETURN VARCHAR2
IS
EXTRA VARCHAR2(10) := 'BAMA';
BEGIN
  RETURN TO_CHAR(DBMS_OBFUSCATION_TOOLKIT.MD5(
  INPUT => UTL_I18N.STRING_TO_RAW(DATA => USERNAME || PASSWORD || EXTRA)));
END;
/
create or replace procedure insert_user(username in varchar2, password in varchar2, first in varchar2, last in varchar2, email in varchar2, role in number)
is 
newpass varchar2(200);
begin
    newpass:=get_customer_hash(username,password);
    insert into ers_users values(make_user_id.nextval, username, newPass, first, last, email, role);
    commit;
end;
/
drop sequence make_user_id;
create sequence make_user_id
    start with 2
    increment by 1;

select get_customer_hash('USERNAME','PASSWORD') FROM DUAL;
commit;

drop function checkLogin;
create or replace procedure checkLogin(user in varchar2)
is
x number,
begin
    select count(ers_username)into x from ers_users where ers_username=user;
    if x>0 then
        select * from ers_users into cust where ers_username=user;
        end if;
end;
/
create sequence reim_id_seq
    start with 1
    increment by 1;

create or replace function make_hash_pass(user in varchar2, pass in varchar) return varchar2
is
x vARCHAR2 (100);
begin
    Select get_customer_hash(user,pass) into x from dual;
    return x;
end;
/

create or replace procedure make_request(amount in number, descr in varchar2, auth in number, stat in number, type in number)
is
begin
    insert into ers_reimbursement values(reim_id_seq.nextval,amount,current_timestamp,null,descr, null, auth ,null, stat,type);
    commit;
end;
/
commit;
create or replace procedure updateUser(id in number, username in varchar2, password in varchar2, first in varchar2, last in varchar2, email in varchar2)
is
begin
    update ers_users set ers_username=username, ers_password=get_customer_hash(username,password), 
        user_first_name=first, user_last_name=last, user_email=email where ers_users_id=id;
end;
/
update ers_users set ers_users_id=5555 where ers_username='toad';

SELECT * FROM ERS_REIMBURSEMENT_STATUS A INNER JOIN ERS_REIMBURSEMENT B ON A.REIMB_STATUS_ID=B.REIMB_STATUS_ID 
    INNER JOIN ERS_REIMBURSEMENT_TYPE C ON B.REIMB_TYPE_ID=C.REIMB_TYPE_ID inner join ers_users D on B.reimb_author = d.ers_users_id 
    left outer join ers_users E on B.REIMB_RESOLVER= E.ers_users_id where reimb_author=0.1;
