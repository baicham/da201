--select * from employee;

--TO CREATE A NEW USER AND GRANT THAT USER PERMISSIONS
CREATE USER pokemondb IDENTIFIED BY p4ssw0rd;

GRANT CONNECT, RESOURCE TO pokemondb;
GRANT DBA TO pokemondb WITH ADMIN OPTION;

--------------------CREATE A TABLE------------------
--DATATYPES IN SQL: NUMBER VARCHAR, VARCHAR2, DATE, TIMESTAMP, BLOB, CLOB
---------
--CONSTRAINTS: PRIMARY KEY, FOREIGN KEY, NOT NULL, UNIQUE, AND CHECK
--NOT NULL ENSURE THE RECORD HAS A VALUE IN THIS COLUMN
--UNIQUE ENSURES ANY TWO RECORDS WILL NOT HAVE THE SAME VALUE
--PRIMARY KEY IT REPRESENTS THE OFFICIAL UNIQUE IDENTIFIER FOR THIS TABLE
--      PK IS A COMBINATION OF UNIQUE AND NOT NULL
--FOREIGN KEY IS A REFERENCE POINTER TO A ANOTHER TABLES PRIMARY KEY
--      FK IS USED AS A LOCATOR
--    EXAMPLE: FOREIGN KEY (refkey from another table) REFERENCES other_table (other_PK)
--CHECK CREATES AN ADDITIONAL CUSTOM CONDITION FOR THE COLUMN
--EXAMPLE: CONSTRAINT constraint_name CHECK (column_name BETWEEN 100 and 999)

--DDL
CREATE TABLE pokemon(
    POKEMONID number (15),
    pokemon_name VARCHAR2 (100) UNIQUE,
    pokemon_type VARCHAR2 (25),
    PRIMARY KEY (pokemonid)
    --Primary key was on first line, but can be moved to another line just for identification;
    -- POKEMONID number (15) PRIMARY KEY,
);

--describes the structure of the table
DESC pokemon;   

--drop  DDL
DROP TABLE pokemon;

--alter     DDL
ALTER TABLE pokemon ADD pokemon_secondtype VARCHAR2 (25);

--TRUNCATE      DDL
---NOT GOING TO DO AN EXAMPLE BUT TRUNCATE DELETES ALL ENTRIES INTO THE TABLE
---WHILE KEEPIN THE TABLE STRUCTURE ITSELF. DROP IS LIKE THROWING A CUP OF WATER
---IN THE TRASH, WHILE TRUNCATE IS LIKE POURING OUT THE WATER AND KEEPING THE CUP.

--INSERT        DML
INSERT INTO pokemon VALUES (1, 'bulbasaur', 'grass', 'ice');
INSERT INTO pokemon VALUES(7, 'squirtle', 'water', 'null');
INSERT INTO pokemon VALUES (121, 'starmie', 'water', 'psychic');
INSERT INTO pokemon VALUES (151, 'MEW','PSYCHIC', 'NULL');
INSERT INTO POKEMON (pokemonid, pokemon_type, pokemon_name)
        VALUES (25, 'electric','pikachu');


--seeing all entries into the table
select * from pokemon;

UPDATE pokemon SET pokemon_secondtype='poison' WHERE pokemonid=1;

DELETE FROM pokemon WHERE pokemonid=25;
DELETE FROM pokemon;
commit;
SELECT * FROM pokemon WHERE pokemonid=25 and POKEMON_SECONDTYPE is null;
DELETE FROM pokemon WHERE pokemon_secondtype='poison';


