--QUERY BASICS--

--SELECT [column 1, column 2] FROM [the table name]
SELECT * FROM employee;
--'*' means all columns
SELECT firstname,lastname FROM employee;

--ALIAS (using the AS keyword) changes the title of the attribute in a result set
SELECT lastname AS muffin, firstname FROM employee;

--DIFFERENT SELECTS
SELECT * FROM employee WHERE title='Sales Manager';
SELECT * FROM employee WHERE title='Sales Support Agent' and FIRSTNAME='Jane';
SELECT * FROM employee WHERE firstname='Andrew' OR firstname='Nancy' OR firstname='Margaret';
--Also you can use >,<,<=,>= the where clause as well
--different ways to say not equals
select * from employee where firstname != 'Jane';
SELECT * FROM EMPLOYEE WHERE FIRSTNAME<> 'Jane';
SELECT * FROM EMPLOYEE WHERE FIRSTNAME ^= 'Jane';
--null references
select * from employee where reportsto is null;
select * from employee where reportsto is not null;

desc album;
select * from album;
--like keyword
--% means you can insert multiple characters (zero characters is also acceptible)
--'_' ,eams you must insert a single character
select * from album where title like 'B%';
select * from album where title like '%ig Ones';
select * from album where title like 'B% %nes';
select * from album where title like '_ig Ones';
select * from album where title like '%B_g_O%';

select * from employee;
--Order by examples--aka sorting
--sorting is java side, ordering is sql side
--defaults to ascending order
select * from employee order by lastname;
select * from employee order by lastname asc;
select * from employee order by lastname desc;
select * from employee order by city, firstname;


select * from employee;
--bvetween keyword--
--inclusive range
select * from employee where employeeid between 5 and 7;

--IN keyword
SELECT * FROM employee WHERE firstname='Andrew' OR firstname='Nancy' OR firstname='Margaret';

--looks cleaner to use the in operator--its also faster
select * from employee where firstname in ('Andrew','Nancy','Margaret');

--aggregate functions
--what is an aggrigate?
---------a calculation operating on a group of records (data entries)
--types:
--different aggregate functions
--count, max, min, avg, sum, distinct       there are more but i wont highlight these
select*from album;
select count(*) from album;
select * from employee;
select count (*) from employee;
select count(*) from employee where title='Sales Support Agent';
select count (distinct (title)) from employee;

select * from invoice;
--other aggregate exampkles
select customerid, invoiceid, total from invoice;
select max (total) from invoice;
select MIN (total) from invoice;
select avg (total) from invoice;
select sum (total) from invoice;
--group by, forces aggregate function to work on subsets of grouped 
--              data as opposed to the entire result set(aka temporary table)
select billingcountry, count(*) from invoice group by billingcountry;

--HAVING example
----when group by is not used, HAVING behaves like WHERE
--WHERE filters before aggregate functions, HAVING may filter after
select billingcountry, sum(total) from invoice where billingcountry!='Ireland' group by billingcountry;
-- incorrect select billingcountry, sum(total) from invoice where billingcountry!='Ireland'and sum(total)>50 group by billingcountry;
--having is capable of creating a condition based on AGGREGATE values, WHERE cannot
select billingcountry, sum(total) from invoice group by billingcountry having sum(total)>50;
--WHERE goes before GROUP BY goes before HAVING
--scalar functions
--the following is my own terms, you may not find them online, others may not know what you are talking about
--Categories of scalar functions:
--numeric, character, date, conversion

--what is dual?
select 7*10 from dual;
select * from dual;
--numeric
--abs(x), ceil(x), floor(x), trunc(x,y), round(x,y)
select abs(-99) from dual;
select floor(88,.77) from dual;
select ceil(88.77) from dual;
select trunc(889.99190099, 3) from dual;
select round(889.99190099, 3) from dual;
--character         there are lots of these. Including but not limited to:
---     upper(x), lower(x), length(x)
select 'hello world' from dual;
select upper('hello world') from dual;
select lower('HELLO WORLD') from dual;
select length('hello world') from dual;
--date      there are lots of these
--            next_day(x, 'week_day'), last_day(x), sysdate
select sysdate from dual;
select last_day(sysdate) from dual;
--conversion convert data types
---         to_char(), to_date(),   and more
--we'll ignore the example for now.

select * from invoice;
select billingcountry, billingcity from invoice;
select upper(billingcountry), billingcity from invoice;
--so what does a scalar actually do?
--its an operation that works on a single record, as opposed to an aggregate function
--that works on a set of data.



---------subquery example-----------
desc customer;
select * from customer;

select * from invoice;

select * from customer where customerid IN(
    select customerid from invoice where total>16.0);
    --------the inner query is defining a set of customerid's that the outer query will choose from.
    --                  for this reason, the inner query must complete first.
    
---more subqueries
select customerid, total, invoiceid from (select * from invoice where total>16.0) where invoiceid>200;







