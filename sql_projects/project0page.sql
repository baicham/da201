create table bank(
    username varchar2(50),
    firstname varchar2(50) not null,
    lastname varchar2(50) not null,
    password varchar2 (50) not null,
    access_type varchar2(50) not null,
    primary key(username)
);

create table bank_Accounts(
    acc_num number,
    acc_bal number,
    acc_type varchar2 (50),
    acc_date date,
    visible varchar2(50),
    primary key(acc_num)
);

create table user_acc (
    username varchar2(50),
    acc_num number,
    constraint pk_user_acc primary key(
    username,
    acc_num
    ),
    foreign key (username) references bank (username),
    foreign key (acc_num) references bank_accounts (acc_num)
);

create sequence make_acc_num
    start with 1813701767
    increment by 1;
    
    
create or replace procedure make_acc(acc_bal in number, acc_type in varchar2, vis in varchar2, username in varchar2)
is
bank number;
begin
    bank:=make_acc_num.nextval;
    insert into bank_accounts values(bank, acc_bal, acc_type, current_timestamp, vis);
    insert into user_acc values(username, bank);
    commit;
end;
/


create or replace procedure update_bal(accnum in number, accbal in number)
is 
begin
    update BANK_ACCOUNTS set acc_bal=accbal where acc_num=accnum;
    commit;
end;
/

create or replace procedure make_user(username in varchar2, first in varchar2, last in varchar2, pass in varchar2, access in varchar2)
is 
begin
    insert into bank values(username, first, last, pass, access);
    commit;
end;
/

create or replace procedure Make_joints(acc_num in number, username in varchar2)
is 
begin
    insert into user_acc values (username, acc_num);
    commit;
end;
/

create or replace procedure delete_Accs(accnum in number)
is 
begin
    delete from user_acc where acc_num=accnum;
    delete from bank_accounts where acc_num=accnum;
end;
/


create or replace trigger insert_acc_num
before insert on bank_accounts
for each row
begin
    if: new.acc_num is null then
    select make_acc_num.nextval into :new.acc_num from dual;
    end if;
end;
/


create or replace trigger insert_date
before insert on bank_accounts
for each row
begin
    if: new.acc_date is null then
        select current_timestamp into :new.acc_date from dual;
    end if;
end;
/


create or replace procedure set_some(num in number)
is
begin
    update bank_accounts set visible='true' where acc_num=num;
    commit;
end;
/



create or replace procedure set_all
is
begin
    update bank_accounts set visible='true' where visible='false';
    commit;
end;
/


begin
    set_all;
end;
/


select * from bank;
select * from bank_accounts;
select * from user_acc;


select * from bank A inner join user_acc B on A.username=B.username;
Select * from user_acc A inner join bank_accounts B on A.acc_num=B.acc_num;

insert into bank values('jojo', 'jojo','jojo','jojo','Admin');


commit;